/**
* Demandware Script File
*
* Provides API field tag data for a transactional message that is sending product list information.
*/
importPackage(dw.customer);
importPackage(dw.system);
importPackage(dw.util);

var FieldDataProvider : Function = require("~/cartridge/scripts/messages/providers/FieldDataProvider");
var CompositeFieldDataProvider : Function = require("~/cartridge/scripts/messages/providers/CompositeFieldDataProvider"); 
var ProductFieldDataProvider : Function = require("~/cartridge/scripts/messages/providers/ProductFieldDataProvider");
var AddressFieldDataProvider : Function = require("~/cartridge/scripts/messages/providers/AddressFieldDataProvider");
var CustomerFieldDataProvider : Function = require("~/cartridge/scripts/messages/providers/CustomerFieldDataProvider");
var CompositeLoopFieldDataProvider : Function = require("~/cartridge/scripts/messages/providers/CompositeLoopFieldDataProvider");

function ProductListFieldDataProvider(productList : ProductList, otherField : String, viewType : String, defaultValues : Object) {
	this.productList = productList;
	this.otherField = otherField;
	this.viewType = viewType;
	this.defaultValues = defaultValues;
}

ProductListFieldDataProvider.prototype = new FieldDataProvider();
ProductListFieldDataProvider.constructor = ProductListFieldDataProvider;

ProductListFieldDataProvider.prototype.provideFields = function() : Object {
	var fieldData : Object = {};
	if (!empty(this.productList)) {
		var productList : ProductList = this.productList;
		fieldData["prodListId"] = productList.getID();
		fieldData["name"] = productList.getName();
		fieldData["description"] = productList.getDescription();
		fieldData["eventCity"] = productList.getEventCity();
		fieldData["eventCountry"] = productList.getEventCountry();
		fieldData["eventState"] = productList.getEventState();
		fieldData["eventType"] = productList.getEventType();
		if (!empty(productList.getEventDate())) {
			var locale : String = Site.getCurrent().getDefaultLocale();
			var calendar : Calendar = Site.getCalendar();
			calendar.setTime(productList.getEventDate());
			fieldData["eventDate"] = StringUtils.formatCalendar(calendar, locale, Calendar.LONG_DATE_PATTERN);
		}
		if (!empty(productList.getRegistrant())) {
			var registrant : ProductListRegistrant = productList.getRegistrant();
			fieldData["regEmail"] = registrant.getEmail();
			fieldData["regFirstName"] = registrant.getFirstName();
			fieldData["regLastName"] = registrant.getLastName();
			fieldData["regRole"] = registrant.getRole();
		}
		if (!empty(productList.getCoRegistrant())) {
			var registrant : ProductListRegistrant = productList.getCoRegistrant();
			fieldData["coregEmail"] = registrant.getEmail();
			fieldData["coregFirstName"] = registrant.getFirstName();
			fieldData["coregLastName"] = registrant.getLastName();
			fieldData["coregRole"] = registrant.getRole();
		}
		var fieldProviders : Array = [];
		if (!empty(productList.getOwner())) {
			fieldProviders.push(new CustomerFieldDataProvider(productList.getOwner(), this.defaultValues));
		}
		if (!empty(productList.getCurrentShippingAddress())) {
			fieldProviders.push(new AddressFieldDataProvider(productList.getCurrentShippingAddress(), "ship"));
		}
		var iterator : Iterator = productList.getPublicItems().iterator();
		var productCount = 1;
		var productFieldProviders : Array = [];
		while (iterator.hasNext()) {
			var productListItem : ProductListItem = iterator.next();
			if (productListItem.isPublic() && !empty(productListItem.getProduct())) {
				fieldData["productPriority_" + productCount] = productListItem.getPriority().toString();
				fieldData["productQty_" + productCount] = productListItem.getQuantityValue().toString();
				fieldData["productQtyPurch_" + productCount] = productListItem.getPurchasedQuantityValue().toString();
				productFieldProviders.push(new ProductFieldDataProvider(productListItem.getProduct(), this.otherField, this.viewType, "_" + productCount));
				productCount++;
			}
		}
		if (productFieldProviders.length > 0) {
			fieldProviders.push(new CompositeLoopFieldDataProvider(productFieldProviders));
		}
		var compositeDataProvider = new CompositeFieldDataProvider(fieldProviders);
		fieldData = compositeDataProvider.getFields(fieldData);
	}
	return fieldData;
};

module.exports = ProductListFieldDataProvider;