/**
* Demandware Script File
*
* Provides a pipelet that can be used for sending a "send to friend" message for a product in Bronto.
*
* @input Product : dw.catalog.Product
* @input SendToFriendForm : dw.web.Form
* @input FromName : String
* @input FromEmail : String
* @input ReplyTo : String
* @input Objects : Array	Optional array of objects (JavaScript or ExtensibleObjects to include field tags)
* @input AdditionalRecipients : Array	Optional array of email addresses that will also receive this message
*/
importPackage(dw.catalog);
importPackage(dw.util);

var BrontoLogger = require("~/cartridge/scripts/util/BrontoLogger");
var SettingsManager = require("~/cartridge/scripts/util/SettingsManager");
var TransactionalMessageHelper = require("~/cartridge/scripts/messages/TransactionalMessageHelper");
var TransactionalMessageScheduler = require("~/cartridge/scripts/messages/TransactionalMessageScheduler");
var CompositeFieldDataProvider : Function = require("~/cartridge/scripts/messages/providers/CompositeFieldDataProvider"); 
var ProductFieldDataProvider : Function = require("~/cartridge/scripts/messages/providers/ProductFieldDataProvider");
var GenericFormFieldDataProvider : Function = require("~/cartridge/scripts/messages/providers/GenericFormFieldDataProvider");
var GenericObjectFieldDataProvider : Function = require("~/cartridge/scripts/messages/providers/GenericObjectFieldDataProvider");

function execute(pdict : PipelineDictionary) : Number {
	var logger = new BrontoLogger("SendToFriendProductMessage");
	var settingsManager = new SettingsManager();
	var otherField : String = settingsManager.getSetting("order-import", ["extensions", "settings", "other-field"]);
	try {
		var transactionalScheduler = new TransactionalMessageScheduler("send-to-friend-product");
		var fieldProviders : Array = [
			new ProductFieldDataProvider(pdict.Product, otherField, transactionalScheduler.getMessageOrDefaultSetting("view-type")),
			new GenericFormFieldDataProvider(pdict.SendToFriendForm)
		];
		GenericObjectFieldDataProvider.addObjectFieldDataProviders(pdict.Objects, fieldProviders);
		var compositeDataProvider = new CompositeFieldDataProvider(fieldProviders);
		var recipientEmail : String = pdict.SendToFriendForm.friendsemail.value;
		var products : List = new ArrayList();
		products.add(pdict.Product);
		var recipients : Array = TransactionalMessageHelper.getCombinedRecipients(recipientEmail, pdict.AdditionalRecipients);
		transactionalScheduler.schedule(compositeDataProvider.getFields(), recipients, products, pdict.FromName, pdict.FromEmail, pdict.ReplyTo);
	} catch (e) {
		logger.getLogger().debug("Error scheduling send to friend product message: " + e.message);
		return PIPELET_ERROR;
	}
	return PIPELET_NEXT;
}