/**
* Demandware Script File
*
* Specialized logger wrapper that provides helpers to log Bronto API responses as well as a
* Demandware logger to log messages scoped to an executing category.
*/
importPackage(dw.system);
importPackage(dw.web);
importPackage(dw.ws);

var DEFAULT_LOG_LEVEL = "error";

function BrontoLogger(category : String) {
	this.logger = Logger.getLogger("int_bronto", category);
}

BrontoLogger.prototype.getLogger = function() : Logger {
	return this.logger;
};

BrontoLogger.prototype.logResponse = function(response, level : String) {
	var errors : Array = getErrorMessages(response);
	var logger : Logger = this.getLogger();
	var func : Function = logger[level] || logger[DEFAULT_LOG_LEVEL];
	for (var i = 0; i < errors.length; i++) {
		func.call(logger, errors[i]);
	} 
};

function getErrorMessages(response) : Array {
	var errorMessages : Array = [];
	if(!empty(response.errors)) {
		var errors : Array = response.errors;
		var results : Array = response.results;
		for (var i = 0; i < errors.length; i++) {
			var result = result[errors[i]];
			errorMessages.push(Resource.msgf("errors.response", "errors", null, result.errorCode, result.errorString)); 
		}
	}
	return errorMessages;
};

module.exports = BrontoLogger;