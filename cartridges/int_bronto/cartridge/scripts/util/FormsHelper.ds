/**
* Demandware Script File
* 
* Helper class for getting form data into a JSON representation.
*/
importPackage(dw.web);

var FormsHelper : Object = {
	/**
	 * Gets a flattened object of all the fields IDs and their values (including all of the children form elements)
	 */
	getFieldsForForm: function(form : FormGroup, separator : String, parentId : String) : Object {
		separator = separator || ".";
		parentId = parentId || "";
		var fields : Object = {};
		for (var i = 0; i < form.getChildCount(); i++) {
			var child = form[i];
			if (child instanceof FormField) {
				var fieldId : String = parentId + child.getFormId();
				fields[fieldId] = child.getValue();
			} else if (child instanceof FormGroup) {
				var formFields = FormsHelper.getFieldsForForm(child, separator, parentId + child.getFormId() + separator);
				for (var key in formFields) {
					fields[key] = formFields[key];
				}
			}
		}
		return fields;
	}
};

module.exports = FormsHelper;