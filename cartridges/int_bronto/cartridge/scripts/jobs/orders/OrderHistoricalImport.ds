/**
* Demandware Script File
*
* Executes the historical import portion of the Order Import job. The historical import
* portion looks for all non-imported orders that are older than the last sync date 
* that have not crossed the error threshold and provides them as a list of orders for
* the OrderBulkProcessor.
*
* @input ItemsFound : Number
* @output Items : dw.util.Iterator
*/
importPackage(dw.object);
importPackage(dw.order);
importPackage(dw.system);
importPackage(dw.util);

var BrontoLogger = require("~/cartridge/scripts/util/BrontoLogger");
var SettingsManager = require("~/cartridge/scripts/util/SettingsManager");
var BrontoConstants = require("~/cartridge/scripts/util/BrontoConstants");

var ORDER_BY : String = "creationDate desc";
var SEARCH_QUERY : String = "status = {0} AND (custom.brontoImported = NULL OR custom.brontoImported = false) AND (NOT custom.brontoImportErrorCount >= {1}) AND lastModified <= {2}";
var CONFIRMATION_QUERY : String = SEARCH_QUERY + " AND confirmationStatus = {3}";

function execute(pdict : PipelineDictionary) : Number {
	var logger = new BrontoLogger("OrderImport");
	var settingsManager = new SettingsManager();
	var lastSyncDate : Date = null;
	var customObject : CustomObject = CustomObjectMgr.getCustomObject("BrontoJobSetting", "OrderImport");
	if (!empty(customObject)) {
		lastSyncDate = customObject.getCustom()["lastSyncDate"];
	}
	var limit : Number = BrontoConstants.DEFAULT_ORDER_IMPORT_LIMIT;
	var jobSettings : Object = settingsManager.getSetting("order-import", ["jobs", "OrderImport"]);
	if (!empty(jobSettings) && !empty(jobSettings["items-to-sync"])) {
		limit = Number(jobSettings["items-to-sync"]);
	}
	var now : Date = new Date();
	if (empty(lastSyncDate)) {
		lastSyncDate = now;
	}
	limit -= pdict.ItemsFound;
	var ordersList : List = new ArrayList();
	var confirmationStatus = settingsManager.getSetting("order-import", ["extensions", "settings", "confirmation-status"]);
	var query : String = confirmationStatus == Order.CONFIRMATION_STATUS_CONFIRMED || confirmationStatus == Order.CONFIRMATION_STATUS_NOTCONFIRMED ? CONFIRMATION_QUERY : SEARCH_QUERY;
	var statuses : Array = settingsManager.getSetting("order-import", ["extensions", "settings", "order-statuses"]) || [Order.ORDER_STATUS_COMPLETED, Order.ORDER_STATUS_NEW, Order.ORDER_STATUS_OPEN];
	for (var i = 0; i < statuses.length && limit - ordersList.size() > 0; i++) {
		var orders : SeekableIterator = OrderMgr.searchOrders(query, ORDER_BY, Number(statuses[i]), BrontoConstants.MAX_ERROR_COUNT, lastSyncDate, confirmationStatus);
		ordersList.addAll(orders.asList(0, limit - ordersList.size()));
		orders.close();
	}
	pdict.Items = ordersList.iterator();
	return PIPELET_NEXT;
}