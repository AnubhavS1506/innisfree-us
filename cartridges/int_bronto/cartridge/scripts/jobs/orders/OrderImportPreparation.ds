/**
* Demandware Script File
*
* Prepares the order import job by verifying the Bronto API is configured and 
* defines a BulkProcessor that will process all contacts in batches for optimal
* import performance with Bronto. This automatically picks the appropriate 
* Bronto API OrderBulkProcessor strategy to use depending on whether they are 
* configured to use the SOAP or REST API.
*
* @output BulkProcessor : Object
*/
importPackage(dw.object);
importPackage(dw.system);

var BrontoLogger = require("~/cartridge/scripts/util/BrontoLogger");
var SettingsManager = require("~/cartridge/scripts/util/SettingsManager");
var BrontoApi = require("~/cartridge/scripts/api/BrontoApi");
var BrontoRestApi = require("~/cartridge/scripts/api/BrontoRestApi");
var SOAPOrderBulkProcessor = require("~/cartridge/scripts/jobs/orders/SOAPOrderBulkProcessor");
var RESTOrderBulkProcessor = require("~/cartridge/scripts/jobs/orders/RESTOrderBulkProcessor");

function execute(pdict : PipelineDictionary) : Number {
	var logger = new BrontoLogger("OrderImportPreparation");
	var settingsManager = new SettingsManager();
	var apiToken : String = settingsManager.getSetting("main", "apiToken");
	if (empty(apiToken)) {
		logger.getLogger().error("No API token set for this site");
		return PIPELET_ERROR;
	}
	var viewType : String = settingsManager.getSetting("order-import", ["extensions", "settings", "view-type"]);
	var productDescriptionId : String = settingsManager.getSetting("order-import", ["extensions", "settings", "product-description"]);
	if (settingsManager.getSetting("main", ["featureMap", "enableOrderService"])) {
		var orderStatus : String = settingsManager.getSetting("order-import", ["extensions", "settings", "order-status"]);
		var otherField : String = settingsManager.getSetting("order-import", ["extensions", "settings", "other-field"]);
		var oauthToken : String = settingsManager.getSetting("main", "oauthToken");
		var refreshToken : String = settingsManager.getSetting("main", "refreshToken");
		pdict.BulkProcessor = new RESTOrderBulkProcessor(new BrontoRestApi(oauthToken, refreshToken), viewType, productDescriptionId, orderStatus, otherField);
	} else {
		pdict.BulkProcessor = new SOAPOrderBulkProcessor(new BrontoApi(apiToken), viewType, productDescriptionId);
	}
	return PIPELET_NEXT;
}