/**
* Demandware Script File
*
* Tests whether a particular job is in "test mode" based on the settings. If it is,
* fires a PIPELET_ERROR such that the job will be skipped.
*
* @input Job : String
*/
importPackage(dw.system);

var BrontoLogger = require("~/cartridge/scripts/util/BrontoLogger");
var TestModeHelper = require("~/cartridge/scripts/util/TestModeHelper");

var BRONTO_JOBS_PREFIX : String = "BrontoJobs-";

function execute(pdict : PipelineDictionary) : Number {
	var jobName : String = pdict.Job;
	if (jobName.indexOf(BRONTO_JOBS_PREFIX) === 0) {
		jobName = jobName.substring(BRONTO_JOBS_PREFIX.length);
	}
	var checkTestMode = dw.system.Site.getCurrent().getCustomPreferenceValue('checkTestMode');
	if(checkTestMode) {
		if (TestModeHelper.isEnabled(jobName)) {
			var logger = new BrontoLogger("CheckTestMode");
			logger.getLogger().debug("Skipping job due to test mode being enabled: {0}", pdict.Job);
			return PIPELET_ERROR;
		}
	}
	return PIPELET_NEXT;
}