/**
* Demandware Script File
*
* A triggered scheduled execution that will reset the Bronto import status for contacts or orders.
*
* @input Execution : dw.object.CustomObject
*/
importPackage(dw.customer);
importPackage(dw.order);
importPackage(dw.system);
importPackage(dw.web);
importPackage(dw.util);

var CREATED_AFTER : String = "creationDate >= {0}";
var CREATED_BEFORE : String = "creationDate < {1}";

function execute(pdict : PipelineDictionary) : Number {
	var data : Object = JSON.parse(pdict.Execution.getCustom()["data"]);
	var importStatus : Boolean = data["imported"];
	var afterDate : Date = getDateFromValue(data["after-date"]);
	var beforeDate : Date = getDateFromValue(data["before-date"]);
	var funcToCall : Function = null;
	if (data["type"] === "orders") {
		funcToCall = resetOrders;
	} else if (data["type"] === "contacts") {
		funcToCall = resetContacts;
	}
	if (funcToCall === null) {
		return PIPELET_ERROR;
	}
	var mainQuery : String = "";
	if (!empty(afterDate)) {
		mainQuery += CREATED_AFTER;
	}
	if (!empty(beforeDate)) {
		if (!empty(mainQuery)) {
			mainQuery += " AND ";
		}
		mainQuery += CREATED_BEFORE;
	}
	var result : Object = funcToCall(importStatus, mainQuery, afterDate, beforeDate);
	pdict.Execution.getCustom()["result"] = JSON.stringify(result);
	return PIPELET_NEXT;
}

function getDateFromValue(value) : Date {
	return !empty(value) ? new Date(value) : null;
}

function resetOrders(importStatus : Boolean, query : String, afterDate : Date, beforeDate : Date) : Object {
	var orders : SeekableIterator = OrderMgr.searchOrders(query, null, afterDate, beforeDate);
	var ordersReset : Number = 0;
	while (orders.hasNext()) {
		var order : Order = orders.next();
		order.getCustom()["brontoImported"] = importStatus;
		order.getCustom()["brontoImportErrorCount"] = 0;
		order.getCustom()["brontoLastImportError"] = null;
		order.getCustom()["brontoLastImportDate"] = null;
		ordersReset++;
	}
	orders.close();
	return {
		"Orders Reset": ordersReset
	};
}

function resetContacts(importStatus : Boolean, query : String, afterDate : Date, beforeDate : Date) : Object {
	var profiles : SeekableIterator = CustomerMgr.searchProfiles(query, null, afterDate, beforeDate);
	var profilesReset : Number = 0;
	while (profiles.hasNext()) {
		var profile : Profile = profiles.next();
		profile.getCustom()["brontoImported"] = importStatus;
		profile.getCustom()["brontoImportErrorCount"] = 0;
		profile.getCustom()["brontoLastImportError"] = null;
		profile.getCustom()["brontoLastImportDate"] = null;
		profilesReset++;
	}
	profiles.close();
	return {
		"Contacts Reset": profilesReset
	};
}