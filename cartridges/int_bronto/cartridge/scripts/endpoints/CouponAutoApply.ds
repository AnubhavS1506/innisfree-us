/**
* Demandware Script File
*
* JSON discovery endpoint that defines the settings to be configured for the Coupons settings.
*
* @output json : String
*/
importPackage(dw.system);

function execute(pdict : PipelineDictionary) : Number {
	pdict.json = JSON.stringify({
		extensions: [
			{
				id: "settings",
				name: "Auto-Apply Query Parameters",
				fields: [
					{
						id: "coupon-parameter",
						name: "Coupon Code Query Parameter",
						type: "text",
						required: true,
						typeProperties: {
							"default": "coupon"
						}
					},
					{
						id: "invalid-coupon-parameter",
						name: "Invalid Coupon Query Parameter",
						type: "text",
						required: true,
						typeProperties: {
							"default": "invalid_coupon"
						}
					}
				]
			},
			{
				id: "message-settings",
				name: "Settings",
				fields: [
					{
						id: "display-flash",
						name: "Display Message",
						type: "boolean",
						required: true,
						typeProperties: {
							"default": true
						}
					},
					{
						id: "flash-success-background",
						name: "Success Background Color",
						type: "color",
						required: true,
						depends: [
							{ id: "display-flash", values: [ true ] }
						],
						typeProperties: {
							"default": "#dff0d8"
						}
					},
					{
						id: "flash-success-foreground",
						name: "Success Text Color",
						type: "color",
						required: true,
						depends: [
							{ id: "display-flash", values: [ true ] }
						],
						typeProperties: {
							"default": "#3c763d"
						}
					},
					{
						id: "success-message",
						name: "Success Message",
						type: "textarea",
						required: true,
						depends: [
							{ id: "display-flash", values: [ true ] }
						],
						typeProperties: {
							"default": "Coupon {code} was successfully applied to your basket."
						}
					},
					{
						id: "flash-failure-background",
						name: "Failure Background Color",
						type: "color",
						required: true,
						depends: [
							{ id: "display-flash", values: [ true ] }
						],
						typeProperties: {
							"default": "#f2dede"
						}
					},
					{
						id: "flash-failure-foreground",
						name: "Failure Text Color",
						type: "color",
						required: true,
						depends: [
							{ id: "display-flash", values: [ true ] }
						],
						typeProperties: {
							"default": "#a94442"
						}
					},
					{
						id: "invalid-message",
						name: "Invalid Message",
						type: "textarea",
						required: true,
						depends: [
							{ id: "display-flash", values: [ true ] }
						],
						typeProperties: {
							"default": "The coupon {code} could not be applied."
						}
					},
					{
						id: "depleted-message",
						name: "Depleted Message",
						type: "textarea",
						required: true,
						depends: [
							{ id: "display-flash", values: [ true ] }
						],
						typeProperties: {
							"default": "The coupon {code} has been depleted."
						}
					},
					{
						id: "expired-message",
						name: "Expired Message",
						type: "textarea",
						required: true,
						depends: [
							{ id: "display-flash", values: [ true ] }
						],
						typeProperties: {
							"default": "The coupon {code} has expired."
						}
					}
				]
			}
		]
	});
	return PIPELET_NEXT;
}