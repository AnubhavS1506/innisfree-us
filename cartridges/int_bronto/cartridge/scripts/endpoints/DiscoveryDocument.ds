/**
* Demandware Script File
*
* JSON discovery document endpoint that defines the locations of extension groups that can be configured in Bronto.
*
* @output json : String
*/
importPackage(dw.system);
importPackage(dw.web);

var BrontoConstants = require("~/cartridge/scripts/util/BrontoConstants");

function execute(pdict : PipelineDictionary) : Number {
	var json = {
		apiVersion: "1.0",
		iconSet: "https://cdn.bronto.com/commerce-connector/dwre/css/dwre.css",
		instanceInfo: {
			id: System.getPreferences().getUUID(),
			name: Site.getCurrent().getName(), 
			type: "Demandware",
			extensionVersion: BrontoConstants.EXTENSION_VERSION,
			features: {
				promotion: true
			},
			properties: {
				type: getInstanceType(),
				hostname: System.getInstanceHostname(),
				timezone: System.getInstanceTimeZone()
			}
		},
		extensionGroups: [
			{
				id: "contact-import",
				name: "Contacts",
				icon: "dwre-icon-contacts",
				url: URLUtils.https(new URLAction("BrontoEndpoints-ContactImport", Site.getCurrent().getID())).toString()
			},
			{
				id: "newsletter-subscription-import",
				name: "Opt-Ins",
				icon: "dwre-icon-optins",
				url: URLUtils.https(new URLAction("BrontoEndpoints-NewsletterSubscriptionImport", Site.getCurrent().getID())).toString()
			},
			{
				id: "order-import",
				name: "Orders",
				icon: "dwre-icon-orders",
				url: URLUtils.https(new URLAction("BrontoEndpoints-OrderImport", Site.getCurrent().getID())).toString()
			},
			{
				id: "transactional-messages",
				name: "Messages",
				icon: "dwre-icon-messages",
				url: URLUtils.https(new URLAction("BrontoEndpoints-TransactionalMessages", Site.getCurrent().getID())).toString()
			},
			{
				id: "product-recommendations",
				name: "Recommendations",
				icon: "dwre-icon-recommendations",
				url: URLUtils.https(new URLAction("BrontoEndpoints-ProductRecommendations", Site.getCurrent().getID())).toString()
			},
			{
				id: "coupon-auto-apply",
				name: "Coupons",
				icon: "dwre-icon-coupons",
				url: URLUtils.https(new URLAction("BrontoEndpoints-CouponAutoApply", Site.getCurrent().getID())).toString()
			},
			{
				id: "integrations",
				name: "Integrations",
				icon: "dwre-icon-integrations",
				url: URLUtils.https(new URLAction("BrontoEndpoints-Integrations", Site.getCurrent().getID())).toString()
			},
			{
				id: "advanced",
				name: "Advanced",
				icon: "dwre-icon-advanced",
				url: URLUtils.https(new URLAction("BrontoEndpoints-Advanced", Site.getCurrent().getID())).toString()
			}
		]
	};
	pdict.json = JSON.stringify(json);
	return PIPELET_NEXT;
}

function getInstanceType() : String {
	var instanceType = null;
	switch (System.getInstanceType()) {
		case System.DEVELOPMENT_SYSTEM:
			instanceType = "Development";
			break;
		case System.STAGING_SYSTEM:
			instanceType = "Staging";
			break;
		case System.PRODUCTION_SYSTEM:
			instanceType = "Production";
			break;
		default:
			instanceType = "Unknown";
			break;
	}
	return instanceType;
}