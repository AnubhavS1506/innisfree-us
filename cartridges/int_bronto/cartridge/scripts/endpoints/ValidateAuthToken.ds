/**
* Demandware Script File
*
* Script node that validates the incoming request is authorized based on the Authorization
* header. The auth token provided must match the registered auth token defined for the 
* current site that is being contacted. This provides a secure communication channel 
* between Bronto and the registered Demandware site in addition to running over HTTPS.
*
* This script node is placed in front of all endpoints that are exposed by this cartridge.
* If the incoming request does not contain a the matching auth token, the request is
* denied with a 403 Forbidden message.
*
* @input CurrentRequest : dw.system.Request
*/
importPackage(dw.object);
importPackage(dw.system);

var RegistrationManager = require("~/cartridge/scripts/registration/RegistrationManager");

var AUTHORIZATION_HEADER : String = "x-authorization";
var BEARER_SCHEME : String = "Bearer ";

function execute(pdict : PipelineDictionary) : Number {
	var authorization : String = pdict.CurrentRequest.getHttpHeaders().get(AUTHORIZATION_HEADER);
	// Verify there's an Authorization header that contains the Bearer scheme
	if (!empty(authorization) && authorization.indexOf(BEARER_SCHEME) === 0) {
		// Validate that the currently defined authToken matches the one passed in the header before
		// allowing access to the remainder of the pipeline
		var registration : CustomObject = RegistrationManager.getRegistrationForCurrentSite();
		if (registration.getCustom()["authToken"] === authorization.substr(BEARER_SCHEME.length)) {
			return PIPELET_NEXT;
		}
	}
	return PIPELET_ERROR;
}