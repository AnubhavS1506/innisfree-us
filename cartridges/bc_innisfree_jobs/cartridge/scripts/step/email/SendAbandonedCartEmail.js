'use strict';

var Status = require('dw/system/Status');
var Logger = require('dw/system/Logger').getLogger("SendAbandonedCartEmail");
var Transaction = require('dw/system/Transaction');
/**
 * @module app
 */
function execute(args) {
	var status = new Status(Status.OK, 'OK');
    var productIdsInAbandonedCart = [];
    	var CustomerMgr = require('dw/customer/CustomerMgr');
    	var customerList = CustomerMgr.queryProfiles("custom.abandonedCartProductJson != {0} AND custom.isAbandonedCartMailSent != {1}", null, null, true);
    	while (customerList.hasNext()) {
            var customerProfile = customerList.next();
            try{ 
            	var abandonedCartPlis = JSON.parse(customerProfile.custom.abandonedCartProductJson).plis;
	            for (var i = 0; i < abandonedCartPlis.length; i++) {
	                var pli = abandonedCartPlis[i];
	                productIdsInAbandonedCart[i] = pli.pid;
	            }
	            var Email =  require('app_innisfree_controllers/cartridge/scripts/models/EmailModel.js');
	            var lastModifiedTime = ((new Date()).getTime() - customerProfile.lastModified.getTime()) / (60 * 60 * 1000);
	            if ( lastModifiedTime > 1){
	            	Email.sendMail({
	                template: 'mail/abondanedcartmail',
	                recipient: customerProfile.email,
	                subject: 'RE: Your Shopping Bag',
	                context: {productIdsInAbandonedCart: productIdsInAbandonedCart}
	                
	            	});
	            	Transaction.wrap(function() {
		    			customerProfile.custom.isAbandonedCartMailSent = true; 
		        	});	
	            }
	            
		    	}catch(e){
		        	var ex = e;
		    		Logger.error('Exception in SendAbandonedCartEmail execute :'+ e +' for customer id : ' +customerProfile.customer.ID);
		    		//status = new Status(Status.OK, 'ERROR');
		        }
        }
    
    
    return status;
}

exports.execute = execute;