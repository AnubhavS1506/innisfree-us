'use strict';

/**
 * @module app
 */
var Status = require('dw/system/Status');
var FileUtil = require('~/cartridge/scripts/util/FileUtil.js');
var Logger = require('dw/system/Logger').getLogger("SendShipmentConfirmationEmail");
var CSVStreamReader = require('dw/io/CSVStreamReader');
var FileReader = require('dw/io/FileReader');
var EmailModel = require('app_innisfree_controllers/cartridge/scripts/models/EmailModel.js');
var Resource = require('dw/web/Resource');
var Transaction = require('dw/system/Transaction');

function sendEmail(allLine) {
    if(allLine) {
        var template = ('mail/shipmentconfirmation');
        var recipient = allLine.customerEmail;
        var subject = Resource.msgf('order.shipmentconfirmation.title', 'order', 'null', allLine.orderNo);
        var options = {
            context: {
                'Order': allLine
            },
            template: template,
            recipient: recipient,
            subject: subject
        };
        EmailModel.sendMail(options);
    } else {
        return false;
    }
}

function readFile(csvFile, archiveFolder) {
    var csvReader = new CSVStreamReader(new FileReader(csvFile), ";", '"');
    try {
        var allLine = csvReader.readAll();
        if( allLine && allLine.size() > 0 ) {
            var result = {
                'order': allLine[0][0],
                'orderNo': allLine[0][1],
                'orderStatus': allLine[0][2],
                'carrier': allLine[0][3],
                'shippmentTrackingNo': allLine[0][4]
            };
            var order = dw.order.OrderMgr.getOrder(result.orderNo);
            var trackingNumber = result.shippmentTrackingNo;
            if(order !== null) {
                if (empty(order.custom.shippedDate)) {
                    Transaction.wrap(function () {
                        order.custom.shippedDate = new Date();
                        order.defaultShipment.setTrackingNumber(trackingNumber);
                    });
                }
                return order;
            }
        }
        if (allLine.length > 1) {
            throw new Error('This file is valid structure');
        }

        if (!sendEmail(allLine)) {
            var rowValue = allLine[0][0].split(';');
            throw new Error('the order ' + rowValue[1] + ' can not be sent');
        }
    } catch (e) {
        Logger.error(e.message);
        return false;
    } finally {
        FileUtil.moveFileToTarget(csvFile, archiveFolder);
        if (csvReader) {
            csvReader.close();
        }
    }
    return true;
}

function execute(args) {
    var filePattern = args.filePattern;
    var sourceFolder = args.sourceFolder;
    var fileList = FileUtil.getFilesFromFolder(sourceFolder, filePattern);
    var listResult = [];
    var valid = true;
    if (!fileList) {
        return new Status(Status.OK, 'WARN');
    }
    if (fileList && fileList.length > 0) {
        for (var i = 0; i < fileList.length; i++) {
            var result = readFile(fileList[i], args.archiveFolder);
            if (!listResult && !valid) {
                valid = false;
            }
            listResult.push(result);
        }
    }
    if (!valid) {
        return new Status(Status.OK, 'ERROR');
    }
    if(listResult && listResult.length > 0) {
        for (var x = 0; x < listResult.length; x++) {
             sendEmail(listResult[x]);
        }
    }
    return new Status(Status.OK, 'OK');
}

exports.execute = execute;