'use strict';

/**
 * @module app
 */
var Status = require('dw/system/Status');
var FileUtil = require('~/cartridge/scripts/util/FileUtil.js');
var CustomObjectMgr = require("dw/object/CustomObjectMgr");
var Transaction = require('dw/system/Transaction');

function createActiveDataCSVFile(csvFile, wcpList) {
    var csvWriter = new dw.io.CSVStreamWriter(new dw.io.FileWriter(csvFile, true));
    csvWriter.writeNext([""]);
    csvWriter.writeNext(['wishlistCounter']);
    csvWriter.writeNext(["productID", "custom.wishlistCounter"]);

    Transaction.wrap(function () {
        for (var wcp in wcpList) {
            var currentObj = wcpList[wcp];
            var productId = currentObj.custom.productId;
            var product = dw.catalog.ProductMgr.getProduct(productId);
            if (product) {
                var count = currentObj.custom.count > 0 ? currentObj.custom.count : 1;
                if (!empty(product.activeData) && product.activeData.UUID && ("wishlistCounter" in product.activeData.custom) && !empty(product.activeData.custom.wishlistCounter)) {
                    count += product.activeData.custom.wishlistCounter;
                }
                csvWriter.writeNext([productId, count]);
            }
            CustomObjectMgr.remove(currentObj);
        }
    });
    // close stream writer
    csvWriter.close();
}

function execute(args) {
    var sourceFolder = args.sourceFolder;
    var folderName = dw.io.File.IMPEX + dw.io.File.SEPARATOR + sourceFolder;
    var fileName = "WISHLIST_COUNTER_" + dw.util.StringUtils.formatCalendar(dw.system.System.getCalendar(), 'yyyyMMdd_HHmmss') + ".csv";
    var allProductWC = CustomObjectMgr.getAllCustomObjects('wishlistCounter');
    if (!allProductWC) {
        return new Status(Status.OK, 'WARN');
    }
    var fileHandler = FileUtil.generateFile(folderName, fileName);
    try {
        createActiveDataCSVFile(fileHandler, allProductWC.asList());
    } catch(e) {
        return new Status(Status.OK, 'ERROR');
    }
    return new Status(Status.OK, 'OK');
}

exports.execute = execute;