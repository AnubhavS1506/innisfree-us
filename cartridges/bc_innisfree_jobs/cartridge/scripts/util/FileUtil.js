var File = require('dw/io/File');
var ArrayList = require('dw/util/ArrayList');

function getFilesFromFolder(sourceFolder, filePattern) {
    var fileList = new ArrayList();
    var theDir = new File("Impex/" + sourceFolder);
    var regExp = new RegExp(filePattern);
    fileList.addAll(theDir.listFiles(function(file) {
        if(!filePattern) {
            return regExp.test(file.name);
        }
        return true;
    }));
    return fileList;
}

function checkExistedFolder(folder) {
    var folderPath = new File(folder);
    if(!folderPath.exists()) {
        folderPath.mkdir();
    }
    return folderPath;
}

function generateFile(sourceFolder, fileName) {
    var directory = new File(sourceFolder);
    if (!directory.exists()) {
        directory.mkdirs();
    }
    var fileHandler = new File(directory, fileName);

    if (!fileHandler.exists()) {
        if (!fileHandler.createNewFile()) {
            throw new Error(' Can not create file ' + fileName);
        }
    }
    return fileHandler;
}

function moveFileToTarget(file, folder) {
    var targetFolder = checkExistedFolder("Impex/" + folder);
    file.renameTo(new File(targetFolder.getFullPath() + "/" + file.name.substring(0, file.name.lastIndexOf('.csv')) + '.csv'));
    file.remove();
    return true;
}

exports.getFilesFromFolder = getFilesFromFolder;
exports.moveFileToTarget = moveFileToTarget;
exports.generateFile = generateFile;