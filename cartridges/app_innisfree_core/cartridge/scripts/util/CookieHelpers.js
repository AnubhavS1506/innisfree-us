'use strict';
var Cookie = require('dw/web/Cookie');

function checkCookie(cookieKey) {
    var isCookieExist = false;
    var cookie = request.httpCookies[cookieKey];
    if (!empty(cookie)) {
        isCookieExist = true;
    }
    return isCookieExist;
}

function createCookie(cookieObject) {
    if (!empty(cookieObject)) {
        var cookie = new Cookie(cookieObject.key, cookieObject.value);
        var cookieExpiredDay = new Number(cookieObject.maxAge);
        cookie.setMaxAge(cookieExpiredDay * 86400);
        response.addHttpCookie(cookie);
    }
}

module.exports.CheckCookie = checkCookie;
module.exports.CreateCookie = createCookie;