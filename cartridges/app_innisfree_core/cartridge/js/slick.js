'use-strict';


var cartSlickOptions = {
    accessibility: false,
    slidesToShow: 4,
    slidesToScroll: 3,
    slide: '.product-item',
    responsive: [
    {
        breakpoint: 768,
        settings: {
            slidesToShow: 2,
            slidesToScroll: 2
        }
    },
    {
        breakpoint: 480,
        settings: {
            slidesToShow: 2,
            slidesToScroll: 1
        }
    }
    ]
};

var pdpSlickOptions = {
    accessibility: false,
    infinite: false,
    slidesToShow: 4,
    slide: '.product-item',
    responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                infinite: false,
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 2
            }
        },
        {
            breakpoint: $('.pt_product-search-noresult').length ? 360 : 480,
            settings: {
                slidesToShow: 1
            }
        }
    ]
};

function initProductCarousel() {
    var $slickContainer = $('.bestSeller-slider > ul');
    var slickOptions = window.pageContext.ns == 'cart' ? cartSlickOptions : pdpSlickOptions;

    if (!$slickContainer.length) {
        return;
    }

    if (typeof $slickContainer.unslick == 'function') {
        $slickContainer.slick('unslick').slick(slickOptions);
    } else {
        $slickContainer.slick(slickOptions);
    }
}

exports.initProductCarousel = initProductCarousel;