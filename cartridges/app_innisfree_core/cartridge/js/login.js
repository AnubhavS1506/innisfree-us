'use strict';

var dialog = require('./dialog'),
    page = require('./page'),
    validator = require('./validator'),
    util = require('./util');

var login = {
    /**
     * @private
     * @function
     * @description init events for the loginPage
     */
    init: function () {
    	popupLogin();
    	missingEmail();
    	
        //o-auth binding for which icon is clicked
        $('.oAuthIcon').bind('click', function () {
            $('#OAuthProvider').val(this.id);
        });

        // subscribe email box
        $('.login-show.login .input-text').on('blur keyup', function() {
            if ($(".login-show #dwfrm_login").valid()) {
                $('.button-sigin').prop('disabled', false);  
            } else {
                $('.button-sigin').prop('disabled', 'disabled');
            }
        });

        //toggle the value of the rememberme checkbox
        $('#dwfrm_login_rememberme').bind('change', function () {
            if ($('#dwfrm_login_rememberme').attr('checked')) {
                $('#rememberme').val('true');
            } else {
                $('#rememberme').val('false');
            }
        });
        
        $('.password-reset').on('click', function (e) {
            e.preventDefault();
            dialog.open({
                url: $(e.target).attr('href'),
                options: {
                	width: 620,
                	dialogClass: 'reset-password provide-pass-account',
                    open: function () {
                        validator.init();
                        var $requestPasswordForm = $('[name$="_requestpassword"]');
                        var $submit = $requestPasswordForm.find('[name$="_requestpassword_send"]');
                        $(document).delegate(".close-popup-provide-pass-account .ui-icon-closethick","click",function(event){
                           $('.ui-widget-overlay').trigger('click');
                        })
                        $($submit).on('click', function (e) {
                            if (!$requestPasswordForm.valid()) {
                                return;
                            }
                            e.preventDefault();
                            var data = $requestPasswordForm.serialize();
                            // add form action to data
                            data += '&' + $submit.attr('name') + '=';
                            // make sure the server knows this is an ajax request
                            if (data.indexOf('ajax') === -1) {
                                data += '&format=ajax';
                            }
                            $.ajax({ 
                                type: 'POST',
                                url: $requestPasswordForm.attr('action'),
                                data: data,
                                success: function (response) {
                                    if (typeof response === 'object' &&
                                            !response.success &&
                                            response.error === 'CSRF Token Mismatch') {
                                        page.redirect(Urls.csrffailed);
                                    } else if (typeof response === 'string') {
                                        dialog.$container.html(response);
                                    }
                                },
                                failure: function () {
                                    dialog.$container.html('<h1>' + Resources.SERVER_ERROR + '</h1>');
                                }
                            });
                        });
                    }
                }
            });
        });
    },
    initLoginDialog: function() {
    	$('.user-account').on('click', function(e) {
	        e.preventDefault();
	        var url = $(this).attr('href');
	        var redirectUrl = $(this).data('href');
	        dialog.open({
	            url: util.appendParamToURL(url, 'scope', 'dialog'),
	            target: 'loginDialog',
	            options: {
	                dialogClass: 'login-dialog-container',
	                width: util.isMobile() ? '100%' : '90%',
	                open: function() {
	                	initLoginDialogEvents();
	                    $(document).on('click', '#loginDialog button[type="submit"]', function(e){
	                        e.preventDefault();
	                        var form = $(this).closest('form');
	                        if (!form.valid()) {
	                        	return;
	                        }
	                        var data = form.serialize();
	                        data = data + '&' + $(this).attr('name') + '=' + $(this).val();
	                        $.ajax({ 
	                            type: 'POST',
	                            url: util.appendParamToURL(form.attr('action'), 'format', 'ajax'),
	                            data: data,
	                            success: function (response) {
	                                if (typeof response === 'object' && response.success) {
	                                	if (redirectUrl) {
	                                		window.location = redirectUrl;
	                                	} else {
	                                		window.location.reload();
	                                	}	                                  
	                                } else {
	                                    $('#loginDialog').html(response);
	                                    initLoginDialogEvents();
	                                }
	                            },
	                            failure: function () {
	                                dialog.$container.html('<h1>' + Resources.SERVER_ERROR + '</h1>');
	                            }
	                        });
	                    });
	                }
	            }
	        });
	    });
    }
}
function popupLogin(){
	$(".social-login ul li a").off('click.showPopup').on('click.showPopup',function(){
        var id=$(this).attr('data-id');
        var param=getUrlParameter('param');
        if(param==null){
            $("#dwfrm_oauthlogin").find('#'+id).trigger('click');
        }
	});
	$(".btn-close").off('click.hidePopup').on('click.hidePopup',function(){
		$(".popup-login").fadeOut(400,function(){
            $("#dwfrm_oauthlogin").find("input[type='submit']").hide();
        });
	});
}
function missingEmail(){
	var param=getUrlParameter('param');
	if(param=='LoginMissingEmail' || param=='GooglePlus' || param=='Facebook'){
		$(".popup-login").fadeIn(400,function(){
			if(param=='Facebook'){
				$('#Facebook').show();
			}else{
				$('#GooglePlus').show();
			}
        });
	}
}

function initLoginDialogEvents() {
    validator.init();
    login.init();
    $('select').niceSelect();
    $('.login-form .input-text').on('blur keyup', function() {
        if ($("#dwfrm_login").valid()) {
            $('.login-form .button-sigin').prop('disabled', false);  
        } else {
            $('.login-form .button-sigin').prop('disabled', 'disabled');
        }
    });
    $('.login-dialog-success a').on('click', function(e) {
    	e.preventDefault();
    	$('#loginDialog').dialog('close');
    	page.refresh();
    });
}

function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};
module.exports = login;