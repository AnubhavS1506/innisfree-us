'use strict';

var dialog = require('./dialog'),
    product = require('./pages/product'),
    ajax = require('./ajax'),
    util = require('./util'),
    _ = require('lodash');


var makeUrl = function (url, source, productListID) {
    if (source) {
        url = util.appendParamToURL(url, 'source', source);
    }
    if (productListID) {
        url = util.appendParamToURL(url, 'productlistid', productListID);
    }
    return url;
};

var removeParam = function (url) {
    if (url.indexOf('?') !== -1) {
        return url.substring(0, url.indexOf('?'));
    } else {
        return url;
    }
};

function findLastItemOnLine(item, selector) {
    var thisItemTop = item.offset().top;
    var nextAllItems = item.nextAll();
    if (selector) {
        nextAllItems = item.nextAll(selector);
    }
    var lastItemOnLine = item;
    for (var i = 0; i < nextAllItems.length; i++) {
        var loopItem = $(nextAllItems[i]);
        if (loopItem.offset().top > thisItemTop) {
            break;
        }
        lastItemOnLine = loopItem;
    }
    return lastItemOnLine;
}

var quickview = {
    init: function () {
        if (!this.exists()) {
            this.$container = $('<div/>').attr('id', 'QuickViewDialog').appendTo(document.body);
        }
        this.productLinks = $('#search-result-items .thumb-link').map(function (index, thumbLink) {
            return $(thumbLink).attr('href');
        });
        $(document).off('click.closeQuickView').on("click.closeQuickView",'.quickshow .close',function(){
            quickview.closeQuickview();
        });
    },

    setup: function (qvUrl) {
        var $btnNext = $('.quickview-next'),
            $btnPrev = $('.quickview-prev');

        product.initializeEvents();

        this.productLinkIndex = _(this.productLinks).findIndex(function (url) {
            return removeParam(url) === removeParam(qvUrl);
        });

        // hide the buttons on the compare page or when there are no other products
        if (this.productLinks.length <= 1 || $('.compareremovecell').length > 0) {
            $btnNext.hide();
            $btnPrev.hide();
            return;
        }

        if (this.productLinkIndex === this.productLinks.length - 1) {
            $btnNext.attr('disabled', 'disabled');
        }
        if (this.productLinkIndex === 0) {
            $btnPrev.attr('disabled', 'disabled');
        }

        $btnNext.on('click', function (e) {
            e.preventDefault();
            this.navigateQuickview(1);
        }.bind(this));
        $btnPrev.on('click', function (e) {
            e.preventDefault();
            this.navigateQuickview(-1);
        }.bind(this));
    },

    /**
     * @param {Number} step - How many products away from current product to navigate to. Negative number means navigate backward
     */
    navigateQuickview: function (step) {
        // default step to 0
        this.productLinkIndex += (step ? step : 0);
        var url = makeUrl(this.productLinks[this.productLinkIndex], 'quickview');
        dialog.replace({
            url: url,
            callback: this.setup.bind(this, url)
        });
    },

    /**
     * @description show quick view dialog
     * @param {Object} options
     * @param {String} options.url - url of the product details
     * @param {String} options.source - source of the dialog to be appended to URL
     * @param {String} options.productlistid - to be appended to URL
     * @param {Function} options.callback - callback once the dialog is opened
     */
    show: function (options) {
        var url;
        if (!this.exists()) {
            this.init();
        }
        url = makeUrl(options.url, options.source, options.productlistid);
        $('select').niceSelect();

        var $productTile = options.productTile;
        var $elemToAppend;
        //if on listing page, append to last item in same line
        if ($productTile.closest('#search-result-items').length) {
            $elemToAppend = findLastItemOnLine($productTile, '.product-item');
        } else {
            $elemToAppend = $productTile.closest('.product-listing');
        }

        $('.quickshow').remove();
        $elemToAppend.after('<div class="quickshow"></div>');
        ajax.load({
            url: url,
            callback: function (response,err) {
                if (response != null) {
                    var $quickshow = $elemToAppend.next('.quickshow');
                    $quickshow.html(response).slideDown();
                    $productTile.addClass('active-quickshop');
                    var arrowPos = $productTile.offset().left + ($productTile.width()/2) - $quickshow.offset().left;
                    $quickshow.find('.icon-arrow-up').css('left', arrowPos);
                    product.initializeEvents();
                    util.scrollBrowser($productTile.offset().top + $productTile.height());
                    $('select').niceSelect();
                } else {
                    window.alert(Resources.BAD_RESPONSE);
                }
            }
        });
    },
    closeQuickview: function () {
        $('.product-item').removeClass('active-quickshop');
        $('.quickshow').slideUp(150);
    },
    exists: function () {
        return this.$container && (this.$container.length > 0);
    }
};
function closeSignup () {
    $('.account-logged').find(".user-panel").slideUp();
};
$(document).keyup(function(e) {
    if (e.keyCode === 27) {
        quickview.closeQuickview();
        closeSignup();
    }
});
$(document).mouseup(function (e) {
    var $container = $('.quickshow');
    if (!$container.is(e.target) && $container.has(e.target).length === 0) {
        quickview.closeQuickview();
    }
});
module.exports = quickview;
