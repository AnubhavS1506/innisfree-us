'use strict';

var naPhone = /^\(?([2-9][0-8][0-9])\)?[\-\. ]?([2-9][0-9]{2})[\-\. ]?([0-9]{4})(\s*x[0-9]+)?$/;
var regex = {
    phone: {
        us: naPhone,
        ca: naPhone,
        fr: /^0[1-6]{1}(([0-9]{2}){4})|((\s[0-9]{2}){4})|((-[0-9]{2}){4})$/,
        it: /^(([0-9]{2,4})([-\s\/]{0,1})([0-9]{4,8}))?$/,
        jp: /^(0\d{1,4}- ?)?\d{1,4}-\d{4}$/,
        cn: /.*/,
        gb: /^((\(?0\d{4}\)?\s?\d{3}\s?\d{3})|(\(?0\d{3}\)?\s?\d{3}\s?\d{4})|(\(?0\d{2}\)?\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$/
    },
    postal: {
        us: /^\d{5}(-\d{4})?$/,
        ca: /^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$/,
        fr: /^(F-)?((2[A|B])|[0-9]{2})[0-9]{3}$/,
        it: /^([0-9]){5}$/,
        jp: /^([0-9]){3}[-]([0-9]){4}$/,
        cn: /^([0-9]){6}$/,
        gb: /^([A-PR-UWYZ0-9][A-HK-Y0-9][AEHMNPRTVXY0-9]?[ABEHMNPRVWXY0-9]? {1,2}[0-9][ABD-HJLN-UW-Z]{2}|GIR 0AA)$/
    },
    notCC: /^(?!(([0-9 -]){13,19})).*$/,
    email: /^[\w.%+-]+@[\w.-]+\.[\w]{2,6}$/,
    password: /^(?=.*[A-Za-z])(?=.*[\d$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{6,}$/
};
// global form validator settings
var settings = {
    errorClass: 'error_message',
    errorElement: 'p',
    onkeyup: false,
    onfocusout: function (element) {
        if (!this.checkable(element)) {
            this.element(element);
        }
    },
    errorPlacement: function(error, element) {
        //Custom position: second name
        if (element.attr("name").indexOf('customer_isAcceptRewardProgram') > -1 || element.attr("name").indexOf('customer_isAcceptTermsAndConditions') > -1) {
            error.insertAfter(element.closest('div').find('label.checkbox'));
        }
        // Default position: if no match is met (other fields)
        else {
            error.insertAfter(element);
        }
    }
};

/**
 * @function
 * @description Validates a given password 
 * @param {String} value The password which will be validated 
 * must contain eight or more characters and must contain 2 of the following: Capital, Lowercase, Numbers, Symbol.
 * @param {String} el The input field
 */
var validatePassword = function (value, el) {
    var isOptional = this.optional(el);
    var isValid = regex.password.test($.trim(value));
    return isOptional || isValid;
};

/**
 * @function
 * @description Validates a given email
 * @param {String} value The email which will be validated
 * @param {String} el The input field
 */
var validateEmail = function (value, el) {
    var isOptional = this.optional(el);
    var isValid = regex.email.test($.trim(value));

    return isOptional || isValid;
};

/**
 * @function
 * @description Validates a given phone number against the countries phone regex
 * @param {String} value The phone number which will be validated
 * @param {String} el The input field
 */
var validatePhone = function (value, el) {
    var country = $(el).closest('form').find('.country');
    if (country.length === 0 || country.val().length === 0 || !regex.phone[country.val().toLowerCase()]) {
        return true;
    }

    var rgx = regex.phone[country.val().toLowerCase()];
    var isOptional = this.optional(el);
    var isValid = rgx.test($.trim(value));

    return isOptional || isValid;
};

/**
 * @function
 * @description Validates that a credit card owner is not a Credit card number
 * @param {String} value The owner field which will be validated
 * @param {String} el The input field
 */
var validateOwner = function (value) {
    var isValid = regex.notCC.test($.trim(value));
    return isValid;
};

/**
 * @function
 * @description Validates that a password confirm must be match with password
 * @param {String} value The owner field which will be validated
 * @param {String} el The input field
 */
var validatePasswordConfirm = function (value, el) {
    var isOptional = this.optional(el);
    var isValid = false;
    if ($.trim(value) == $(el).parentsUntil('form').find('input.password').val()) {
        isValid = true;
    }
    return isOptional || isValid;
};

/**
 * Add password validation method to jQuery validation plugin.
 * Text fields must have 'passwordregister' css class to be validated as phone
 */
$.validator.addMethod('passwordregister', validatePassword, Resources.VALIDATE_PASSWORD);

/**
 * Add email validation method to jQuery validation plugin.
 * Text fields must have 'email' css class to be validated as phone
 */
$.validator.addMethod('email', validateEmail, Resources.VALIDATE_EMAIL);
/**

/**
 * Add phone validation method to jQuery validation plugin.
 * Text fields must have 'phone' css class to be validated as phone
 */
$.validator.addMethod('phone', validatePhone, Resources.INVALID_PHONE);

/**
 * Add CCOwner validation method to jQuery validation plugin.
 * Text fields must have 'owner' css class to be validated as not a credit card
 */
$.validator.addMethod('owner', validateOwner, Resources.INVALID_OWNER);

/**
 * Add gift cert amount validation method to jQuery validation plugin.
 * Text fields must have 'gift-cert-amont' css class to be validated
 */
$.validator.addMethod('gift-cert-amount', function (value, el) {
    var isOptional = this.optional(el);
    var isValid = (!isNaN(value)) && (parseFloat(value) >= 5) && (parseFloat(value) <= 5000);
    return isOptional || isValid;
}, Resources.GIFT_CERT_AMOUNT_INVALID);

/**
 * Add positive number validation method to jQuery validation plugin.
 * Text fields must have 'positivenumber' css class to be validated as positivenumber
 */
$.validator.addMethod('positivenumber', function (value) {
    if ($.trim(value).length === 0) { return true; }
    return (!isNaN(value) && Number(value) >= 0);
}, ''); // '' should be replaced with error message if needed

/**
 * Add confirm password validation method to jQuery validation plugin.
 * Text fields must have 'passwordconfirm' css class to be validated as passwordconfirm
 */
$.validator.addMethod('passwordconfirm', validatePasswordConfirm, Resources.VALIDATE_PASSWORD_MATCH);

$.extend($.validator.messages, {
    required: Resources.VALIDATE_REQUIRED,
    remote: Resources.VALIDATE_REMOTE,
    email: Resources.VALIDATE_EMAIL,
    url: Resources.VALIDATE_URL,
    date: Resources.VALIDATE_DATE,
    dateISO: Resources.VALIDATE_DATEISO,
    number: Resources.VALIDATE_NUMBER,
    digits: Resources.VALIDATE_DIGITS,
    creditcard: Resources.VALIDATE_CREDITCARD,
    equalTo: Resources.VALIDATE_EQUALTO,
    maxlength: $.validator.format(Resources.VALIDATE_MAXLENGTH),
    minlength: $.validator.format(Resources.VALIDATE_MINLENGTH),
    rangelength: $.validator.format(Resources.VALIDATE_RANGELENGTH),
    range: $.validator.format(Resources.VALIDATE_RANGE),
    max: $.validator.format(Resources.VALIDATE_MAX),
    min: $.validator.format(Resources.VALIDATE_MIN),
    passwordconfirm : Resources.VALIDATE_PASSWORD_MATCH
});

var validator = {
    regex: regex,
    settings: settings,
    init: function () {
        var self = this;
        $('form:not(.suppress)').each(function () {
            $(this).validate(self.settings);
        });
    },
    initForm: function (f) {
        $(f).validate(this.settings);
    }
};

module.exports = validator;
