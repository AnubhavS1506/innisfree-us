'use strict';

var util = require('../util');

var pointHistoryEvents = function () {
    $('body').on('click', '.point-history-button.fetch', function() {
    	var url = Urls.requestPointHistory;
    	$('.point-history-wrapper').addClass('active');
    	$('.membership-points-des,.account-point-history, .point-history-options').addClass('hide');
    	$('.membership-desc').removeClass('hide');
    	$.ajax({
    		type: "POST",
            url: url 
    	})
    	.done(function(data) {
    		 setTimeout(function(){
    			$('.point-history-wrapper').removeClass('active');
    			$('.point-history-wrapper').html(data);
    			$('.point-history-button-check').addClass('hide');
    			$('.membership-points-des, .account-point-history,.point-history-button-refresh, .point-history-options').removeClass('hide');
            }, 500);
    	})
    	.fail(function() {
    		$('.point-history-wrapper').removeClass('active');
    		console.log("AJAX call failed....");
    	});
    });

    $('body').on('click', '.pagination a', function(e) {
    	var url = $(this).attr('href');
    	var orderPurchaseHistory = Urls.orderPurchaseHistory;
    	var orderHistory = Urls.orderHistory;
    	if ((url.indexOf(orderPurchaseHistory) == -1) && (url.indexOf(orderHistory) == -1)) {
    		e.preventDefault();
    		$.ajax({
        		type: "POST",
                url: url 
        	})
        	.done(function(data) {
        		$('.point-history-wrapper').html(data);
        	})
        	.fail(function() {
        		console.log("AJAX call failed....");
        	});
    	}
    });
};

exports.init = pointHistoryEvents;