'use strict';
var util = require('../util'),
image = require('./product/image'),
minicart = require('../minicart');
function initializeMyCuishonsEvents() {

	
	var pagecarousel = $('.mc-page-vertical-carousel-root');

	pagecarousel.on('jcarousel:reload jcarousel:create', function() {
	    $(pagecarousel._element).height(pagecarousel.jcarousel('items').eq(0).height());
	}).on('jcarousel:targetin', '.mc-page-vertical-carousel-list-item', function(event, pagecarousel) {
	    $(pagecarousel._element).height('auto').height($(this).height());
	}).jcarousel({
	    vertical: true,
	    wrap: 'circular'
	});  
	    
    $('#previous').on('click',function(e){
        e.preventDefault();
        $('.mc-page-vertical-carousel-root').jcarousel('scroll', '-=1');
    });
    $('#next').on('click',function(e){
        e.preventDefault();
        $('.mc-page-vertical-carousel-root').jcarousel('scroll', '+=1');
    });
	
	$('.mc-goto-page-mc-landing').on('click',function(e){
		$('.mc-page-vertical-carousel-root').jcarousel('scroll', '0');
	});
	$('.mc-shade-swatchanchor').on('click', function(e){
		e.preventDefault();
		if($('#YMK-module').css('display').length > 0 || $('#YMK-module').css('display') == 'block'){
			YMK.applyMakeupBySku($(this).data('variantProductYoucamGuid'));
		}
	});
	
	 
	
	$('.mc-goto-page-choose-shade, .mc-shade-swatchanchor, #mc-header-section-shade').off("click").on('click',function(e){
		var $this = $(this);
		if($this.is('#mc-header-section-shade') && $this.hasClass('active')){
			return true;
		}
		var isShadeSelectedFromSwatch = $this.hasClass( 'mc-shade-swatchanchor' );
		if(isShadeSelectedFromSwatch){
			$('img.mc-product-swatch').removeClass('active');
			$this.find('img.mc-product-swatch').addClass('active');
			//YMK.applyMakeupBySku('BCC-INNUS_20171220_FD_01_'+shadeVariantIndex);
			YMK.applyMakeupBySku($this.data('variantProductYoucamGuid'));
		}else{
			if (typeof(YMK) !== 'undefined' && YMK){
				YMK.init();
			}
		}
		var shadeProductType = '',variantId = '';
		if($this.parents('#mc-landing-aqua-section').length > 0){
			shadeProductType='aqua';
		}else
			if($this.parents('#mc-landing-matte-section').length > 0){
				shadeProductType='matte';
			}else
				if($this.data('variantId')){
					variantId = $this.data('variantId');
				}
		var target = $('#mc-shade-wrapper');
		$.ajax({
            url: util.appendParamsToUrl(Urls.myCuishonShowShades, {
            	shadeProductType: shadeProductType,
            	pid : variantId
            }), 
            method: 'GET',
            datatype: 'html'
        }).done(function(data) {
        	if(!isShadeSelectedFromSwatch){
	    		target.html(data);
	    	}else{
	    		var returnedHTML = $(data);
	    		var x = 10;
	    		target.find('div.mc-product-header').html(returnedHTML.find('div.mc-product-header').html());
	    		$('.mc-product-container-left .primary-image').attr({
    		        src: returnedHTML.find('img.primary-image').attr("src"),
    		        alt: returnedHTML.find('img.primary-image').attr("alt"),
    		        title: returnedHTML.find('img.primary-image').attr("title"),
    		    });
	    		target.find('div#thumbnails').html(returnedHTML.find('div#thumbnails').html());
	    		target.find('div#mc-shade-product-master-title').html(returnedHTML.find('div#mc-shade-product-master-title').html());
	    		target.find('div#mc-shade-product-variant-price').html(returnedHTML.find('div#mc-shade-product-variant-price').html());
	    		target.find('div#mc-shade-product-variant-name').html(returnedHTML.find('div#mc-shade-product-variant-name').html());
	    		target.find('div#mc-shade-product-variant-description').html(returnedHTML.find('div#mc-shade-product-variant-description').html());
	    		target.find('div#mc-shade-mobile-title').html(returnedHTML.find('div#mc-shade-mobile-title').html());
	    		target.find('div#mc-shade-mobile-image-container').html(returnedHTML.find('div#mc-shade-mobile-image-container').html());
	    		target.find('div#mc-shade-mobile-product-name-price').html(returnedHTML.find('div#mc-shade-mobile-product-name-price').html());
	    		target.find('#mc-goto-applicator-next').data("selected-variant-id", returnedHTML.find('#mc-goto-applicator-next').data('selected-variant-id'));
	    		target.find('#mc-goto-applicator-prev').data("selected-variant-id", returnedHTML.find('#mc-goto-applicator-prev').data('selected-variant-id'));
	    	}
	        initializeMyCuishonsEvents();
	        $('.mc-page-vertical-carousel-root').jcarousel('scroll', '1');
    		
        });
 
	 	
	}); 
	
	$('.you-cam-thumbnail').off("click").on('click',function(e){
		if($('#YMK-module').css('display').length <= 0 || $('#YMK-module').css('display') != 'block'){
			window.YMK.open();
			$('#mc-youcam-screen').css('display','block');
			$('#mc-shade-mobile-product-name-price, #mc-shade-product-description-mobile').css('display','none');
			$('#you-cam-open-close-buttons').css('top','497px');
			$(this).text('Close');
		}else{
			window.YMK.close();
			$('#mc-youcam-screen').css('display','none');
			$('#mc-shade-mobile-product-name-price, #mc-shade-product-description-mobile').css('display','block');
			$('#you-cam-open-close-buttons').css('top','424px');
			$(this).text('Live Shade Finder!');
		}
	});
	
	$('.mc-goto-page-choose-applicator, .mc-applicator-swatchanchor, #mc-header-section-applicator').off("click").on('click',function(e){
		var $this = $(this);
		var isApplicatorPageSelectedFromMobileSwatch = $this.parent().hasClass( 'mc-mobile-applicator-jcarousel-list-item' );
		if(isApplicatorPageSelectedFromMobileSwatch){
			$('#mc-mobile-applicator-jcarousel-root').jcarousel('scroll', $this.parent().data('scrollIndex'));
		}
		if($this.is('#mc-header-section-applicator') && $this.hasClass('active')){
			return true;
		}
		var variantId = '',saveShadeIdInSession = false,shadeVariantIdToBeSaved = '';
		if($this.hasClass('mc-goto-page-choose-applicator') && $this.hasClass('mc-product-next-button')){
			saveShadeIdInSession = true;
			shadeVariantIdToBeSaved = $this.data('selectedVariantId');
		}
		if($this.data('variantId')){
			variantId = $this.data('variantId');
		}
		var target = $('#mc-applicator-wrapper');
		$.ajax({
            url: util.appendParamsToUrl(Urls.myCuishonShowApplicator, {
            	pid : variantId,
            	saveShadeIdInSession : saveShadeIdInSession,
            	shadeVariantIdToBeSaved : shadeVariantIdToBeSaved
            }),
            method: 'GET',
            datatype: 'html'
        }).done(function(data) {
			if(isApplicatorPageSelectedFromMobileSwatch){
				var returnedHTML = $(data);
        		target.find('div.mc-product-header').html(returnedHTML.find('div.mc-product-header').html());
        		target.find('div#mc-applicator-mobile-product-name').html(returnedHTML.find('div#mc-applicator-mobile-product-name').html());
        		target.find('div#mc-applicator-mobile-product-price').html(returnedHTML.find('div#mc-applicator-mobile-product-price').html());
        		target.find('div#mc-applicator-product-description-mobile').html(returnedHTML.find('div#mc-applicator-product-description-mobile').html());
        		target.find('a.mc-goto-page-choose-case').data('selectedVariantId', returnedHTML.find('a.mc-goto-page-choose-case').data('selectedVariantId'));
				
			}
			else{
				target.html(data);
            
			}
            initializeMyCuishonsEvents();
            $('.mc-page-vertical-carousel-root').jcarousel('scroll', '2');
    		
        });


	});
	
	$('.mc-goto-page-choose-case, .mc-case-swatchanchor, #mc-header-section-case').off("click").on('click',function(e){
		var $this = $(this);
		var isCasePageSelectedFromSwatch = $this.hasClass( 'mc-case-swatchanchor' );
		if(isCasePageSelectedFromSwatch){
			$('.carousel-stage').find('img.mc-product-swatch.swatchanchor.active').removeClass('active')
			$('#mc-case-product-all-swatch-mobile').find('img.mc-product-swatch.swatchanchor.active').removeClass('active')
			$(this).find('img.mc-product-swatch.swatchanchor').addClass('active');
		}
		if($this.is('#mc-header-section-case') && $this.hasClass('active')){
			return true;
		}
		var variantId = '',saveApplicatorIdInSession = false,applicatorVariantIdToBeSaved = '';
		if($this.hasClass('mc-goto-page-choose-case') && $this.hasClass('mc-product-next-button')){
			saveApplicatorIdInSession = true;
			applicatorVariantIdToBeSaved = $this.data('selectedVariantId');
		}
		if($this.data('variantId')){
			variantId = $this.data('variantId');
		}
		var target = $('#mc-case-wrapper');
		$.ajax({
            url: util.appendParamsToUrl(Urls.myCuishonShowCase, {
            	pid : variantId,
            	saveApplicatorIdInSession : saveApplicatorIdInSession,
            	applicatorVariantIdToBeSaved : applicatorVariantIdToBeSaved
            }),
            method: 'GET',
            datatype: 'html'
        }).done(function(data) {
        	if(isCasePageSelectedFromSwatch){
        		var returnedHTML = $(data);
        		target.find('div.mc-product-header').html(returnedHTML.find('div.mc-product-header').html());
        		target.find('div#mc-case-product-master-title').html(returnedHTML.find('div#mc-case-product-master-title').html());
        		target.find('div#mc-case-product-variant-price').html(returnedHTML.find('div#mc-case-product-variant-price').html());
        		target.find('div#mc-case-container-image').html(returnedHTML.find('div#mc-case-container-image').html());
        		target.find('div#mc-case-product-name-price').html(returnedHTML.find('div#mc-case-product-name-price').html());
				target.find('a.mc-goto-page-choose-finish').data('selectedVariantId', returnedHTML.find('a.mc-goto-page-choose-finish').data('selectedVariantId'));
				}else{
        		target.html(data);
        	}
            initializeMyCuishonsEvents();
            $('.mc-page-vertical-carousel-root').jcarousel('scroll', '3');
    		
        });


	});
	
	$('.mc-goto-page-choose-finish, .mc-finish-swatchanchor, #mc-header-section-finish').off("click").on('click',function(e){
		var $this = $(this);
//		if($this.is('#mc-header-section-finish') 
//				&& ( $this.hasClass('active') || $this.hasClass('mc-final-unclickable') ) ){
//			return true;
//		}
		var variantId = '',saveCaseIdInSession = false,caseVariantIdToBeSaved = '';
		if($this.hasClass('mc-goto-page-choose-finish') && $this.hasClass('mc-product-next-button')){
			saveCaseIdInSession = true;
			caseVariantIdToBeSaved = $this.data('selectedVariantId');
		}
		if($this.data('variantId')){
			variantId = $this.data('variantId');
		}
		var target = $('#mc-finish-wrapper');
		$.ajax({
            url: util.appendParamsToUrl(Urls.myCuishonShowFinish, {
            	pid : variantId,
            	saveCaseIdInSession : saveCaseIdInSession,
            	caseVariantIdToBeSaved : caseVariantIdToBeSaved
            }),
            method: 'GET',
            datatype: 'html'
        }).done(function(data) {
            target.html(data);
            initializeMyCuishonsEvents();
            $('.mc-page-vertical-carousel-root').jcarousel('scroll', '4');
    		
        });


	});

	$('#mc-finish-add-all-to-cart-button').off("click").on('click',function(e){
			var $this = $(this);
			var target = $('#mc-success-wrapper');
			$.ajax({
	            url: util.appendParamsToUrl(Urls.myCuishonAddAllToCart, {}),
	            method: 'GET',
	            datatype: 'html'
	        }).done(function(data) {
	            target.html(data);
	            initializeMyCuishonsEvents();
	            $('.mc-page-vertical-carousel-root').jcarousel('scroll', '5');
				minicart.show(data.substring(data.indexOf('<div id="mc-minicart-content">') + 31,data.length -6));
	            $('#mc-minicart-content').hide();
	        });


	});
	
	$('.mc-finish-lineitem-remove').off("click").on('click',function(e){
		var $this = $(this);
		var mcProductToBeRemoved = '';
		if($this.parents('.mc-finish-lineitem-shade').length > 0){
			mcProductToBeRemoved='shade';
		}else 
			if($this.parents('.mc-finish-lineitem-applicator').length > 0){
				mcProductToBeRemoved='applicator';
			}else
				if($this.parents('.mc-finish-lineitem-case').length > 0){
					mcProductToBeRemoved='case';
				}
		var target = $('#mc-finish-wrapper');
		$.ajax({
            url: util.appendParamsToUrl(Urls.myCuishonRemoveMcLineItem, {
            	mcProductToBeRemoved : mcProductToBeRemoved
            }),
            method: 'GET',
            datatype: 'html'
        }).done(function(data) {
            target.html(data);
            initializeMyCuishonsEvents();
            $('.mc-page-vertical-carousel-root').jcarousel('scroll', '4');
        });
	});
	
	$('.mc-start-over').off("click").on('click',function(e){
		e.preventDefault();
		$.ajax({
            url: util.appendParamsToUrl(Urls.myCuishonShowMcLanding, {}),
            method: 'GET',
            datatype: 'html'
        }).done(function(data) {
        	window.location.href = util.appendParamsToUrl(Urls.searchShow, {cgid:'my-diy-compact'});
        });
	});
	
	
	
	
	 $('#mc-case-swatch-carousel').jcarousel();
	 $('#mc-case-swatch-carousel .jcarousel-prev')
	     .on('jcarouselcontrol:active', function () {
	         $(this).removeClass('inactive');
	     })
	     .on('jcarouselcontrol:inactive', function () {
	         $(this).addClass('inactive');
	     })
	     .jcarouselControl({
	         target: '-=1'
	     });
	
	 $('#mc-case-swatch-carousel .jcarousel-next')
	     .on('jcarouselcontrol:active', function () {
	         $(this).removeClass('inactive');
	     })
	     .on('jcarouselcontrol:inactive', function () {
	         $(this).addClass('inactive');
	     })
	     .jcarouselControl({
	         target: '+=1'
	     });
	
	$('.thumbnail-link').off("click").on('click',function(e){
		e.preventDefault();
	});
	$('div.mc-product-container-left').on('click', '.productthumbnail', function () {
		if (typeof(YMK) !== 'undefined' && YMK){
			YMK.close();
		}
		// switch indicator
        $(this).closest('.product-thumbnails').find('.thumb.selected').removeClass('selected');
        $(this).closest('.thumb').addClass('selected');
        //removed as this was closing the iframe 
        //image.removeVideo();
        //image.vCenter();
        //image.aCenter();
        image.setMainImage($(this).data('lgimg'));
        image.loading($('.product-primary-image .img'));
        $('.main-image').imagesLoaded( function() {
        	image.destroyLoading();
        });
    });
    $('div.mc-product-container-left').on('click', '.video .productvideo', function () {
        $(this).closest('.product-thumbnails').find('.thumb.selected').removeClass('selected');
        $(this).closest('.thumb').addClass('selected');
        if($('.product-primary-image').find('iframe').length == 0){
        	image.displayYouTubeVideo($(this));
        	image.vCenter();
        	image.aCenter();
        }
    });
	
    
    var connector = function(itemNavigation, carouselStage) {
        return carouselStage.jcarousel('items').eq(itemNavigation.index());
    };

    $(function() {
        // Setup the carousels. Adjust the options for both carousels here.
        var carouselStage      = $('.carousel-stage').jcarousel();
        var carouselNavigation = $('.carousel-navigation').jcarousel();

        // We loop through the items of the navigation carousel and set it up
        // as a control for an item from the stage carousel.
        carouselNavigation.jcarousel('items').each(function() {
            var item = $(this);

            // This is where we actually connect to items.
            var target = connector(item, carouselStage);

            item
                .on('jcarouselcontrol:active', function() {
                    carouselNavigation.jcarousel('scrollIntoView', this);
                    item.addClass('jcarousel-active');
                })
                .on('jcarouselcontrol:inactive', function() {
                    item.removeClass('jcarousel-active');
                })
                .jcarouselControl({
                    target: target,
                    carousel: carouselStage
                });
        });

        // Setup controls for the stage carousel
        $('.prev-stage')
            .on('jcarouselcontrol:inactive', function() {
                $(this).addClass('jcarousel-inactive');
            })
            .on('jcarouselcontrol:active', function() {
                $(this).removeClass('jcarousel-inactive');
            })
            .jcarouselControl({
                target: '-=1'
            });

        $('.next-stage')
            .on('jcarouselcontrol:inactive', function() {
                $(this).addClass('jcarousel-inactive');
            })
            .on('jcarouselcontrol:active', function() {
                $(this).removeClass('jcarousel-inactive');
            })
            .jcarouselControl({
                target: '+=1'
            });

        // Setup controls for the navigation carousel
        $('.prev-navigation')
            .on('jcarouselcontrol:inactive', function() {
                $(this).addClass('jcarousel-inactive');
            })
            .on('jcarouselcontrol:active', function() {
                $(this).removeClass('jcarousel-inactive');
            })
            .jcarouselControl({
                target: '-=1'
            });

        $('.next-navigation')
            .on('jcarouselcontrol:inactive', function() {
                $(this).addClass('jcarousel-inactive');
            })
            .on('jcarouselcontrol:active', function() {
                $(this).removeClass('jcarousel-inactive');
            })
            .jcarouselControl({
                target: '+=1'
            });
        var initalActiveindex = 0;
    	
        $('.carousel-stage').find('li').each(function(){
        	var $li=$(this); 
        	if($li.find('.active').length>0){
        		carouselStage.jcarousel('scrollIntoView', initalActiveindex);
        		return false; 
        	}
        	initalActiveindex++;
        });
        
        
    });
    
    $('#mc-mobile-shade-image-jcarousel').jcarousel({
        wrap: 'circular'
    });
    
    $('#mc-mobile-shade-image-jcarousel').on( "mousemove", function(e){
        e.preventDefault();
        $('#mc-mobile-shade-image-jcarousel').jcarousel('scroll', '+=1');
    } );
    
    $('.mc-mobile-shade-image-jcarousel-dots').on('click','a',function(e){
        e.preventDefault();
        var scrollTo = $(this).data('scrollIndex');
        $('#mc-mobile-shade-image-jcarousel').jcarousel('scroll', scrollTo.toString());
    });
    
    $('#mc-mobile-shade-image-jcarousel').on('jcarousel:targetin', 'li', function(event, carousel) {
        // "this" refers to the item element
        var targetDot = $(this).data('scrollIndex');
        $('.dot-'+targetDot).addClass('active');
        // "carousel" is the jCarousel instance
    });
   $('#mc-mobile-shade-image-jcarousel').on('jcarousel:targetout', 'li', function(event, carousel) {
        // "this" refers to the item element
        var targetDot = $(this).data('scrollIndex');
        $('.dot-'+targetDot).removeClass('active');
        // "carousel" is the jCarousel instance
    });
   $('#mc-mobile-shade-image-jcarousel').jcarousel('scroll', '0');
   
   $('#mc-mobile-shade-image-jcarousel').touchwipe({
       wipeLeft: function() { 
    	   $('#mc-mobile-shade-image-jcarousel').jcarousel('scroll', '+=1');
       },
       wipeRight: function() {
    	   $('#mc-mobile-shade-image-jcarousel').jcarousel('scroll', '-=1');
       }
   });
   $('#mc-shade-images-jcarousel-root').jcarousel({
	   visible: 3
	   });
    $('#mc-mobile-applicator-jcarousel-root').jcarousel({
//        wrap: 'circular',
        center: true
    });
    /*$('#mc-mobile-applicator-jcarousel-previous').on('click',function(e){
        e.preventDefault();
        $('#mc-mobile-applicator-jcarousel-root').jcarousel('scroll', '-=1');
    });
    $('#mc-mobile-applicator-jcarousel-next').on('click',function(e){
        e.preventDefault();
        $('#mc-mobile-applicator-jcarousel-root').jcarousel('scroll', '+=1');
    });
    
    $('.mc-mobile-applicator-jcarousel-list-item').on('click',function(e){
        e.preventDefault();
        $('#mc-mobile-applicator-jcarousel-root').jcarousel('scroll', $(this).data('scrollIndex'));
    });*/
    
    $('#mc-mobile-applicator-jcarousel-root').on('jcarousel:targetin', '.mc-mobile-applicator-jcarousel-list-item', function(event, carousel) {
        // "this" refers to the item element
        $(this).find('img').css({'width':'145px','height':'145px'});
        $(this).addClass('slide_active');
        $(this).find('.mc-applicator-swatchanchor').find('.mc-product-swatch').addClass('active');
        
        
        var target = $('#mc-applicator-wrapper');
		$.ajax({
            url: util.appendParamsToUrl(Urls.myCuishonShowApplicator, {
            	pid : $(this).data('variantId')
            }),
            method: 'GET',
            datatype: 'html'
        }).done(function(data) {
        	var returnedHTML = $(data);
    		target.find('div.mc-product-header').html(returnedHTML.find('div.mc-product-header').html());
    		target.find('div#mc-applicator-mobile-product-name').html(returnedHTML.find('div#mc-applicator-mobile-product-name').html());
    		target.find('div#mc-applicator-mobile-product-price').html(returnedHTML.find('div#mc-applicator-mobile-product-price').html());
    		target.find('div#mc-applicator-product-description-mobile').html(returnedHTML.find('div#mc-applicator-product-description-mobile').html());
    		target.find('a.mc-goto-page-choose-case').data('selectedVariantId', returnedHTML.find('a.mc-goto-page-choose-case').data('selectedVariantId'));
            initializeMyCuishonsEvents();
        });
        // "carousel" is the jCarousel instance
    });
   $('#mc-mobile-applicator-jcarousel-root').on('jcarousel:targetout', '.mc-mobile-applicator-jcarousel-list-item', function(event, carousel) {
        // "this" refers to the item element
        $(this).find('img').css({'width':'100px','height':'100px'});
		$(this).removeClass('slide_active');
		$(this).find('.mc-applicator-swatchanchor').find('.mc-product-swatch').removeClass('active');
        // "carousel" is the jCarousel instance
    });
   $('#mc-mobile-applicator-jcarousel-root').touchwipe({
       wipeLeft: function() {
    	   $('#mc-mobile-applicator-jcarousel-root').jcarousel('scroll', '+=1');
       },
       wipeRight: function() {
    	   $('#mc-mobile-applicator-jcarousel-root').jcarousel('scroll', '-=1');
       }
   });
    
    //$('#mc-mobile-applicator-jcarousel-root').jcarousel('scroll', '1');
	
	
	var initalApplicatorActiveindex = 0;
   
   var carouselCaseMobile      = $('#mc-mobile-applicator-jcarousel-root').jcarousel();
   $('#mc-mobile-applicator-jcarousel-list').find('.mc-applicator-swatchanchor').each(function(){
   	var $li=$(this); 
   	if($li.find('.active').length>0){
		return false; 
   	}
   	initalApplicatorActiveindex++;
   });
   //
   if(initalApplicatorActiveindex==0)
	   {
	   
	   var first = $('#mc-mobile-applicator-jcarousel-root').jcarousel('first');
	   first.find('img').css({'width':'145px','height':'145px'});
	   first.addClass('slide_active');
	   first.find('.mc-applicator-swatchanchor').find('.mc-product-swatch').addClass('active');
	   }
   else
	   $('#mc-mobile-applicator-jcarousel-root').jcarousel('scroll', initalApplicatorActiveindex.toString());
    
    $('#mc-mobile-case-jcarousel-root').jcarousel({
        center: true
    });
    $('#mc-mobile-case-jcarousel-previous').on('click',function(e){
        e.preventDefault();
        $('#mc-mobile-case-jcarousel-root').jcarousel('scroll', '-=1');
    });
    $('#mc-mobile-case-jcarousel-next').on('click',function(e){
        e.preventDefault();
        $('#mc-mobile-case-jcarousel-root').jcarousel('scroll', '+=1');
    });
    
    $('#mc-mobile-case-jcarousel-root').touchwipe({
        wipeLeft: function() {
     	   $('#mc-mobile-case-jcarousel-root').jcarousel('scroll', '+=1');
        },
        wipeRight: function() {
     	   $('#mc-mobile-case-jcarousel-root').jcarousel('scroll', '-=1');
        }
    });
     
    
    $('.mc-mobile-case-jcarousel-list-item').on('click',function(e){
        e.preventDefault();
        $('#mc-mobile-case-jcarousel-root').jcarousel('scroll', $(this).data('scrollIndex'));
    });
    
    $('#mc-mobile-case-jcarousel-root').on('jcarousel:targetin', '.mc-mobile-case-jcarousel-list-item', function(event, carousel) {
        // "this" refers to the item element
     //   $(this).find('.mc-case-swatches-purple-to-pink').css('display','block');
        var swatchToShow = $(this).data( "swatchId" );
        $('#'+swatchToShow).css('display','block');
        // "carousel" is the jCarousel instance
       // var applicatorProductId = $(this).data('applicatorVariantId');
        $(this).addClass('slide_active');
    });
   $('#mc-mobile-case-jcarousel-root').on('jcarousel:targetout', '.mc-mobile-case-jcarousel-list-item', function(event, carousel) {
        // "this" refers to the item element
	  // $(this).find('.mc-case-swatches-purple-to-pink').css('display','none');
	   var swatchToShow = $(this).data( "swatchId" );
       $('#'+swatchToShow).css('display','none');
	   $(this).removeClass('slide_active');
        // "carousel" is the jCarousel instance
    });
	//$('#mc-mobile-case-jcarousel-root').jcarousel('scroll', '1');
    
	
   
   var initalActiveindex = 0;
   
   var carouselCaseMobile      = $('#mc-mobile-applicator-jcarousel-root').jcarousel();
   $('#mc-case-product-all-swatch-mobile').find('.mc-mobile-case-swatches').each(function(){
   	var $li=$(this); 
   	if($li.find('.active').length>0){
		$li.css('display','block');
   		return false; 
   	}
   	initalActiveindex++;
   });
   if(initalActiveindex==0)
	   {
	   
	   var first = $('#mc-mobile-case-jcarousel-root').jcarousel('first');
	   first.addClass('slide_active');
	   }
   else
	   $('#mc-mobile-case-jcarousel-root').jcarousel('scroll', initalActiveindex.toString());

   //$('#mc-mobile-case-jcarousel-root').jcarousel('scroll', '0');
   
  
}


exports.init = function init() {
	initializeMyCuishonsEvents();
	$(".product-thumbnails .thumbnails-slider").slick({
		   dots: false,
		   infinite: true,
		   slidesToShow: 3,
		   slidesToScroll: 3
		  });
};