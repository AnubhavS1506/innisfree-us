'use strict';

var dialog = require('../dialog');

exports.init = function () {
    $('.main-visual .visual-slider').slick({
        autoplay: true,
        autoplaySpeed: 4700,
        speed: 1000,
        dots: true,
        customPaging: function(slider, i){
            var thumb = i;
            return '<a><span class="side side-left"><span class="fill"></span></span><span class="side side-right"><span class="fill"></span></span></a>';
        }
    });
    // fill animate back
    $('.visual-slider').on('mouseenter', '.slick-track', function(){
        $('.visual-slider').find('.slick-active').addClass('back');
    });
    $('.visual-slider').on('mouseleave', '.slick-track', function(){
        $('.visual-slider').find('.slick-active').removeClass('back');
    });

    $('#vertical-carousel')
        .jcarousel({
            vertical: true
        })
        .jcarouselAutoscroll({
            interval: 5000
        });
    $('#vertical-carousel .jcarousel-prev')
        .on('jcarouselcontrol:active', function () {
            $(this).removeClass('inactive');
        })
        .on('jcarouselcontrol:inactive', function () {
            $(this).addClass('inactive');
        })
        .jcarouselControl({
            target: '-=1'
        });

    $('#vertical-carousel .jcarousel-next')
        .on('jcarouselcontrol:active', function () {
            $(this).removeClass('inactive');
        })
        .on('jcarouselcontrol:inactive', function () {
            $(this).addClass('inactive');
        })
        .jcarouselControl({
            target: '+=1'
        });
    
    var _winW = $(window).width() > 1840 ? 1840 : $(window).width();
    var _winH = $(window).height();
    $(window).resize(function(){
    });

    $(window).bind('scroll', function(){
        _winW = $(window).width();
        var scrollTop = $(window).scrollTop();
        var tmpTop = 2 * scrollTop / 100;
        $('.main-beautytip .bg img').css({'transform' : 'translateY(-'+tmpTop+'px) translateZ(0px)'})
    });

    //Slidedown branner when users visit storefront
    var $brandBannerContainer = $('.brand-banner-container');
    //removed the slide down of the banner on first visit
    //if (!$brandBannerContainer.hasClass('hide')) {
    //    $('.brand-banner-container').slideDown(1000);
    //}
       
    
    //pop up in the main page 
    function setCookie(name, value, expiredays) {
        var todayDate = new Date();
        todayDate.setDate(todayDate.getDate() + expiredays);
        document.cookie = name + "=" + value + "; path=/; expires=" + todayDate.toGMTString() + ";";
    }

    setTimeout(function(){
        var $storefrontNotification = $('#varShowStorefrontNotification');
        var cookiedata = document.cookie;

        if ($storefrontNotification.val() == 'true' && cookiedata.indexOf("showStorefrontNotification=false") < 0) {
            dialog.open({
                url: $storefrontNotification.attr('href'),
                options: {
                    autoOpen: true,
                    modal: true,
                    width: 700,
                    dialogClass: 'home-popup default-popup',
                    close: function() {
                        if ($('#dont_show:checked').length) {
                            setCookie("showStorefrontNotification", "false", 7);
                        }
                    }
                }
            });
        };
    }, 1000);
};
