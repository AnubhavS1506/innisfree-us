'use strict';

var dialog = require('../../dialog'),
    productStoreInventory = require('../../storeinventory/product'),
    tooltip = require('../../tooltip'),
    util = require('../../util'),
    addToCart = require('./addToCart'),
    availability = require('./availability'),
    image = require('./image'),
    productNav = require('./productNav'),
    productSet = require('./productSet'),
    recommendations = require('./recommendations'),
    variant = require('./variant');

/**
 * @description Initialize product detail page with reviews, recommendation and product navigation.
 */
function initializeDom() {
    productNav();
    recommendations();
    tooltip.init();
}

/**
 * @description Initialize event handlers on product detail page
 */
function addImageSwatchToSelect() {
    $('.select-type select').each(function(i) {
        var $ul = $('.select-type ul');
        var $ListLi = $($ul[i]).find('li');
        $(this).find('option').each(function(j) {
            var bg = $(this).css('background-image').replace(/\"/g, '\'');
            var li = $ListLi[j];
            if (!$(li).find('.item-icon').length) {
                $(li).prepend('<span class="item-icon" style="background-image:' + decodeURIComponent(bg) + '"></span>');
                if ($(li).hasClass('selected')) {
                    $(li).closest('.select-type').find('span.current').prepend('<span style="background-image:' + decodeURIComponent(bg) + '"></span>');
                }
            }
            
        })
    });
}
function initializeEvents() {
    var $pdpMain = $('#pdpMain');

    addToCart();
    availability();
    variant();
    image();
    addImageSwatchToSelect();
    productSet();
    if (SitePreferences.STORE_PICKUP) {
        productStoreInventory.init();
    }

    // Add to Wishlist and Add to Gift Registry links behaviors
    $pdpMain.on('click', '[data-action="wishlist"], [data-action="gift-registry"]', function () {
        var data = util.getQueryStringParams($('.pdpForm').serialize());
        if (data.cartAction) {
            delete data.cartAction;
        }
        var url = util.appendParamsToUrl(this.href, data);
        this.setAttribute('href', url);
    });

    // product options
    $pdpMain.on('change', '.product-options select', function () {
        var salesPrice = $pdpMain.find('.product-add-to-cart .price-sales');
        var selectedItem = $(this).children().filter(':selected').first();
        salesPrice.text(selectedItem.data('combined'));
    });

    // prevent default behavior of thumbnail link and add this Button
    $pdpMain.on('click', '.thumbnail-link, .unselectable a', function (e) {
        e.preventDefault();
    });

    $('.size-chart-link a').on('click', function (e) {
        e.preventDefault();
        dialog.open({
            url: $(e.target).attr('href')
        });
    });
    $( "#tabs" ).tabs();
    $('.learn-more .more-title').on('click', function(){
        $(this).toggleClass('active');
        $(this).next().toggleClass('active');
    });
    $('.shades').slick({
        dots: false,
        slidesToShow: 6,
        slidesToScroll: 1,
        responsive: [{
                breakpoint: 768,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            }
        ]
    });
    $('.shades li').on('click', function(){
        $('.shades li, .shades-content li').removeClass('active');
        $(this).addClass('active');
        var content = $(this).attr('data-item');
        $('.shades-content li').filter('[data-item=' + content + ']').addClass('active');
    });
    $('.shades li.slick-current').trigger('click');
    var pointInput = $('.review-grade.large').find('input');
    var reviewTxt = $('.ratings').find('.yotpo-header .stars-wrapper');
    var pointWrite = {
        reset: function(){
            $('.ratings').find('.yotpo-header .stars-wrapper').css({width : (5 - $('.yotpo-header .stars-wrapper span.yotpo-icon-empty-star').length) * 20 + '%'});
        },
        move: function(el, e){
            var reviewPos = el.offset();
            var reviewMouse = Math.round((e.pageX - reviewPos.left) / el.width() * 100);
            el.find('span').css('width', reviewMouse+'%');
        },
        click: function(el, e){
            var reviewPos = el.offset();
            var reviewMouse = Math.round((e.pageX - reviewPos.left) / el.width() * 100);
            for (var i = 0; i < 6; i++) {
                if (reviewMouse <= i * 20) {
                    reviewMouse = i * 20;
                    pointInput.val(i);
                    break;
                }
            }
            el.find('span').stop().animate({width : reviewMouse + '%'}, 400);
            pointInput.focus();
            pointInput.blur();
        }
    }

    $(document).on('mousemove', '.yotpo-header label + div', function(e){
        pointWrite.move($(this), e);
    });
    $(document).on('mouseleave', '.yotpo-header label + div', function(e){
        pointWrite.reset();
    });
    $(document).on('click', '.yotpo-header label + div', function(e){
        pointWrite.click($(this), e);
    });

    $(window).load(function() {
        if (window.location.hash.indexOf('writeReview') > -1) {
            // remove the hash
            window.location.hash = '';

            // create a ID attribute
            var $writeReview = $('.yotpo-default-button.yotpo-icon-btn.pull-right');

            // Scroll to button write a review
            $('html, body').animate({
                scrollTop: $writeReview.offset().top - 100
            }, 500);

            // Show form write review
            $writeReview.trigger('click');
        }
    });

    $('.wrap-button a').on('click', function (e) {
        e.preventDefault();
        var $productSetList = $('#product-set-list');
        $(this).toggleClass( "active" );
        if ($productSetList.is(':visible')) {
            $('.wrap-button a').find('span').html('<i class="fa fa-chevron-down" aria-hidden="true"></i>');
        } else {
            $('.wrap-button a').find('span').html('<i class="fa fa-chevron-up" aria-hidden="true"></i>');
        }
        // scroll to product list set section
		var offset = $productSetList.offset().top - 50;
	  	$('html,body').stop().animate({scrollTop: offset}, 800);
    });

}

var product = {
    initializeEvents: initializeEvents,
    init: function () {
        initializeDom();
        initializeEvents();
    }
};
module.exports.addImageSwatchToSelect = addImageSwatchToSelect;
module.exports = product;