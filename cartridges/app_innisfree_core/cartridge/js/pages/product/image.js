'use strict';
var dialog = require('../../dialog');
var util = require('../../util');
var qs = require('qs');
var url = require('url');
var _ = require('lodash');
var imagesLoaded = require('imagesloaded');

var zoomMediaQuery = matchMedia('(min-width: 960px)');

/**
 * @description Enables the zoom viewer on the product detail page
 * @param zmq {Media Query List}
 */
function loadZoom (zmq) {
    var $imgZoom = $('#pdpMain .main-image'),
        hiresUrl;
    if (!zmq) {
        zmq = zoomMediaQuery;
    }
    if ($imgZoom.length === 0 || dialog.isActive() || util.isMobile() || !zoomMediaQuery.matches) {
        // remove zoom
        $imgZoom.trigger('zoom.destroy');
        return;
    }
    hiresUrl = $imgZoom.attr('href');

    if (hiresUrl && hiresUrl !== 'null' && hiresUrl.indexOf('noimagelarge') === -1 && zoomMediaQuery.matches) {
        $imgZoom.zoom({
            url: hiresUrl
        });
    }
}

zoomMediaQuery.addListener(loadZoom);

/**
 * @description Sets the main image attributes and the href for the surrounding <a> tag
 * @param {Object} atts Object with url, alt, title and hires properties
 */
function setMainImage (atts) {
	$('#pdpMain .primary-image').attr({
        src: atts.url,
        alt: atts.alt,
        title: atts.title
    });
	$('.mc-product-container-left .primary-image').attr({
        src: atts.url,
        alt: atts.alt,
        title: atts.title
    });
    updatePinButton(atts.url);
    if (!dialog.isActive() && !util.isMobile()) {
        $('#pdpMain .main-image').attr('href', atts.hires);
    }
    loadZoom();
}

function updatePinButton (imageUrl) {
    var pinButton = document.querySelector('.share-icon[data-share=pinterest]');
    if (!pinButton) {
        return;
    }
    var newUrl = imageUrl;
    if (!imageUrl) {
        newUrl = document.querySelector('#pdpMain .primary-image').getAttribute('src');
        if(!newUrl){
        	 newUrl = document.querySelector('.mc-product-container-left .primary-image').getAttribute('src');
        }
    }
    var href = url.parse(pinButton.href);
    var query = qs.parse(href.query);
    query.media = url.resolve(window.location.href, newUrl);
    query.url = window.location.href;
    var newHref = url.format(_.extend({}, href, {
        query: query, // query is only used if search is absent
        search: qs.stringify(query)
    }));
    pinButton.href = newHref;
}

/**
 * @description Replaces the images in the image container, for eg. when a different color was clicked.
 */
function replaceImages () {
    var $newImages = $('#update-images'),
        $imageContainer = $('#pdpMain .product-image-container');
    if ($newImages.length === 0) { return; }

    $imageContainer.html($newImages.html());
    $newImages.remove();
    loadZoom();
}

function vCenter(){
	$('.v-center').each(function(){
		$(this).css({marginTop : -$(this).height()/2, top : '50%'});
	});
}
function aCenter(){
	$('.a-center').each(function(){
		$(this).css({marginLeft : -$(this).width()/2, left : '50%'});
	});
}

function removeVideo(){
    $('body').find('.product-primary-image iframe').remove();
    $('body').find('.product-primary-image .main-image').removeClass('hide');
}

function loading(container){
    container.append('<span class="loading"></span>')
}
function destroyLoading(){
    $('.loading').remove();
}
function displayYouTubeVideo(url) {
    var videoID = url.data('vid');
    var iframe = '<iframe width="940" height="540" src="//www.youtube.com/embed/' + videoID + '" frameborder="0" allowfullscreen></iframe>';
    $('.product-primary-image .v-center').append(iframe);
    $('.product-primary-image .main-image').addClass('hide');
    $('.pdt-img-cho-mobile').removeClass('hide');
   
}
/* @module image
 * @description this module handles the primary image viewer on PDP
 **/

/**
 * @description by default, this function sets up zoom and event handler for thumbnail click
 **/
module.exports = function () {
    if (dialog.isActive() || util.isMobile()) {
        $('#pdpMain .main-image').removeAttr('href');
    }
    updatePinButton();
    loadZoom();
    vCenter();
    aCenter();
    // handle product thumbnail click event
    $('#pdpMain,.mc-product-container-left').on('click', '.productthumbnail', function () {
        
    	if (typeof(YMK) !== 'undefined' && YMK){
    		YMK.close();
    	}
    	// switch indicator
        $(this).closest('.product-thumbnails').find('.thumb.selected').removeClass('selected');
        $(this).closest('.thumb').addClass('selected');
        removeVideo();
        vCenter();
        aCenter();
        setMainImage($(this).data('lgimg'));
        loading($('.product-primary-image .img'));
        $('.main-image').imagesLoaded( function() {
            destroyLoading();
        });
    });
    $('#pdpMain,.mc-product-container-left').on('click', '.video .productvideo', function () {
        $(this).closest('.product-thumbnails').find('.thumb.selected').removeClass('selected');
        $(this).closest('.thumb').addClass('selected');
        if($('.product-primary-image').find('iframe').length == 0){
            displayYouTubeVideo($(this));
            vCenter();
            aCenter();
        }
    });
    $(window).resize(function(){
        vCenter();
        aCenter();
    });
};
module.exports.loadZoom = loadZoom;
module.exports.setMainImage = setMainImage;
module.exports.replaceImages = replaceImages;

module.exports.removeVideo=removeVideo;
module.exports.vCenter=vCenter;
module.exports.aCenter=aCenter;
module.exports.setMainImage=setMainImage;
module.exports.loading=loading;
module.exports.destroyLoading=destroyLoading;
