'use strict';

var ajax = require('../../ajax'),
    image = require('./image'),
    progress = require('../../progress'),
    productStoreInventory = require('../../storeinventory/product'),
    tooltip = require('../../tooltip'),
    product = require('./index'),
    util = require('../../util');


/**
 * @description update product content with new variant from href, load new content to #product-content panel
 * @param {String} href - url of the new product variant
 **/
var updateContent = function (href) {
    var $pdpForm = $('.pdpForm');
    var qty = $pdpForm.find('input[name="Quantity"]').first().val();
    var params = {
        Quantity: isNaN(qty) ? '1' : qty,
        format: 'ajax',
        productlistid: $pdpForm.find('input[name="productlistid"]').first().val()
    };

    if(($('#YMK-module').length > 0) && ($('#YMK-module').css('display').length <= 0 || $('#YMK-module').css('display') != 'block')){
    	progress.show($('#pdpMain'));
    }
    ajax.load({
        url: util.appendParamsToUrl(href, params),
        target: $('#product-content'),
        callback: function () {
            if (SitePreferences.STORE_PICKUP) {
                productStoreInventory.init();
            }
            image.replaceImages();
            tooltip.init();
            $('select').niceSelect();
        }
    });
};

var updateContentForCompact = function (href) {
    var url, index;
    try {
    	url = new URL(href);
    	index = url.searchParams.get('index');
    }
    catch (ex) {
    	index = getQueryStringFallBack(href, 'index');
    }
   
    var $pdpForm = $('#form-item-' + index);
    var $setDetail = $pdpForm.closest("div.product-set-details");
    var oldPrice = $setDetail.find("span.price-sales").text();
    var qty = $pdpForm.find('input[name="Quantity"]').first().val();
    
    var params = {
        index: index,
        Quantity: isNaN(qty) ? '1' : qty,
        format: 'ajax'
    };

    progress.show($('#pdpMain'));

    ajax.load({
        url: util.appendParamsToUrl(href, params),
        target: $('.product-set-item-detail-item-' + index),
        callback: function (response) {
            $('select').niceSelect();
            product.addImageSwatchToSelect();
            updateProductSetPrice(oldPrice, response);
            updateAddToCartButton();
        }
    });
};

var updateAddToCartButton = function() {
    var $addToCart = $('#add-to-cart'),
    $addAllToCart = $('div.product-set-add-all-cart button'),
    $productSetList = $('#product-set-list');

    if ($productSetList.find('.add-to-cart[disabled]').length > 0) {
        $addAllToCart.attr('disabled', 'disabled');
        // product set does not have an add-to-cart button, but product bundle does
        $addToCart.attr('disabled', 'disabled');
        if(!$('.compactPrice').hasClass('hide')){
            $('.compactPrice').addClass('hide');
        }
    } else {
        $addAllToCart.removeAttr('disabled');
        $addToCart.removeAttr('disabled');
        if($('.compactPrice').hasClass('hide')){
            $('.compactPrice').removeClass('hide');
        }
    }
}

var getQueryStringFallBack = function(url, prop)
{
    var vars = [], hash;
    var hashes = url.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars[prop];
}

var updateProductSetPrice = function (oldPrice, response) {
    try {
        var $response = $(response);
        var newPrice = $response.find("span.price-sales").text();
        if (oldPrice != newPrice) {
            var setPrice = $("div.salesprice").text();
            var newSetPrice = calculateProductSetPrice(oldPrice, newPrice, setPrice);
            $("div.salesprice").text(newSetPrice);
        }
    }
    catch (ex) {
        console.log("update product set error : ", ex)
    }   
}

var calculateProductSetPrice = function (_oldPrice, _newPrice, _setPrice) {
    var oldPrice = parseFloat(_oldPrice.match(/(\D+)(\d+\.*\d*)/)[2]);
    var newPrice = parseFloat(_newPrice.match(/(\D+)(\d+\.*\d*)/)[2]);
    var setPrice = parseFloat(_setPrice.match(/(\D+)(\d+\.*\d*)/)[2]);
    var newSetPrice = (setPrice + (newPrice - oldPrice)).toFixed(2);
    
    var unit = _oldPrice.match(/(\D+)(\d+\.*\d*)/)[1];
    return (unit + newSetPrice);
}   

module.exports = function () {
    var $pdpMain = $('#pdpMain');
    // hover on swatch - should update main image with swatch image
    $pdpMain.on('click', '.swatchanchor', function () {
    	if (typeof(YMK) !== 'undefined' && YMK){
    		YMK.applyMakeupBySku($(this).data('variantProductYoucamGuid'));
    	}
		var largeImg = $(this).data('lgimg'),
            $imgZoom = $pdpMain.find('.main-image'),
            $mainImage = $pdpMain.find('.primary-image');

        if (!largeImg) { return; }
        // store the old data from main image for mouseleave handler
        $(this).data('lgimg', {
            hires: $imgZoom.attr('href'),
            url: $mainImage.attr('src'),
            alt: $mainImage.attr('alt'),
            title: $mainImage.attr('title')
        });
        // set the main image
        image.setMainImage(largeImg);
    });

    // click on swatch - should replace product content with new variant
    $pdpMain.on('click', '.product-detail .swatchanchor', function (e) {
        e.preventDefault();
        if ($(this).parents('li').hasClass('unselectable')) { return; }
        updateContent(this.href);
    });

    // change drop down variation attribute - should replace product content with new variant
    $pdpMain.on('change', '.variation-select', function () {
        if ($(this).val().length === 0) { return; }
        updateContent($(this).val());
    });

    // change drop down variation attribute - should replace product content with new variant
    $('.product-set-item').on('change', '.my-compact-select', function () {
        if ($(this).val().length === 0) { 
            return;
        }

        var href = $(this).val();
        href = href.replace('Variation', 'VariationForMyCompact');
        updateContentForCompact(href);
    });
    if($('#pdp-you-cam-open-close-buttons').length > 0 && $('#pdp-you-cam-open-close-buttons').css('display') != 'none'){
    		while(typeof YMK !== 'undefined'){
    			YMK.init();
    		}
    }
    $('.pdp-you-cam-thumbnail').on('click',function(e){
    	if($('#YMK-module').css('display').length <= 0 || $('#YMK-module').css('display') != 'block'){
			window.YMK.open();
			$('#pdp-youcam-screen').css('display','block');
			$('#product-content .product-name,#product-content .short-desc').css('display','none');
			$(this).text('Close');
		}else{
			window.YMK.close();
			$('#pdp-youcam-screen').css('display','none');
			$('#product-content .product-name,#product-content .short-desc').css('display','block');
			$(this).text('Live Shade Finder!');
		}
	});
};
