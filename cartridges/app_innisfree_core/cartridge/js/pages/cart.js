'use strict';

var account = require('./account'),
    bonusProductsView = require('../bonus-products-view'),
    quickview = require('../quickview'),
    sample = require('../sample'),
    cartStoreInventory = require('../storeinventory/cart'),
    dialog = require('../dialog'),
    page = require('../page'),
    util = require('../util');

/**
 * 
 * Function update to cart when select quantity on product
 */
 function updateCart() {
    $('.select-type').on('change','select', function(){
        $("#cart-items-form" ).submit();
    })
 }
 
 /**
  * @description Make the AJAX request to add an recommendation item to cart
  * @returns {Promise}
  */
 
 var addRecItemToCart = function () {
 	var recElement = $(this);
     return Promise.resolve($.ajax({
         type: 'POST',
         url: util.ajaxUrl(Urls.addProduct),
         data: {
         	'pid': recElement.data('product-id'),
         	'Quantity': recElement.data('qty') 
         }
     })).then(function (response) {
         // handle error in the response
         if (response.error) {
             throw new Error(response.error);
         } else {
             page.refresh();
         }
     });
 };

/**
 * @private
 * @function
 * @description Binds events to the cart page (edit item's details, bonus item's actions, coupon code entry)
 */
function initializeEvents() {
    $('#cart-table').on('click', '.item-edit-details a', function (e) {
        e.preventDefault();
        quickview.show({
            url: e.target.href,
            source: 'cart'
        });
    })
    .on('click', '.bonus-item-actions a, .item-details .bonusproducts a', function (e) {
        e.preventDefault();
        bonusProductsView.show(this.href);
    });
    // override enter key for coupon code entry
    $('form input[name$="_couponCode"]').on('keydown', function (e) {
        if (e.which === 13 && $(this).val().length === 0) { return false; }
    });

    //to prevent multiple submissions of the form when removing a product from the cart
    var removeItemEvent = false;
    $('button[name$="deleteProduct"]').on('click', function (e) {
        if (removeItemEvent) {
            e.preventDefault();
        } else {
            removeItemEvent = true;
        }
    });
    
    //update cart
    updateCart();
    //init sample function
    sample();
    $('.add-to-wishlist').on('click', function(e){
        e.preventDefault();
        var addToWishlistItem = $(this);
        var url = $(this).attr('href');
        $.ajax({
            url: url,
            success: function () {
                dialog.open({
                    html: addToWishlistItem.clone().text(Resources.MESSAGE_IN_WISHLIST).removeAttr('href'),
                    options: {
                        width: 400,
                        dialogClass: 'default',
                        close: function() {
                            addToWishlistItem.parent().empty().append('<div class="in-wishlist"><i class="fa fa-heart"></i></div>');
                        }
                    }
                });
            }
        });
    })
    
    if($('.cartcheckout').length == 0){
        $('.minicartcheckout').hide();
    }
 

	/*
	event on clicking of add bonus to cart, this is totally customised from OOTB choice of bonus products and
	currently supports only onc choice from a group of products, for 2 or more choice code will be needed to change. On clicking 
	on add to cart to bonus, all other are marked as disabled, the number of items allowed can be fetched from data attribute data-limit
	which is with class custom-bonus-choice-bar-body
	*/    
    $('.add-bonus-product-to-cart').on('click', function(e){
    	e.preventDefault();
    	if(true || $(this).hasClass('registered')){   	 
    		 var url = util.appendParamsToUrl(Urls.addBonusProduct, {bonusDiscountLineItemUUID: $(this).data('bonusDiscountLineItemUuid')});
    		 var bonusproducts = [];
	         
	        if($(this).hasClass('bonusProductAlreadyAdded')){
	    		 url = util.appendParamsToUrl(url, {removeBonusLineItemOnly: true});
	    	 }else{
	    		 var p = {
		                 pid: $(this).data('productId').toString(),
		                 qty: 1,
		                 options: {}
		             }; 
		         bonusproducts.push({product:p});
	    	 }
	         
	         if($(this).parents('.sample-tmp').find('.bonusProductAlreadyAdded')){
	        	 
	        	 var clickedPid = $(this).data('productId');
	        	 $(this).parents('.sample-tmp').find('.bonusProductAlreadyAdded').each(function() {
	        		 
	        		var p = {
			                 pid: $( this ).data('productId').toString(),
			                 qty: 1,
			                 options: {}
			             };
	        		
	        		//add only products which were added earlier, already clicked one has been added above
			        if($(this).data('productId') != clickedPid){
			        	bonusproducts.push({product:p});
			        }
			        	
	        	 });
	        	 
	        	 
	         }
	         var bonusProducts = {bonusproducts: bonusproducts};
	         // make the server call
	         $.ajax({
	             type: 'POST',
	             dataType: 'json',
	             cache: false,
	             contentType: 'application/json',
	             url: url,
	             data: JSON.stringify(bonusProducts)
	         })
	         .done(function () {
	             // success
	        	 location.reload();
	         })
    	}
    });
	/*
	event on clicking of drop down of bonus choices
	*/
    $('.custom-bonus-choice-bar-header .choose-tit').on('click', function (e) {
        e.preventDefault();
        if($(this).hasClass('hide')){
           $(this).removeClass('hide');
           $(this).parents('.custom-bonus-choice-bar-header').siblings('.choose-con').slideDown(200);
       } else {
           $(this).addClass('hide');
           $(this).parents('.custom-bonus-choice-bar-header').siblings('.choose-con').slideUp(200);
       }
   });
    
    $('.pt_cart').on('click', '.add-rec-product-to-cart', addRecItemToCart);
   
}

exports.init = function () {
    initializeEvents();
    if (SitePreferences.STORE_PICKUP) {
        cartStoreInventory.init();
    }
    account.initCartLogin();
};
