'use strict';

var dialog = require('../dialog')
var wrapDialogClass = 'reset-password wrap-from-returnrequest';
var util = require('../util');

var runReturnRequestEvent = function () {
    $('body').on('click', '.order-return-button.open-dialog', function() {
        dialog.open({
            url: Urls.returnRequest,
            options: {
                width: 720,
                dialogClass: wrapDialogClass,
                show: {
                    effect: "fade",
                    duration: 500
                },
                hide: {
                    effect: "fade",
                    duration: 500
                }
            }
        });
    });
    $('body').on('click', '.order-return-button.step2', function() {
        var orderNo = $('.order-number-text').data('order');
        var sendMailUrl = util.appendParamsToUrl(Urls.sendOrderReturnEmail, {orderNo: orderNo});
        $.ajax({
            url: sendMailUrl,
            success: function (res) {
                var $contentFooter = $('.content_footer');
                if (res.status === 'OK') {
                    $contentFooter.find('button.order-return-button').remove();
                    $contentFooter.find('.success-message').show();
                    $contentFooter.find('.error-message').hide();
                } else {
                    $contentFooter.find('.error-message').show();
                }
            }
        });
    });
};

exports.init = runReturnRequestEvent;
