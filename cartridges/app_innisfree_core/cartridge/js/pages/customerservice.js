'use strict';

var dialog = require('../dialog');
/**
 * @private
 * @function
 * @description Binds the click events to the remove-link and quick-view button
 */
function initializeEvents() {
    $(document).on('submit', '.pt_content #RegistrationForm', function(event) {
        event.preventDefault();
        var $form = $(this);
        var $formSubmitBtn = $form.find('button[type=submit]');
        $formSubmitBtn.prop('disabled', 'disabled');
        $.ajax( {
            type: 'POST',
            url: $form.attr('action'),
            data: $form.serialize(),
            success: function( response ) {
                dialog.open({
                    html: response.message,
                    options: {
                        autoOpen: true,
                        modal: true,
                        width: 410,
                        dialogClass: 'default-popup',
                        buttons: [{
                            text: "Ok",
                            click: function() {dialog.close()}
                        }]
                    }
                });
            },
            complete: function() {
                $formSubmitBtn.prop('disabled', false);
            }
        } );
    });
}

exports.init = function () {
    initializeEvents();
};
