'use strict';

/**
 * @function
 * @description file develop on checkout login page
 */
function validateForm(form,btn){
    if(form.length > 0 && btn.length > 0){
        if (form.valid()) {
            btn.prop('disabled', false);  
        } else {
            btn.prop('disabled', 'disabled');
        }
    } else {
        return false;
    }
}
exports.init = function () {
    var guestForm = $(".guest-checkout"),
        guestInput = $('.guest-checkout .input-text'),
        continuteBtn = $('.continute-btn button'),
        loginForm = $("#dwfrm_login"),
        loginInput = $('#dwfrm_login .input-text'),
        signinBtn = $('.button-sigin');
    signinBtn.prop('disabled', 'disabled');
    continuteBtn.prop('disabled', 'disabled');
    $(window).load(function() {
        // Guest Form Validate Button
        guestInput.on('blur keyup', function() {
            validateForm(guestForm,continuteBtn);
        });
        
        // Login Form Validate Button
        loginInput.on('blur keyup', function() {
            validateForm(loginForm,signinBtn);
        });
    });
};