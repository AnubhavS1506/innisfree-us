'use strict';

var address = require('./address'),
    billing = require('./billing'),
    multiship = require('./multiship'),
    checkoutlogin = require('./checkoutlogin'),
    shipping = require('./shipping');

/**
 * @function Initializes the page events depending on the checkout stage (shipping/billing)
 */
exports.init = function () {
    address.init();
    checkoutlogin.init();
    if ($('.checkout-shipping').length > 0) {
        shipping.init();
    } else if ($('.checkout-multi-shipping').length > 0) {
        multiship.init();
    } else {
        billing.init();
    }

    //if on the order review page and there are products that are not available diable the submit order button
    if ($('.order-summary-footer').length > 0) {
        if ($('.notavailable').length > 0) {
            $('.order-summary-footer .submit-order .button-fancy-large').attr('disabled', 'disabled');
        }
    }
    
    // show/hide cart items
    $('body').on('click','.wrap-cart-content h3', function(){
        var _active = $(this).parents('.wrap-cart-content .title-summary').hasClass('active');
        if(_active){
            $('.wrap-cart-content .title-summary').removeClass('active');
            $('.cart-summary').find('.content').slideUp(150);
        } else {
            $('.wrap-cart-content .title-summary').addClass('active');
            $('.cart-summary').find('.content').slideDown(150);
        }
        return false;
    });
    // prevent click action tooltip
    $('body').on('click', '.tooltip', function(e){
        e.preventDefault();
    })
};
