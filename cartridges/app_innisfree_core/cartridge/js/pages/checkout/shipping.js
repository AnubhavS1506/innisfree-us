'use strict';

var ajax = require('../../ajax'),
    formPrepare = require('./formPrepare'),
    progress = require('../../progress'),
    tooltip = require('../../tooltip'),
    util = require('../../util'),
    validator = require('../../validator');

var shippingMethods,
    isUserAuthenticated = false,
    creditCardTileFirstClick = true,
    isShippingAddressBeingEdited = false,
    editAddressId = "",
    newCreditCardForm = "",
    selectedPaymentMethod = "CREDIT_CARD",
    checkoutStep = "",
    buttonYes = Resources.CONFIRMATION_YES,
    buttonNo = Resources.CONFIRMATION_NO;
/**
 * Below mentioned code adds to javascript logic for SP(Single Page) Checkout
 */

$(document).ready(function() {

    $('.wrap-cart-content .title-summary').find('h3').click(function(){
            var _active = $(this).parents('.wrap-cart-content .title-summary').hasClass('active');
            if(_active){
                $('.wrap-cart-content .title-summary').removeClass('active');
                $('.cart-summary').find('.content').slideUp(150);
            } else {
                $('.wrap-cart-content .title-summary').addClass('active');
                $('.cart-summary').find('.content').slideDown(150);
            }
            return false;
    });

    /**
     * Click event on shipping form submit to show shipping methods
     */ 
    $(document).on("click",".shipping-submit", function(event) {
        
        event.preventDefault();
        var el = $(this);
        var data = "";
        var url = "";

        if(isShippingAddressBeingEdited) {
            data = $("[id$='_shippingAddress']").serialize();
            url = util.appendParamToURL(Urls.handleSPAddressEdit, 'addressID', editAddressId);
        } else {
            data = "";
            url = Urls.handleShippingMethod;
            
            if(isUserAuthenticated) {
                $("input[name$='_shippingAddress_addToAddressBook']").prop("disabled", false);
                data = $("[id$='_shippingAddress']").serialize();
            } else {
                data = $(this).closest('form').serialize();
            }
            
            if(url.indexOf('?') != -1) {
                url += "&SPCheckoutStep=ShippingMethod&format=ajax";
            } else {
                url += "?SPCheckoutStep=ShippingMethod&format=ajax";
            }
        }
        
        // Common functionality
        $.ajax({
            type: "POST",
            url: url,
            data: data
          })
          .done(function(response) {
              if (isShippingAddressBeingEdited) {
                  isShippingAddressBeingEdited = false;
                  $(".userAddressList").html(response);
                  $(".shipping-submit").trigger("click");
              } else {
                  $(".primary-content").html(response);
              }
              if(isUserAuthenticated == "true") {
                  $(".userAddressList").addClass("hide");
                  $(".shipping-registered").addClass("hide");
                  $(".useAnotherShippingAddress").addClass("hide");
              }
              
              $(".editShippingAddress ").removeClass("hide").addClass("show"); 
              
              bindClickEvents();
              updateSummary();
              
              // If shipping address is being by passed as there is already a default selected address, 
              // then trigger the click event of the default shipping method
              var defaultShippingMethod = $('#shipping-method-list').find('[name$="_shippingMethodID"]:checked');
              if(defaultShippingMethod.length) {
                  defaultShippingMethod.trigger("click");
              } else {
                  $('#shipping-method-list').find('[name$="_shippingMethodID"]:first').trigger("click");
              }
              
              $('.shipping-submit').closest('.checkout-form').addClass('step-box');
              el.closest('.checkout-form').removeClass('active');
              $('.spShippingMethodForm').addClass('active');
              validator.init();
              formPrepare.init({
                  continueSelector: '[name$="shippingAddress_save"]',
                  formSelector:'form[id$="singleshipping_shippingAddress"]'
              });
          })
          .fail(function() {
              console.log("AJAX call failed....");
          });
    });
    
    /**
     * Click event on shipping method submit to show payment page
     */ 
    $(document).on("click",".shippingmethod-submit", function() {
        var data = $(this).closest('form').serialize();
        var selectedShippingMethodID = $("#shipid").val();
        var el = $(this);
        var url = util.appendParamToURL(Urls.handleSPBillingPage, 'selectedShippingMethodID', selectedShippingMethodID);
        
        var checkoutStepParam = "ShippingMethod";
        if(checkoutStep == "Billing") {
            checkoutStepParam = checkoutStep;
        }
        
        if(url.indexOf('?') != -1) {
            url += "&SPCheckoutStep="+checkoutStepParam+"&format=ajax";
        } else {
            url += "?SPCheckoutStep="+checkoutStepParam+"&format=ajax";
        }
        $.ajax({
          type: "POST",
          url: url,
          data: data
        })
        .done(function(response) {
            $(".spPaymentFormDetail").html(response);
            
            $(".spSelectedShippingMethod").removeClass("hide").addClass("show");
            $(".spShippingMethodFormDetail").removeClass("show").addClass("hide");
            $(".editShippingMethod").removeClass("hide").addClass("show");

            $(".editPaymentSection").removeClass("show").addClass("hide");
            
            $(".spSelectedShippingMethod").html($(".selectedShippingMethodName").html());
            
            $(".spPaymentFormDetail").removeClass("hide").addClass("show");
            $(".spSelectedPayment").removeClass("show").addClass("hide");
            
            newCreditCardForm = $(".new_credit_card");
            bindClickEvents("payment");
            if(isUserAuthenticated == "true") {
                if($(".creditcard-list .creditcard-tile.default").length) {
                    $(".creditcard-list .creditcard-tile.default .creditCardInfo").trigger("click");
                } else {
                    $(".creditcard-list .creditcard-tile:first .creditCardInfo").trigger("click");
                }
                creditCardTileFirstClick = false;
            }
            
            if(checkoutStep == "Billing") {
                // Show the error over here
                var $paymentError = $(".js-payment-error");
                $paymentError.removeClass("hide");
                $('html, body').animate({
                    scrollTop: $(".spPaymentForm").offset().top
                }, 500);
            }
            
            updateSummary();
            el.closest('.checkout-form').removeClass('active');
            $('.spPaymentForm').addClass('active');
            
            // Fix error can't check because billing address is null
            populateSavedAddress();
        })
        .fail(function() {
            console.log("AJAX call failed....");
        });
    });
    
    
    /**
     * Click event on payment form submit to show order review page
     */ 
    
    $(document).on("click",".paymentpage-submit", function() {
        
        var $paymentError = $(".js-payment-error");
        $paymentError.addClass("hide")
        $paymentError.find('.js-payment-error-message').html("");
        
        var $paymentForm = $(this).closest('form');
        var data = $paymentForm.serialize();
        var url;
        var selectedRadioButtonValue = $('input[name="billingaddressradio"]:checked').val();
        if(selectedRadioButtonValue == "shippingaddress") {
            url = util.appendParamToURL(Urls.handleSPOrderReview, 'shippingBillingAddressSame', true); 
        }else{
            url = Urls.handleSPOrderReview;
        }
        
        var el = $(this);
        $.ajax({
          type: "POST",
          url: url,
          data: data
        })
        .done(function(response) {
            if( $('span.errormessages').find('div').length > 0) {
                $('span.errormessages').find('div').remove();
            }

            if (response.inventoryError) {
                window.location.href = response.redirectURL;
            }
            
            if(selectedPaymentMethod == "PayPal" && response.reasonCode == 100) {
                window.location.href = response.redirectURL;
            } else if(selectedPaymentMethod == "PayPal" && response.reasonCode != "100") { // Logic for paypal response code 233
                
                // This means an error has occurred in paypal checkout
                $(".editPaymentSection").trigger("click");
                $paymentError.removeClass("hide");
                $('html, body').animate({
                    scrollTop: $(".spPaymentForm").offset().top
                }, 500);
                $paymentError.find('.js-payment-error-message').html(Resources.INVALID_ADDRESS);
            } else {
                $paymentForm.find('.valid-error-wrapper').removeClass('valid-error-wrapper')
                    .find('.valid-error').remove();
                if (typeof response.paymentCardStatus != 'undefined' && response.paymentCardStatus.error) {
                    var paymentCardStatusObject = response.paymentCardStatus;
                    for (var key in paymentCardStatusObject) {
                        if ((paymentCardStatusObject.hasOwnProperty(key)) && (key != 'error') &&(paymentCardStatusObject[key]!= 'undefined')) {
                            var $errorItem = $paymentForm.find('.form-row.'+ key);
                            $errorItem.addClass('valid-error-wrapper')
                                .append('<div class="valid-error">' + paymentCardStatusObject[key] + '</div>');
                        }
                    }
                    $('.valid-error-wrapper').on('change keyup focus', 'input, select', function() {
                        $(this).closest('.valid-error-wrapper').removeClass('valid-error-wrapper')
                            .find('.valid-error').remove();
                    });
                    return;
                }
                
                $(".spPaymentFormDetail").removeClass("show").addClass("hide");
                $(".spSelectedPayment").removeClass("hide").addClass("show");
                $(".editPaymentSection ").removeClass("hide").addClass("show");
                
                // removing
                $(".spSelectedPayment").html("");
                $(".paymentInfo").detach().appendTo(".spSelectedPayment");
                        
                $(".spOrderReviewDetail").removeClass("hide").addClass("show");
                bindClickEvents();
                el.closest('.checkout-form').removeClass('active');
                $('.spOrderReviewForm').addClass('active');
                $('body').find('input[name=SPCheckoutStep]').val('OrderReview');
                updateSummary();
            }
        })
        .fail(function() {
            console.log("AJAX call failed....");
        });
        
    });
    
    /**
     * Click event on coupon code submit for SP Checkout
     */ 
    $(document).on("click",".coupon-submit", function(e){
    	$('.coupon-error').html('');
        var $checkoutForm = $('.checkout-shipping');
        var $couponCode = $('input[name$="_couponCode"]');
        e.preventDefault();
        
        var $error = $checkoutForm.find('.coupon-error'),
            code = $couponCode.val();
        if (code.length === 0) {
            $('.coupon-error').html("<div class='couponUnknown couponErrorMsg error_message'><span class='sp-icons caution'></span>" + Resources.COUPON_CODE_MISSING + "</div>");
            return;
        }

        var url = util.appendParamsToUrl(Urls.addCoupon, {couponCode: code, format: 'ajax'});
        $.getJSON(url, function (data) {
            var fail = false;
            var msg = '';
            if (!data) {
                msg = Resources.BAD_RESPONSE;
                fail = true;
            } else if (!data.success) {
                msg = data.message.split('<').join('&lt;').split('>').join('&gt;');
                fail = true;
            }
            if (fail) {
                $error.html(msg);
                return;
            }
            if (data.success){
            	if(typeof data.CouponStatus === 'undefined'){
            	   $(".coupon-error").append("<div class='couponUnknown couponErrorMsg error_message'><span class='sp-icons caution'></span>"+Resources.COUPONCODEUNKNOWN+"</div>");
            	   return;
            	}
                var couponMessage = data.CouponStatus.replace(/_/g ," ");
                $(".coupon-error").html("");
                $(".redemption").html("");
                
                switch(data.CouponStatus) {
                    case "COUPON_CODE_UNKNOWN":
                        $(".coupon-error").append("<div class='couponUnknown couponErrorMsg error_message'><span class='sp-icons caution'></span>"+couponMessage+"</div>")
                        break;
                        
                    case "COUPON_CODE_ALREADY_IN_BASKET":
                        $(".coupon-error").append("<div class='couponInBasket couponErrorMsg error_message'><span class='sp-icons caution'></span>"+couponMessage+"</div>")
                        break;
                        
                    case "APPLIED":
                        updateSummary();
                        
                        setTimeout(function() {
                            $("#cart-items-form").find('.coupon-submit').text('Applied');
                        }, 1500);
                        break;
                        
                    default:
                        break;
                }               
            }
        });
    });
    
    /**
     * Click event on redeem reward points submit for SP Checkout
     */
    $(document).on("click", ".rewardpoints-submit", function(e) {
        var $checkoutForm = $('.redeem-point');
        var $rewardPoints = $('input[name$="_rewardpoints"]');
        var $totalPoints = $('input[name$="totalrewardpoints"]');
        e.preventDefault();
        var $error = $checkoutForm.find('.rewardpoint-error');
        if ($('.redeempointErrorMsg').length > 0) {
            $('.redeempointErrorMsg').remove();
        }
        if ($rewardPoints.val().length === 0) {
            $error.html(Resources.REWARDPOINT_MISSING);
            return;
        }
        var points = parseInt($rewardPoints.val()),
            totalPoints = parseInt($totalPoints.val());

        if (points === 0) {
            $error.html(Resources.ZERO_REDEEM);
            return;
        }
        if (points > totalPoints) {
            $error.html(Resources.MORE_THAN_TOTAL_REDEEM);
            return;
        }

        var url = util.appendParamsToUrl(Urls.redeemRewardPoints, {
            rewardPoints: points,
            format: 'ajax'
        });

        $.getJSON(url, function(data) {
            var fail = false;
            var msg = '';
            if (!data) {
                msg = Resources.BAD_RESPONSE;
                fail = true;
            } else if (!data.success) {
                msg = data.message.split('<').join('&lt;').split('>').join('&gt;');
                fail = true;
            }
            if (fail) {
                $error.html(msg);
                return;
            }
            if (data.success) {
                switch (data.status) {
                    case "ERROR":
                        $(".rewardpoint-error").append("<span class='redeempointErrorMsg'>" + data.message + "</span>")
                        break;

                    case "OK":
                        updateSummary();
                        if($('.spPaymentForm.billingPage').find('.spPaymentFormDetail').hasClass('show')) {
                            $(".shippingmethod-submit").trigger("click");
                        }
                        
                        setTimeout(function() {
                            $(".rewardpoint-redemption").append("<span class='pointredeemed success'>" + data.message +"</span>")
                        }, 1500);
                        break;

                    default:
                        break;
                }
            }
        });
    });
    
     /**
     * Click event on coupon code remove link on spcheckout
     */ 
    
    $(document).on("click",".couponcode-remove", function(){
        var couponCodeToRemove = $(this).data("couponcode");
        var url = util.appendParamToURL(Urls.handleSPCouponRemove, 'couponCodeToRemove', couponCodeToRemove);
        $.ajax({
          type: "POST",
          url: url
        })
        .done(function() {
            updateSummary();
        })
    });

    
    /**
     * Click event on reset points link on spcheckout
     */     
    $(document).on("click", ".resetpoints-submit", function(e) {
        var url = Urls.resetPoints;
        $.ajax({
          type: "POST",
          url: url
        })
        .done(function(data) {
        	if(data.status === 'OK') {
        		updateSummary();
        		
        		setTimeout(function() {
	    			$(".rewardpoint-redemption").append("<span class='pointredeemed success'>" + data.message +"</span>")
	    		}, 1500);
    		}        	
        })
    });

    /**
     * Click event on place order submit
     */ 
    /*
    $(document).on("click",".placeoder-btn", function(e){
        $(.submit-order" ).trigger( "click" );
    })
    */
});

function initializeShippingPage() {
    
    // Check if user is authenticated
    isUserAuthenticated = $("input[name='userAuthenticated']").val();
    checkoutStep = $("input[name='SPCheckoutStep']").val();
    
    bindClickEvents("shipping");
    
    if(checkoutStep == "OrderReview") {
        setTimeout(function() {
            $("html, body").animate({
                scrollTop: $(".spOrderReviewForm").offset().top
            }, 1000);
        }, 1500);
        return;
    }
    
    if(checkoutStep == "Billing") {
        $(".spShippingMethodFormDetail").addClass("hide");
        
        setTimeout(function() {
            $(".editPaymentSection").trigger("click");

            $("html, body").animate({
                scrollTop: $(".spPaymentForm").offset().top
            }, 1000);
            
            $(".shippingPage.checkout-form").addClass('step-box');
           
        }, 500);
        return;
    }
    
    formPrepare.init({
        continueSelector: '[name$="shippingAddress_save"]',
        formSelector:'[id$="singleshipping_shippingAddress"]'
    });
    
    $('input[name$="_shippingAddress_isGift"]').on('click', giftMessageBox);

    $('.checkout-shipping.address').on('change',
        'input[name$="_addressFields_address1"], input[name$="_addressFields_address2"], select[name$="_addressFields_states_state"], input[name$="_addressFields_city"], input[name$="_addressFields_zip"]',
        updateShippingMethodList
    );
    
    // Logic for showing dash in place shipping value when loaded first time
    if($(".shipping-value").length > 0) {
        $(".shipping-value").html("-");
    }
    
    $("#secondary.summary").removeClass("hide");

    
    if($(".userAddressList").length && $(".userAddressList .address-list").length && !$(".address-list .address-tile").hasClass("default")) {
        $(".editShippingAddress").trigger("click");
    }
    
    if($(".userAddressList").length && $(".userAddressList .address-list").length && $(".address-list .address-tile").hasClass("default")) {
        $(".shipping-submit").trigger("click");
    }
    
    // check if the payment summary has information in it already
    var $miniPaymentInstrument = $(".spSelectedPayment").find(".mini-payment-instrument");
    var mini_billing_address = $(".spSelectedPayment").find(".mini-billing-address");
    
    if(($miniPaymentInstrument.length && $miniPaymentInstrument.html().length > 1) || (mini_billing_address.length && mini_billing_address.html().length > 1)) {
        setTimeout(function() {
            // show the edit button on payment section
            $(".editPaymentSection").removeClass("hide").addClass("show");
        }, 1500);
    }
}

function bindClickEvents(checkoutStep) {
    
    if(checkoutStep != undefined && checkoutStep != "") {
        if(checkoutStep == "payment") {
            formPrepare.init({      
                continueSelector: '[name$="paymentoptions_save"]',
                formSelector: 'form[id$="dwfrm_billing"]'
            });
        } else if(checkoutStep == "shipping") {
            formPrepare.init({
                continueSelector: '[name$="shippingAddress_save"]',
                formSelector:'form[id$="singleshipping_shippingAddress"]'
            });
        }
    }
    
    $('input[name$="_shippingAddress_isGift"]').bind('click', giftMessageBox);
    
    $('.address').on('change',
        'input[name$="_addressFields_address1"], input[name$="_addressFields_address2"], select[name$="_addressFields_states_state"], input[name$="_addressFields_city"], input[name$="_addressFields_zip"]',
        updateShippingMethodList
    );
    // Click event for editaddress button
    $(".editShippingAddress").off("click").on("click", function(event) {
        
        if(isUserAuthenticated == "true" && $(".userAddressList").length > 0 && $(".userAddressList .address-list").length > 0) {
            // functionality for authenticated user
            $(this).unbind( event );
            
            $(".address-list li, .useAnotherShippingAddress, .shipping-registered").removeClass("hide");
            $(".userAddressList").removeClass("hide").addClass("show");
            
            $(".spSummary").removeClass("hide").addClass("show");
            $(".spDetail").removeClass("show").addClass("hide");
            $(".editButton").removeClass("hide").addClass("show");
            
            $(this).addClass("hide");
            
            $(".spSelectedShippingAddress").removeClass("show").addClass("hide");
            
        } else {
            // functionality for guest user
            $(".spSummary").removeClass("hide").addClass("show");
            $(".spDetail").removeClass("show").addClass("hide");
            $(".editButton").removeClass("hide").addClass("show");
            
            $(".spShippingAddressFormDetail").removeClass("hide").addClass("show");
            $(".editShippingAddress").removeClass("show").addClass("hide");
            $(".spSelectedShippingAddress").removeClass("show").addClass("hide");
        }
        
        if($("input[name$='_isGift']:checked").val() == "false") {
            $(".gift-message-text").addClass("hidden");
        }
    });
        
    // Shipping Method Edit
    $(".editShippingMethod").off("click").on("click", function() {
        if(isUserAuthenticated == "true") {
            
            $(".spSummary").removeClass("hide").addClass("show");
            $(".spDetail").removeClass("show").addClass("hide");
            $(".editButton").removeClass("hide").addClass("show");
            
            $(".userAddressList").removeClass("show").addClass("hide");
            $(".useAnotherShippingAddress").addClass("hide").removeClass("show");
            $(".shipping-registered").addClass("hide");
            
        } else {
            $(".spSummary").removeClass("hide").addClass("show");
            $(".spDetail").removeClass("show").addClass("hide");
            $(".editButton").removeClass("hide").addClass("show");
        }

        $(".spShippingMethodFormDetail").removeClass("hide").addClass("show");
        $(".editShippingMethod").removeClass("show").addClass("hide");
        $(".spSelectedShippingMethod").removeClass("show").addClass("hide");
    });
    
    // Payment Options Edit
    $(".editPaymentSection").off("click").on("click", function(event) {
        event.preventDefault();
        
        $(".spSummary").removeClass("hide").addClass("show");
        $(".spDetail").removeClass("show").addClass("hide");
        $(".editButton").removeClass("hide").addClass("show");
        
        if($.trim($(".spPaymentFormDetail").html()) == '') {
            $(".shippingmethod-submit").trigger("click");
        }
        $(".spPaymentFormDetail").removeClass("hide").addClass("show");

        $(".editPaymentSection").removeClass("show").addClass("hide");
        $(".spSelectedPayment").removeClass("show").addClass("hide");
        
        // bindClickEvents("payment");
    });
    
    // Handle click event for shipping method
    $('#shipping-method-list').find('[name$="_shippingMethodID"]').bind("click", function() {
        $("#shipid").val($(this).val());
        selectShippingMethod($(this).val());
    });
    
    // Event to capture billing address radio buttons
    $('input[name="billingaddressradio"]').off('change').on('change', function(e) {
        var selectedRadioButtonValue = $(this).val();
        
        // shippingBillingAddress is a common class which will first hide both the forms
        // and than based on the radio button selected it will display only that form
        $(".shippingBillingAddress").removeClass("show").addClass("hide");
        
        if(selectedRadioButtonValue == "shippingaddress") {
            $(".customerShippingAddress").removeClass("hide").addClass("show");
            populateSavedAddress();
            
        } else if(selectedRadioButtonValue == "billingaddress") {
            $(".newBillingAddress").removeClass("hide").addClass("show");
            loadEmptyAddressForm("billing");
        }
    });
    
    $(".address-list .registeredAddressBlock").off('click').on("click", function(event) {
        // event.preventDefault();
        
        // fetch the selected address from BE
        var addressID = $(this).find(".mini-address-title").html(); // pass the addressId;  
        var url = util.appendParamToURL(Urls.handleSPRegisteredAddress, 'addressID', addressID);
        var addressblock = $(this);
        
        ajax.getJson({
            url: url,
            callback: function (data) {
                
                if (!data || !data.address) {
                    return false;
                }
                
                $(".useAnotherShippingAddress input").trigger("change");
                
                // Populate the shipping address form with the selected address
                updateShippingAddressForm(data);
                
                if(!addressblock.parent().hasClass("default")) {
                    addressblock.siblings(".address-make-default").trigger("click");
                }
            }
        });
    });
    
    // Handle click event of use another shipping address checkbox
    $(".useAnotherShippingAddress input").off('change').on("change", function() {
        if($(this).is(":checked")) {
            $(".spShippingAddressFormDetail").removeClass("hide").addClass("show");
            $(".shipping-registered").addClass("hide");
            
            // logic for showing empty form
            loadEmptyAddressForm("shipping");
            $(".add-to-address-book").parents(".form-row").removeClass("hide");
            $(".spEditShippingAddressTitleContainer").removeClass("show").addClass("hide");

            $("[name$='paymentoptions_save']").attr("disabled","disabled"); // Disable the continue button
            $("[name$='shippingAddress_save']").attr("disabled","disabled"); // Disable the continue button
            updateShippingAddressSave();
            
            // Uncheck the checkboxes for addToAddressBook and setAsDefaultShippingAddress
            $("input[name$='_shippingAddress_defaultShippingAddress']").prop("checked", false);
            $("input[name$='_shippingAddress_addToAddressBook']").prop("checked", false).prop("disabled", false);
        } else {
            $(".spShippingAddressFormDetail").removeClass("show").addClass("hide");
            $(".shipping-registered").removeClass("hide");
            
            $("[name$='shippingAddress_save']").prop("disabled",false); // Disable the continue button
        }
    });
    
    $(".address-make-default").off('click').on("click", function(event){
        event.preventDefault();
        event.stopPropagation();
        
        var addressId = $(this).siblings(".registeredAddressBlock").find(".mini-address-title").html();
        var url = util.appendParamToURL(Urls.handleSPSetDefault, 'AddressID', addressId);
        
        $.ajax({
          type: "POST",
          url: url,
          dataType : 'html',
        })
        .done(function(data) {
            $(".userAddressList").html(data);
            $(".address-list .address-tile").removeClass("hide");
            
            bindClickEvents(addressId?'shipping':'');
        })
        .fail(function() {
            console.log("AJAX call failed....");
        });
    });
    
    $(".address-edit").off('click').on("click", function(event){
        event.preventDefault();
        event.stopPropagation();
        
        isShippingAddressBeingEdited = true;
        
        // fetch the selected address from BE
        var addressId = $(this).siblings(".registeredAddressBlock").find(".mini-address-title").html();
        var url = util.appendParamToURL(Urls.handleSPRegisteredAddress, 'addressID', addressId);
        
        editAddressId = addressId;
        
        ajax.getJson({
            url: url,
            callback: function (data) {
                
                if (!data || !data.address) {
                    return false;
                }
                
                $(".spShippingAddressFormDetail").removeClass("hide").addClass("show");
                $(".shipping-registered").addClass("hide");
                $(".add-to-address-book").parents(".form-row").addClass("hide");
                $(".useAnotherShippingAddress input").attr("checked", false);
                $(".spEditShippingAddressTitleContainer").removeClass("hide").addClass("show");
                
                // Populate the shipping address form with the selected address
                updateShippingAddressForm(data);
            }
        });
    });
    
    $(".address-delete").off('click').on("click", function(event){
        event.preventDefault();
        event.stopPropagation();
        
        var selectedAddress = $(this);
        
        $("#dialog-confirm-delete-address").dialog ({
            resizable: false,
            height: "auto",
            width: 400,
            modal: true,
            buttons: [{
                text: buttonNo,
                click: function() {
                   $(this).dialog('close');
                }},{
                text: buttonYes,
                click: function() {
                    $( this ).dialog( "close" );
                    
                    var addressId = selectedAddress.siblings(".registeredAddressBlock").find(".mini-address-title").html();
                    var url = util.appendParamToURL(Urls.handleSPDelete, 'AddressID', addressId);
                    
                    $.ajax({
                      type: "POST",
                      url: url,
                      dataType : 'html',
                    })
                    .done(function(data) {
                        $(".userAddressList").html(data);
                        $(".address-list .address-tile").removeClass("hide");
                        
                        bindClickEvents("shipping");
                    })
                    .fail(function() {
                        console.log("AJAX call failed....");
                    });
                }
            }]
        });
    });
    
    // Handle click event of cancel button when editing shipping address form
    $(".spEditShippingAddressCancel").off('click').on("click", function(event) {
        event.preventDefault();
        
        $(".spShippingAddressFormDetail").removeClass("show").addClass("hide");
        $(".shipping-registered").removeClass("hide").addClass("show");
        $(".spEditShippingAddressTitleContainer").removeClass("show").addClass("hide");
    });
    
    $(".creditcard-list .creditCardInfo").off('click').on("click", function(event) {
        event.preventDefault();
        // fetch the selected creditcard from BE
        var creditCardUUID = $(this).find("input[name='ccuuid']").val(); // fetch the ccuuid of the selected credit card;   
        var url = util.appendParamToURL(Urls.handleSPCreditCard, 'creditCardUUID', creditCardUUID);
        var $creditCardTile = $(this).parent(".creditcard-tile");
        var $securityField = $(this).find(".security-field");
        var $securityInput = $securityField.find('.security-temp');
        
        if($(".useAnotherCreditCard input[name='use_another_creditcard']").is(":checked")) {
            $(".useAnotherCreditCard input[name='use_another_creditcard']").trigger("click");
        }
        
        ajax.getJson({
            url: url,
            callback: function (data) {
                if (!data) {
                    return false;
                }
                $(".creditcard-tile").removeClass("selected");
                $(".creditcard-tile").find('.security-temp').show();
                $creditCardTile.addClass("selected");
                $(".form-row.cvn").detach().insertAfter($securityField);
                $(".form-row.cvn input").val("").attr("placeholder", "Security Code").focus()
                    .closest('form').validate().resetForm();
                $securityInput.hide();
                // Populate the shipping address form with the selected address
                updateCreditCardForm(data);
            }
        });
    });
    
    // Handle click event of use another shipping address checkbox
    $(".useAnotherCreditCard input[name='use_another_creditcard']").off('change').on("change", function() {
        $(".form-row.valid-error-wrapper").removeClass('valid-error-wrapper')
            .find('.valid-error').remove();
        $(this).closest('form').validate().resetForm();
        if($(this).is(":checked")) {
            $(".new_credit_card").removeClass("hide").addClass("show");
            
            $(".form-row.cvn").detach().insertBefore($(".credit_card_expiry"));
            $(".form-row.cvn input").val("");
            $(".form-row .ownerFirstName").val("");
            loadEmptyCreditCardForm();
            $("[name$='paymentoptions_save']").attr("disabled","disabled");
            
        } else {
            $(".new_credit_card").removeClass("show").addClass("hide");
        }
    });
    
    // Handle credit card delete functionality
    $(".creditcardDelete").off('click').on("click", function(event) {
        var selectedCreditCard = $(this);
        
        $("#dialog-confirm-delete-creditcard").dialog ({
            resizable: false,
            height: "auto",
            width: 400,
            modal: true,
            dialogClass: "default",
            buttons: [{
                class: "btn-type dark-line size-s",
                text: buttonNo,
                click: function() {
                   $(this).dialog('close');
                }},{
                class: "btn-type dark-line size-s",
                text: buttonYes,
                click: function() {
                    $( this ).dialog( "close" );
                    
                    // fetch the selected creditcard from BE
                    var creditCardUUID = selectedCreditCard.parents(".creditCardInfo").find("input[name='ccuuid']").val(); // fetch the ccuuid of the selected credit card;   
                    var url = util.appendParamToURL(Urls.handleSPCardDelete, 'creditCardUUID', creditCardUUID);
                    var creditCardTile = selectedCreditCard.parents(".creditcard-tile");
                    
                    var creditcardTilesWithoutHide = $(".creditcard-tile").length - $(".creditcard-tile.hide").length;
                    
                    if($(".useAnotherCreditCard input[name='use_another_creditcard']").is(":checked")) {
                        $(".useAnotherCreditCard input[name='use_another_creditcard']").trigger("click");
                    }
                    
                    $.ajax({
                      type: "POST",
                      url: url,
                      dataType : 'html',
                    })
                    .done(function(data) {
                        if(creditcardTilesWithoutHide == 1) {
                            creditCardTile.addClass("hide");
                            $(".creditCardWrapper").addClass("hide");
                            $(".useAnotherCreditCard input[name='use_another_creditcard']").trigger("click");
                            
                            $("[name$='paymentoptions_save']").attr("disabled","disabled");
                            
                        } else if (creditcardTilesWithoutHide > 1) {
                            creditCardTile.addClass("hide");
                        }
                        $(".form-row.cvn").detach().insertBefore($(".credit_card_expiry"))
                            .closest('form').validate().resetForm();
                    })
                    .fail(function() {
                        console.log("AJAX call failed....");
                    });
                }
            }]
        });
    });
    
    // Event to handle payment methods - CREDIT_CARD or PAYPAL
    $('input[name$="_selectedPaymentMethodID"]').off('change').on('change', function(e) {
        var selectedRadioButtonValue = $(this).val();
        
        $(".payment-method").removeClass("payment-method-expanded");
        selectedPaymentMethod = selectedRadioButtonValue;
        
        if(selectedRadioButtonValue == "CREDIT_CARD") {
            $(".payment-method[data-method = 'CREDIT_CARD']").addClass("payment-method-expanded").append(newCreditCardForm);
            // populateSavedAddress();
            
        } else if(selectedRadioButtonValue == "PayPal") {
            
            // We will remove the new credit card form from the DOM so that valdiation erros do not get fired
            $(".new_credit_card").remove();
            $(".payment-method[data-method = 'Custom']").addClass("payment-method-expanded");
            
            $("[name$='paymentoptions_save']").removeAttr('disabled');
        }
    });
    
    
    // Handle click event of use another shipping address checkbox
    $("input[name$='_shippingAddress_defaultShippingAddress']").off('change').on("change", function() {
        if($(this).is(":checked")) {
        	$("input[name$='_shippingAddress_addToAddressBook']").prop("checked", true).prop("disabled", true);
        } else {
        	$("input[name$='_shippingAddress_addToAddressBook']").prop("checked", false).prop("disabled", false);
        }
    });
}

// Logic for SP checkout Ends here

/**
 * @function
 * @description Initializes gift message box, if shipment is gift
 */
function giftMessageBox() {
    // show gift message box, if shipment is gift
    $('.gift-message-text').toggleClass('hidden', $('input[name$="_shippingAddress_isGift"]:checked').val() !== 'true');
}

/**
 * @function
 * @description Update Shipping Address Save Button
 */
function updateShippingAddressSave(){
    var $shippingAddressSaveButton = $("[name$='shippingAddress_save']");
    $('.checkout-shipping input').keyup(function() {
        var empty = false;
        $('.checkout-shipping input.required').each(function(){
            if ($(this).val() == '') {
                empty = true;
            }
        });
        if (empty) {
            $shippingAddressSaveButton.attr("disabled","disabled"); // Disable the continue button
        } else {
            $shippingAddressSaveButton.prop("disabled",false); // Disable the continue button
        }
    });
}

/**
 * @function
 * @description updates the order summary based on a possibly recalculated basket after a shipping promotion has been applied
 */
function updateSummary() {
    var $summary = $('#secondary.summary');
    var SPCheckoutStep = $('body').find('input[name=SPCheckoutStep]').val();
    var url = util.appendParamToURL(Urls.summaryRefreshURL, 'SPCheckoutStep', SPCheckoutStep);
    var applyPoint = $('body').find('input[name=apllyPoint]').val();
    // indicate progress
    progress.show($summary);

    // load the updated summary area
    $summary.load(url, function () {
        // hide edit shipping method link
        $summary.fadeIn('fast');
        $summary.find('.checkout-mini-cart .minishipment .header a').hide();
        $summary.find('.order-totals-table .order-shipping .label a').hide();
    });
    $('select').niceSelect();
    if(applyPoint == 'applied'){
        $('.redeem-point').find('.rewardpoints-submit').text(Resources.APPLIED);
    }
    
}

/**
 * @function
 * @description Helper method which constructs a URL for an AJAX request using the
 * entered address information as URL request parameters.
 */
function getShippingMethodURL(url, extraParams) {
    var $form = $('.address');
    var params = {
        address1: $form.find('input[name$="_address1"]').val(),
        address2: $form.find('input[name$="_address2"]').val(),
        countryCode: $form.find('select[id$="_country"]').val(),
        stateCode: $form.find('select[id$="_state"]').val(),
        postalCode: $form.find('input[name$="_postal"]').val(),
        city: $form.find('input[name$="_city"]').val()
    };
    return util.appendParamsToUrl(url, $.extend(params, extraParams));
}

/**
 * @function
 * @description selects a shipping method for the default shipment and updates the summary section on the right hand side
 * @param
 */
function selectShippingMethod(shippingMethodID) {
    // nothing entered
    if (!shippingMethodID) {
        return;
    }
    // attempt to set shipping method
    var url = getShippingMethodURL(Urls.selectShippingMethodsList, {shippingMethodID: shippingMethodID});
    ajax.getJson({
        url: url,
        callback: function (data) {
            updateSummary();
            if (!data || !data.shippingMethodID) {
                window.alert('Couldn\'t select shipping method.');
                return false;
            }
            // display promotion in UI and update the summary section,
            // if some promotions were applied
            $('.shippingpromotions').empty();
        }
    });
}

/**
 * @function
 * @description Make an AJAX request to the server to retrieve the list of applicable shipping methods
 * based on the merchandise in the cart and the currently entered shipping address
 * (the address may be only partially entered).  If the list of applicable shipping methods
 * has changed because new address information has been entered, then issue another AJAX
 * request which updates the currently selected shipping method (if needed) and also updates
 * the UI.
 */
function updateShippingMethodList() {
    var $shippingMethodList = $('#shipping-method-list');
    if (!$shippingMethodList || $shippingMethodList.length === 0) { return; }
    var url = getShippingMethodURL(Urls.shippingMethodsJSON);

    ajax.getJson({
        url: url,
        callback: function (data) {
            if (!data) {
                window.alert('Couldn\'t get list of applicable shipping methods.');
                return false;
            }
            if (shippingMethods && shippingMethods.toString() === data.toString()) {
                // No need to update the UI.  The list has not changed.
                return true;
            }

            // We need to update the UI.  The list has changed.
            // Cache the array of returned shipping methods.
            shippingMethods = data;
            // indicate progress
            progress.show($shippingMethodList);

            // load the shipping method form
            var smlUrl = getShippingMethodURL(Urls.shippingMethodsList);
            $shippingMethodList.load(smlUrl, function () {
                $shippingMethodList.fadeIn('fast');
                // rebind the radio buttons onclick function to a handler.
                $shippingMethodList.find('[name$="_shippingMethodID"]').click(function () {
                    selectShippingMethod($(this).val());
                });

                // update the summary
                updateSummary();
                progress.hide();
                tooltip.init();
                
            });
        }
    });
}
function activeForm(el){
    $('.checkout-form').removeClass('active');
    el.closest('.checkout-form').addClass('active');
}
/**
 * @Function: populateSavedAddress 
 * @description Populate the billing address form with values from shipping address form
 */
function populateSavedAddress() {
    
      var shippingForm = $(".checkout-shipping");
      var billingForm = $(".checkout-billing");
    
      billingForm.find("[id$='_firstName']").val(shippingForm.find("[id$='_firstName']").val());
      billingForm.find("[id$='_lastName']").val(shippingForm.find("[id$='_lastName']").val());
      billingForm.find("[id$='_address1']").val(shippingForm.find("[id$='_address1']").val());
      billingForm.find("[id$='_address2']").val(shippingForm.find("[id$='_address2']").val());
      billingForm.find("[id$='_city']").val(shippingForm.find("[id$='_city']").val());
      billingForm.find("[id$='_postal']").val(shippingForm.find("[id$='_postal']").val());
      billingForm.find("[id$='_phone']").val(shippingForm.find("[id$='_phone']").val());
      
      // Handling the logic to prepopulate the state option separately
      var stateValue = shippingForm.find("[id$='_state'] option:selected").val();
      billingForm.find("[id$='_state'] option[value='"+stateValue+"']").prop("selected", true);
}

function updateShippingAddressForm(data) {
    var shippingForm = $(".checkout-shipping");
    
    shippingForm.find("[id$='_firstName']").val(data.address.firstName);
    shippingForm.find("[id$='_lastName']").val(data.address.lastName);
    shippingForm.find("[id$='_address1']").val(data.address.address1);
    shippingForm.find("[id$='_address2']").val(data.address.address2);
    shippingForm.find("[id$='_city']").val(data.address.city);
    shippingForm.find("[id$='_postal']").val(data.address.postalCode);
    shippingForm.find("[id$='_phone']").val(data.address.phone);
    
    shippingForm.find("[id$='_country'] option[value='"+data.address.countryCode+"']").prop("selected", true);
    shippingForm.find("[id$='_state'] option[value='"+data.address.stateCode+"']").prop("selected", true);
    
    var stateSelect = shippingForm.find("[id$='_state']").next(".nice-select");
    stateSelect.find("ul").find("li").removeClass("selected");
    stateSelect.find("[data-value='"+data.address.stateCode+"']").addClass("selected");
    stateSelect.find("span").html(stateSelect.find("[data-value='"+data.address.stateCode+"']").html());
    
    var countrySelect = shippingForm.find("[id$='_country']").next(".nice-select");
    countrySelect.find("ul").find("li").removeClass("selected");
    countrySelect.find("[data-value='"+data.address.countryCode+"']").addClass("selected");
    countrySelect.find("span").html(countrySelect.find("[data-value='"+data.address.countryCode+"']").html());
}
function redeemUsing(){
    $('body').on('click', '.redeem-point a.using', function(e){
        e.preventDefault();
        if($(this).hasClass('active')){
            $(this).find('em').text(Resources.USING);
            $(this).removeClass('active');
            $('.redeem-point').css('minHeight', '0');
            $('.redeem-point').find('.loading-dot').removeClass('active');
            $('.redeem-point').find('.redeem-con').removeClass('active');
            clearTimeout(redeemLoading);
        } else {
        	var availablePoints = $('input[name$="totalrewardpoints"]').val();
        	var url = util.appendParamToURL(Urls.populatePoints, 'availablePoints', availablePoints);
            $.ajax({
            	type: "GET",
                url: url
            })
            .done(function(data) {
            	if(data.status === 'OK') {
            		$('input[name$="_rewardpoints"]').val(data.orderPoints);
            	    var applyPoint = $('body').find('input[name=apllyPoint]').val();
            	    if(applyPoint == 'applied'){
            	        $('.redeem-point').find('.rewardpoints-submit').text(Resources.APPLIED);
            	    }
            		return;
            	}
            })
        	.fail(function() {
        		console.log("AJAX call failed....");
        	});
        	$(this).find('em').text(Resources.CLOSE);
            $('.redeem-point').css('minHeight', '220px');
            $(this).addClass('active');
            $('.redeem-point').find('.loading-dot').addClass('active');
            redeemLoadingTimeout();
        }
        return false;
    })
    var redeemLoading;
    function redeemLoadingTimeout(){
        redeemLoading = setTimeout(function(){
            $('.redeem-point').find('.loading-dot').removeClass('active');
            $('.redeem-point').find('.redeem-con').addClass('active');
        }, 2000);
    }
}
function loadEmptyAddressForm(formType) {
    var addressForm = "";
    
    $(".checkout-shipping").find("[name$='_useAsBillingAddress']").val(false);
    
    if(formType == "shipping") {
        addressForm = $(".checkout-shipping");
    } else if(formType == "billing") {
        addressForm = $(".checkout-billing");
    }
    
    addressForm.find("[id$='_firstName']").val("");
    addressForm.find("[id$='_lastName']").val("");
    addressForm.find("[id$='_address1']").val("");
    addressForm.find("[id$='_address2']").val("");
    addressForm.find("[id$='_city']").val("");
    addressForm.find("[id$='_postal']").val("");
    addressForm.find("[id$='_phone']").val("");
    
    addressForm.find("[id$='_country'] option").removeAttr("selected");
    addressForm.find("[id$='_state'] option").removeAttr("selected");
    
    var countrySelect = addressForm.find("[id$='_country']").next(".nice-select");
    countrySelect.find("ul").find("li").removeClass("selected");
    countrySelect.find("ul").find("li:first").addClass("selected");
    countrySelect.find("span").html(countrySelect.find("ul").find("li:first").html());
    
    var stateSelect = addressForm.find("[id$='_state']").next(".nice-select");
    stateSelect.find("ul").find("li").removeClass("selected");
    stateSelect.find("ul").find("li:first").addClass("selected");
    stateSelect.find("span").html(stateSelect.find("ul").find("li:first").html());
}

function updateCreditCardForm(data) {
    var billingForm = $(".checkout-billing");
    
    billingForm.find("[id$='_creditCard_ownerFirstName']").val(data.holder);
    billingForm.find("[id$='_creditCard_ownerLastName']").val(data.holderLastName);
    billingForm.find(".registered_creditcard_number").val(data.maskedNumber);
    
    billingForm.find("[id$='_creditCard_type'] option[value='"+data.type+"']").prop("selected", true);
    billingForm.find("[id$='_expiration_month'] option[value='"+data.expirationMonth+"']").prop("selected", true);
    billingForm.find("[id$='_expiration_year'] option[value='"+data.expirationYear+"']").prop("selected", true);
}

function loadEmptyCreditCardForm() {
    var creditcardForm = $(".checkout-billing");
    
    creditcardForm.find("[id$='_creditCard_owner']").val("");
    creditcardForm.find(".registered_creditcard_number").val("");
    
    creditcardForm.find("[id$='_creditCard_type'] option").removeAttr("selected");
    creditcardForm.find("[id$='_expiration_month'] option").removeAttr("selected");
    creditcardForm.find("[id$='_expiration_year'] option").removeAttr("selected");
}

exports.init = function () {
    initializeShippingPage();
    giftMessageBox();
    updateShippingMethodList();
    redeemUsing();
};

exports.updateShippingMethodList = updateShippingMethodList;
