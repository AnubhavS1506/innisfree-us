'use strict';

var _ = require('lodash');

var $form, $continue, $requiredInputs, $requiredSelect, validator;

var hasEmptyRequired = function () {
    // filter out only the visible fields
    var requiredValues = $requiredInputs.filter(':visible').map(function () {
        return $(this).val();
    });
    return _(requiredValues).contains('');
};

var validateForm = function () {
    // only validate form when all required fields are filled to avoid
    // throwing errors on empty form
    if (!validator) {
        return;
    }
    if (!hasEmptyRequired()) {
        if (validator.form()) {
            $continue.removeAttr('disabled');
        }
    } else {
        $continue.attr('disabled', 'disabled');
    }
};

var validateEl = function (event) {
    if ($(event.target).val() === '') {
        $continue.attr('disabled', 'disabled');
    } else {
        // enable continue button on last required field that is valid
        // only validate single field
        if (validator.element(event.target) && !hasEmptyRequired()) {
            $continue.removeAttr('disabled');
        } else {
            $continue.attr('disabled', 'disabled');
        }
    }
};

var validateDropdown = function (event) {
    var selectedOption =  $(event.target).find(":selected");
    if (selectedOption.val() === '') {
        $continue.attr('disabled', 'disabled');
    } else {
        // enable continue button on last required field that is valid
        // only validate single field
        if (validator.element(event.target) && !hasEmptyRequired()) {
            $continue.removeAttr('disabled');
        } else {
            $continue.attr('disabled', 'disabled');
        }
    }
};

var init = function (opts) {
    if (!opts.formSelector || !opts.continueSelector) {
        throw new Error('Missing form and continue action selectors.');
    }
    $form = $(opts.formSelector);
    $continue = $(opts.continueSelector);
    validator = $form.validate();
    $requiredInputs = $('.required', $form).find(':input');
    $requiredSelect = $('.required', $form).find('select');
    validateForm();
    // start listening
    $(document).on('change', $requiredInputs , validateEl); 
    $(document).on('keyup', $requiredInputs.filter('input'), _.debounce(validateEl, 200));
    
    // start listening for drop downs
    $(document).on('change', $requiredSelect, validateDropdown);
    $(document).on('keyup', $requiredSelect.filter('select'), _.debounce(validateDropdown, 200));
};

exports.init = init;
exports.validateForm = validateForm;
exports.validateEl = validateEl;
exports.validateDropdown = validateDropdown;
