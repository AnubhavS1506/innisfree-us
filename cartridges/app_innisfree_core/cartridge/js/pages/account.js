'use strict';

var giftcert = require('../giftcert'),
    tooltip = require('../tooltip'),
    util = require('../util'),
    dialog = require('../dialog'),
    page = require('../page'),
    login = require('../login'),
    validator = require('../validator'),
    returnRequest = require('./return-request'),
    pointhistory = require('./pointhistory'),
    wrapDialogClass = 'reset-password credit-card';

/**
 * @function
 * @description Initializes the events on the address form (apply, cancel, delete)
 * @param {Element} form The form which will be initialized
 */
function initializeAddressForm() {
    $('#edit-address-form').on('submit', function(e) {
        if (!$(this).valid()) {
            e.preventDefault();
        }

        // Set value for address id before submit
        var city = $(this).find('input[name$="_address_city"]').val();
        var firstName = $(this).find('input[name$="_address_firstname"]').val();
        var lastName = $(this).find('input[name$="_address_lastname"]').val();
        var $addressID = $(this).find('input[name$="_address_addressid"]');
        $addressID.val(city + '-' + firstName + '-' + lastName);
    });

    validator.init();
}
/**
 * @private
 * @function
 * @description Toggles the list of Orders
 */
function toggleFullOrder() {
    $('.order-items')
        .find('li.hidden:first')
        .prev('li')
        .append('<a class="toggle">View All</a>')
        .children('.toggle')
        .click(function() {
            $(this).parent().siblings('li.hidden').show();
            $(this).remove();
        });
}
/**
 * @private
 * @function
 * @description Binds the events on the address form (edit, create, delete)
 */
function initAddressEvents() {
    $(".address-delete").on("click", function(event) {
        event.preventDefault();
        event.stopPropagation();

        var $selectedAddress = $(this);

        $("#dialog-confirm-delete-address").dialog({
            resizable: false,
            height: "auto",
            width: 400,
            modal: true,
            dialogClass: wrapDialogClass + ' delete-address-confirm-dialog',
            buttons: [
            {
                text: Resources.CONFIRMATION_YES,
                "class": 'btn-type dark-line size-m',
                click: function() {
                    $(this).dialog("close");
                    var url = $selectedAddress.attr('href');
                    $.ajax({
                        type: "POST",
                        url: url
                    })
                    .done(function(data) {
                        location.reload();
                    })
                    .fail(function() {
                        console.log("AJAX call failed....");
                    });
                }
            }, 
            {
                text: Resources.CONFIRMATION_NO,
                "class": 'btn-type dark-line size-m',
                click: function() {
                    $(this).dialog('close');
                }
            }]
        });
    });
}
/**
 * @private
 * @function
 * @description Binds the events of the payment methods list (delete card)
 */
function initPaymentEvents() {

    var paymentList = $('.payment-list');
    if (paymentList.length === 0) {
        return;
    }

    $(".creditcardDelete").on("click", function(event) {
        event.preventDefault();
        event.stopPropagation();
        var $formPayment = $('form[name="payment-remove"]');

        $("#dialog-confirm-delete-creditcard").dialog({
            resizable: false,
            height: "auto",
            width: 400,
            modal: true,
            dialogClass: wrapDialogClass,
            buttons: [{
                text: Resources.CONFIRMATION_NO,
                "class": 'btn-type dark-line size-m',
                click: function() {
                    $(this).dialog('close');
                }
            }, {
                text: Resources.CONFIRMATION_YES,
                "class": 'btn-type dark-line size-m',
                click: function() {
                    $(this).dialog("close");

                    // fetch the selected creditcard from BE
                    event.preventDefault();
                    // override form submission in order to prevent refresh issues
                    var button = $formPayment.find('.delete');
                    $('<input/>').attr({
                        type: 'hidden',
                        name: button.attr('name'),
                        value: button.attr('value') || 'delete card'
                    }).appendTo($formPayment);
                    var data = $formPayment.serialize();
                    $.ajax({
                            type: 'POST',
                            url: $formPayment.attr('action'),
                            data: data
                        })
                        .done(function() {
                            page.redirect(Urls.paymentsList);
                        });

                }
            }]
        });
    });
}

function initializePaymentForm() {
    $('#CreditCardForm').on('click', '.cancel-button', function(e) {
        e.preventDefault();
        dialog.close();
    });

}
/**
 * @private
 * @function
 * @description Binds the events of the order, address and payment pages
 */
function initializeEvents() {
    toggleFullOrder();
    initializeAddressForm();
    initAddressEvents();
    initPaymentEvents();
    login.init();

    $('.back-to-top-btn').on('click', function() {
        $('html, body').animate({scrollTop: 0}, 'slow');
    });

    var $form = $("#RegistrationForm");
    var $dateDay = $form.find('[name$="customer_customerBirthday_day"]');
    var $dateMonth = $form.find('[name$="customer_customerBirthday_month"]');
    var $dateYear = $form.find('[name$="customer_customerBirthday_year"]');
    
    var barcodeElement = '.wrap-barcode .barcode-generate .barcode-image';
    var $barcodeElement = $(barcodeElement);
    var dataBarcode = $barcodeElement.attr('data-barcode');
    if ($barcodeElement.length > 0 && typeof dataBarcode !== "undefined" && typeof JsBarcode == 'function') {
        JsBarcode(
            barcodeElement,
            $barcodeElement.attr('data-barcode'),
            {
                width: 1.5,
                text: ' '
            }
        );
        var wrapDialogClass = 'reset-password wrap-from-barcode';
        $('body').on('click', '.barcode-open-dialog .button-open-dialog', function() {
            dialog.open({
                url: Urls.dialogAccountBarcode,
                options: {
                    width: 620,
                    dialogClass: wrapDialogClass,
                    show: {
                        effect: "fade",
                        duration: 500
                    },
                    hide: {
                        effect: "fade",
                        duration: 500
                    },
                    close: function() {
                        $('.wrap-from-barcode').removeClass("barcode-wrap")
                    },
                },
                callback: function() {
                    $barcodeElement.parent().clone().appendTo('.wrap-content .barcode');
                    setTimeout(function() {
                        $('.wrap-from-barcode').addClass("barcode-wrap");
                    }, 0);
                }
            });
        });
    }

    $form.find("button[name='submit']").on('click', function(event) {
        var oldBirthday = $form.find("input[name='old_birthday']").val();
        var $currentPasswordField = $form.find("input[name*='_login_currentpassword']");
        if ((oldBirthday == null || oldBirthday == 'null')) {
            var $dayOfBirth = $form.find('[name$="customer_customerBirthday_day"]');
            $dayOfBirth.closest('.field-wrapper').next('.form-caption.error-message').remove();
            var $day = $dayOfBirth.val();
            var $month = $form.find('[name$="customer_customerBirthday_month"]').val();
            var $year = $form.find('[name$="customer_customerBirthday_year"]').val();
            if ($day && $month && $year) {
                var params = {
                    day: $day,
                    month: $month,
                    year: $year
                };
                var url = Urls.checkBirthday;
                $.ajax({
                    type: "POST",
                    url: url,
                    data: params
                })
                .done(function(data) {
                    console.log("AJAX call success....");
                    if(data.status === false) {
                        var htmlDateError = '<div class="form-caption error-message">' + Resources.BIRHTDAYINVALID + '</div>';
                        $dayOfBirth.closest('.field-wrapper').after(htmlDateError);
                        return;
                    } else {
                        dialog.open({
                             url: Urls.confirmBirthdayDialog,
                             options: {
                             dialogClass: 'confirm-birthday before-confirm-password reset-password',
                             width: 560,
                             open: function() {
                                $("button[title='Close']").on('click', function() {
                                    dialog.close();
                                });
                                $("button[type='button'][name*='_confirm']").on('click', function() {
                                    if ($currentPasswordField.length > 0 && !$($currentPasswordField).val()) {
                                        checkCurrentPassword($form, $currentPasswordField);
                                    } else {
                                        $form.find("[name*='profile_confirm']").trigger('click');
                                    }
                                });
                            }
                        }
                    });
                    }
                })
                .fail(function() {
                    console.log("AJAX call failed....");
                });
            }
        } else if ($currentPasswordField.length > 0 && !$($currentPasswordField).val()) {
            checkCurrentPassword($form, $currentPasswordField);
        } else {
            $form.find("[name*='profile_confirm']").trigger('click');
        }
    });
}

function checkCurrentPassword($form, $currentPasswordField) {
    dialog.open({
        url: Urls.confirmPasswordDialog,
        options: {
            dialogClass: 'before-confirm-password reset-password',
            width: 560,
            open: function() {
                $("button[title='Close']").on('click', function() {
                    dialog.close();
                });
                $("#currentPasswordForm").on('submit', function(e) {
                    e.preventDefault();
                    var $this = $(this);
                    $.ajax({
                            url: $this.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: $this.serialize()
                        })
                        .done(function(respon) {
                            if (respon.success) {
                                $($currentPasswordField).val($this.find("input[name*='login_currentpassword']").val());
                                $form.find("[name*='profile_confirm']").trigger('click');
                            } else {
                                $this.find("input[name*='login_currentpassword']").after('<p class="error-message full-wrap">' + respon.message + '</p>');
                                setTimeout(function() {
                                    $this.find('.error-message').remove();
                                }, 2000);
                            }
                        })
                        .fail(function() {
                            console.log('AJAX call failed....');
                        });
                });
            }
        }
    });
}

var account = {
    init: function() {
        initializeEvents();
        giftcert.init();
        returnRequest.init();
        pointhistory.init();
    },
    initCartLogin: function() {
        login.init();
    }
};

module.exports = account;