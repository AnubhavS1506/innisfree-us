'use strict';

exports.init = function () {
	/*------ Style Guide Banner -----------*/
	$(window).bind('scroll', function(){
		var scrollTop = $(window).scrollTop();
		var tmpTop = 2 * scrollTop / 100;
		$('.bannerType2 img').css({'transform' : 'translateY(-'+tmpTop+'px) translateZ(0px)'})
	});
	
	
	/*------ Style Guide Elements -----------*/
	//product detail
	var pointInput = $('.review-grade.large').find('input');
	var reviewTxt = $('.review-grade.large').find('.grade-act');
	var pointWrite = {
		reset: function(){
			reviewTxt.css({width : pointInput.val() * 20 + '%'});
		},
		move: function(el, e){
			var reviewPos = el.offset();
			var reviewMouse = Math.round((e.pageX - reviewPos.left) / el.width() * 100);
			el.find('span').css('width', reviewMouse+'%');
		},
		click: function(el, e){
			var reviewPos = el.offset();
			var reviewMouse = Math.round((e.pageX - reviewPos.left) / el.width() * 100);
			for (var i = 0; i < 6; i++) {
				if (reviewMouse <= i * 20) {
					reviewMouse = i * 20;
					pointInput.val(i);
					break;
				}
			}
			el.find('span').stop().animate({width : reviewMouse + '%'}, 400);
			//밸리데이션 체크 될 수 있도록 강제 이벤트 적용
			pointInput.focus();
			pointInput.blur();
		}
	}

	$(document).on('mousemove', '.review-grade.large', function(e){
		pointWrite.move($(this), e);
	});
	$(document).on('mouseleave', '.review-grade.large', function(e){
		pointWrite.reset();
	});
	$(document).on('click', '.review-grade.large', function(e){
		pointWrite.click($(this), e);
	});
	$(document).on('click', '.addCart', function(){
		var e = $(this);
		addCart(e);
	});
	$(document).on('click', '.addWish', function(){
		var e = $(this);
		addWish(e);
	});
	
	
	//color swatch
	$('.color-chip').find('a').click(function(){
		$(this).parents('.color-chip').find('a').removeClass('active');
		$(this).addClass('active');
		return false;
	});
	
	// load more
	$('.load-more').click(function(){
		var _this = $(this);
		if(_this.hasClass('loading')) return false;
		_this.addClass('loading');
		setTimeout(function(){
			_this.removeClass('loading');
		}, 2500);
	});
	
	//slick slider
	$('.product-slider').slick({
		infinite: false,
		slidesToShow: 2
	});

	$('.colorchip-slider .color-slider').slick({
		// dots: false,
		// arrows: false,
		infinite: false,
		slidesToShow: 7

	});

	var slick_banner = $('.banner-slider').slick({
		autoplay: true,
		autoplaySpeed: 4700,
		pauseOnHover: false,
		dots: true,
		customPaging: function(slider, i){
			var thumb = i;
			return '<a><span class="side side-left"><span class="fill"></span></span><span class="side side-right"><span class="fill"></span></span></a>';
		}
	});
	
	/*------------ Product Style Guide -----------------*/
	$('select').niceSelect();
	
	$('.color-chip').find('a').click(function(){
		$(this).parents('.color-chip').find('a').removeClass('active');
		$(this).addClass('active');
		return false;
	});
	$('.colorchip-slider .color-slider').mCustomScrollbar({
		axis:"x",
		scrollButtons:{enable:true},
		theme:"dark-thin",
		advanced:{autoExpandHorizontalScroll:true}
	});
	// $('.colorchip-slider .color-slider').slick({
	// 	// dots: false,
	// 	// arrows: false,
	// 	infinite: false,
	// 	slidesToShow: 7
	// });

	// Add Tocart
	function addCart(e){
		var _this = $(e);
		if(_this.hasClass('add-on')){
			$(e).removeClass('add-on');
		} else {
			$(e).addClass('add-on');
			var _product = $(e).parents('.product-type');
			var _cart = $('<span class="button-effect ico-cart"></span>');
			_cart.appendTo(_product);
			setTimeout(function(){
				_product.find('.button-effect.ico-cart').addClass('in').delay(1200).queue(function(){
					$(this).addClass('out').delay(900).dequeue().queue(function(){
						_cart.remove();
					});
				});
			}, 10);
		}
	}
	// Add Wishlist
	function addWish(e){
		var _this = $(e);
		if(_this.hasClass('add-on')){
			$(e).removeClass('add-on');
		} else {
			$(e).addClass('add-on');
			var _product = $(e).parents('.product-type');
			var _cart = $('<span class="button-effect ico-wish"></span>');
			_cart.appendTo(_product);
			setTimeout(function(){
				_product.find('.button-effect.ico-wish').addClass('in').delay(1200).queue(function(){
					$(this).addClass('out').delay(900).dequeue().queue(function(){
						_cart.remove();
					});
				});
			}, 10);
		}
	}
};
