/**
 *    (c) 2009-2014 Demandware Inc.
 *    Subject to standard usage terms and conditions
 *    For all details and documentation:
 *    https://bitbucket.com/demandware/sitegenesis
 */
'use strict';

var countries = require('./countries'),
    dialog = require('./dialog'),
    minicart = require('./minicart'),
    page = require('./page'),
    rating = require('./rating'),
    searchplaceholder = require('./searchplaceholder'),
    searchsuggest = require('./searchsuggest'),
    tooltip = require('./tooltip'),
    util = require('./util'),
    validator = require('./validator'),
    tls = require('./tls'),
    login=require('./login'),
    productTile = require('./product-tile'),
    slick = require('./slick');

// if jQuery has not been loaded, load from google cdn
if (!window.jQuery) {
    var s = document.createElement('script');
    s.setAttribute('src', 'https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js');
    s.setAttribute('type', 'text/javascript');
    document.getElementsByTagName('head')[0].appendChild(s);
}

require('./jquery-ext')();
require('./cookieprivacy')();
require('./captcha')();

function initializeEvents() {
    $('select').niceSelect();
    $('.mini-cart-products').mCustomScrollbar();
    var controlKeys = ['8', '13', '46', '45', '36', '35', '38', '37', '40', '39'];

    $('body')
        .on('keydown', 'textarea[data-character-limit]', function(e) {
            var text = $.trim($(this).val()),
                charsLimit = $(this).data('character-limit'),
                charsUsed = text.length;

            if ((charsUsed >= charsLimit) && (controlKeys.indexOf(e.which.toString()) < 0)) {
                e.preventDefault();
            }
        })
        .on('change keyup mouseup', 'textarea[data-character-limit]', function() {
            var text = $.trim($(this).val()),
                charsLimit = $(this).data('character-limit'),
                charsUsed = text.length,
                charsRemain = charsLimit - charsUsed;

            if (charsRemain < 0) {
                $(this).val(text.slice(0, charsRemain));
                charsRemain = 0;
            }

            $(this).next('div.char-count').find('.char-remain-count').html(charsRemain);
        });

    // Email optin from footer section
    $(document).on("submit",".optinfooter", function(event){
        event.preventDefault();
        var email = $('.optInEmailFooter').val(); 
        var url = util.appendParamToURL(Urls.optInFooter, 'email', email);
        $.ajax({
          type: "POST",
          url: url
        })
        .done(function() {
            $('.optInEmailFooter').val('');
            dialog.open({
                html: $('.success-message').html(),
                options: {
                    width: 400,
                    dialogClass: 'wrap-from-dialog success-message-block'
                }
            });
        })
     });
  
    // Email optin from header section
    $(document).on("submit",".optinheader", function(event){
        event.preventDefault();
        var email = $('.optInEmailHeader.valid').val(); 
        var url = util.appendParamToURL(Urls.optInHeader, 'email', email);
        $.ajax({
          type: "POST",
          url: url
        })
        .done(function() {
            $('.email.optinheader').hide();
            $('.sign-up-now').hide();
            $('.success-message').removeClass('visually-hidden');
        })        
    });
    
    // Popup default
    $('body').on('click', '.popup-default', function(e){
        e.preventDefault();
        var url = $(this).attr('href');
        dialog.open({
            url: url,
            options: {
                dialogClass: 'default',
            }
        });
    })
    
    /**
     * initialize search suggestions, pending the value of the site preference(enhancedSearchSuggestions)
     * this will either init the legacy(false) or the beta versions(true) of the the search suggest feature.
     * */
    var $searchContainerDesktop = $('#d-header .header-search .search-layer-container');
    var $searchContainerMobile = $('#m-header .header-search .search-layer-container');
    $(window).resize(function() {
        var winW = $(window).width();
        if (winW < 1025) {
            searchsuggest.init($searchContainerMobile, Resources.SIMPLE_SEARCH);
        } else {
            searchsuggest.init($searchContainerDesktop, Resources.SIMPLE_SEARCH);
        }
    });
    $(window).load(function() {
        var winW = $(window).width();
        if (winW < 1024) {
            searchsuggest.init($searchContainerMobile, Resources.SIMPLE_SEARCH);
        } else {
            searchsuggest.init($searchContainerDesktop, Resources.SIMPLE_SEARCH);
        }
        if($("#redirect-dialog").length != 0) {
            $("#redirect-dialog").dialog({
                modal: true
              });
          } 
        
    });
    //toggle search layer
    $(".search").off('click.SearchLayer').on('click.SearchLayer',function(){
        $('.search-layer').toggleClass('open');
        var $searchInput;
        if (util.isMobile()) {
            $searchInput = $('.search-layer form input').first();
        } else {
            $searchInput = $('.search-layer form input');
        }
        $searchInput.focus();
    });
    $('body').on('click', '.search-layer .close-button, .search-layer .search-close', function(){
        var $searchField = $('.search-field');
    	$searchField.val('');
        $('.search-layer').toggleClass('open');
    })

    // add show/hide navigation elements
    $('.secondary-navigation .toggle').click(function() {
        $(this).toggleClass('expanded').next('ul').toggle();
    });

    // add generic toggle functionality
    $('.toggle').next('.toggle-content').hide();
    $('.toggle').click(function() {
        $(this).toggleClass('expanded').next('.toggle-content').toggle();
    });
    
    // subscribe email box
    var $subscribeEmail = $('.subscribe-email');
    if ($subscribeEmail.length > 0) {
        $(document).on('focus', '.subscribe-email', function() {
            var val = $(this).val();
            if (val.length > 0 && val !== Resources.SUBSCRIBE_EMAIL_DEFAULT) {
                return; // do not animate when contains non-default value
            }

            $(this).animate({
                color: '#999999'
            }, 500, 'linear', function() {
                $(this).val('').css('color', '#333333');
            });

        }).on('blur', '.subscribe-email', function() {
            var val = $.trim($(this).val());
            if (val.length > 0) {
                return; // do not animate when contains value
            }
            $(this).val(Resources.SUBSCRIBE_EMAIL_DEFAULT)
                .css('color', '#999999')
                .animate({
                    color: '#333333'
                }, 500, 'linear');
        }).on('input propertychange', '.subscribe-email', function(){
            var $subscribeEmailbtn = $(this).closest('form').find('[type="submit"]');
            var regexpEmail = new RegExp(Resources.REGEX_EMAIL);
            if (!regexpEmail.test($(this).val())) {
                $subscribeEmailbtn.attr('disabled','disabled');
            } else {
                $subscribeEmailbtn.removeAttr('disabled');
            }
        })
    }
    // header util helper
    $('.header-util .help-arr').click(function(){
        $(this).closest('.help').toggleClass('active');
        return false;
    });
    $('.header-util .help').on('mouseenter', function(){
        //close minicart popup
        minicart.close();
    });

    $('#reward-info').on('click', function() {
		$('.rewards-container-summary').removeClass('visually-hidden');
    })
    $('.rewards-container-summary > .close-button').on('click', function() {
		$('.rewards-container-summary').addClass('visually-hidden');
    })

    $('.privacy-policy').on('click', function(e) {
        e.preventDefault();
        dialog.open({
            url: $(e.target).attr('href'),
            options: {
                height: 600
            }
        });
    });

    // main menu toggle
    $('.menu-toggle').on('click', function() {
        $('#wrapper').toggleClass('menu-active');
    });
    $('.menu-category li .menu-item-toggle').on('click', function(e) {
        e.preventDefault();
        var $parentLi = $(e.target).closest('li');
        $parentLi.siblings('li').removeClass('active').find('.menu-item-toggle').removeClass('fa-chevron-up active').addClass('fa-chevron-right');
        $parentLi.toggleClass('active');
        $(e.target).toggleClass('fa-chevron-right fa-chevron-up active');
    });
    $('.vertical-promotion')
    .jcarousel({
        vertical: true,
        wrap: 'both'
    })
    .jcarouselAutoscroll({
        interval: 5000,
        target: '+=1',
        autostart: true
    });
    $('a#sign-in-cart-bag').click(function(e) {
        e.preventDefault();
        $('.mini-cart').find('.mini-cart-content').slideUp();
        $('.user-account').trigger('click');
    });
    $('.select-country').off('click.toggleClass').on('click.toggleClass', function() {
        $(this).find('.country-layer').toggleClass('active');
    })
    $('.footer-item .title').off('click.toggleClass').on('click', function() {
        $(this).parent().toggleClass('current');
    });
    var $brandStory = $('.brand-story');
    var $bsCon = $('.bs-con');
    var $brandBg = $('.brand-bg');
    var $topBrandStory = $('.top-brand-story');
    var brandBannerChk = true;
    var $brandBannerContainer  = $('.brand-banner-container');
    $('.bs-nav').find('li a').click(function(){
        $('.bs-nav li a').removeClass('active');
        $bsCon.find('.con > div').removeClass('active');
        var _idx = $(this).parent('li').index() + 1;
        $(this).addClass('active');
        $bsCon.find('.con > div:eq('+_idx+')').addClass('active');
    });

    if ($brandBannerContainer.hasClass('hide')) {
        brandBannerChk = false;
    }

    $topBrandStory.find('.bs-show').click(function(){
		if(!brandBannerChk){
			$brandStory.removeClass('hide');
            $brandBannerContainer.slideDown(400);
		    $bsCon.find('.con > div').eq(0).addClass('active');
			brandBannerChk = true;
		} else {
			$brandStory.addClass('hide');
            $brandBannerContainer.slideUp(400);
			$bsCon.find('.con > div').removeClass('active');
			brandBannerChk = false;
		}
        $topBrandStory.find('.bs-show').toggleClass('active');
	});
	$('.bs-close').click(function(){
		$brandBannerContainer.slideUp(400);
		setTimeout(function(){
			$brandStory.addClass('hide');
			$('.bs-nav li a').removeClass('active');
			$bsCon.find('.con > div').removeClass('active');
			
			$('.top-banner').find('.bs-show').removeClass('active');
			brandBannerChk = false;
		}, 500);
	});

    var secH, scrollReady;
    var timeout = false;
    var rtime = new Date();
    var delta = 100;
    var winScroll = $(window).scrollTop();
    var brandHeight = $brandBg.height();
    $(window).scroll(function() {
        winScroll = $(window).scrollTop();
        brandHeight = $brandBg.height();

        rtime = new Date();
        if (timeout === false) {
            timeout = true;
            setTimeout(scrollEnd, delta);
        }
    });

    function scrollEnd() {
        if (new Date() - rtime < delta) {
            setTimeout(scrollEnd, delta);
        } else {
            timeout = false;

            if (winScroll > brandHeight && brandBannerChk) {
                hiddenBrand();
            }
        }

    }

    function hiddenBrand(){
    	$brandStory.addClass('hide');
        $brandBg.hide();
        $('.bs-nav li a').removeClass('active');
        $bsCon.find('.con div').removeClass('active');
        $('body, html').stop().animate({scrollTop : winScroll - brandHeight}, 0);
        brandBannerChk = false;
    }

    $('.pi-slot').on('carouselLoaded', function() {
        slick.initProductCarousel();
        productTile.init();
    });

    slick.initProductCarousel();

    $(window).resize(function() {
        var winW = $(window).width();
        if (winW < 1024) {
            mainBestSellerMobile();
        } else {
            mainBestSellerWeb();
        }
    });
    $(window).load(function() {
        var winW = $(window).width();
        if (winW < 1024) {
            mainBestSellerMobile();
        } else {
            mainBestSellerWeb();
        }
    });
    
    $('.fevorites_slider > ul').slick({
        accessibility: false,
        infinite: false,
        slidesToShow: 4,
        responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    infinite: false,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });
    
    $('.colorchip-slider .color-slider').mCustomScrollbar({
        axis: "x",
        scrollButtons: {
            enable: true
        },
        theme: "dark-thin",
        advanced: {
            autoExpandHorizontalScroll: true
        }
    });
    $('li.message .i-message').on('click', function() {
        dialog.open({
            html: $('.from-dialog').html(),
            options: {
                width: 400,
                dialogClass: 'wrap-from-dialog',
                show: {
                    effect: "fade",
                    duration: 500
                },
                hide: {
                    effect: "fade",
                    duration: 500
                },
                open: function(type, data) {
                    validator.init();
                    var $form = $('.subscription');
                    $form.validate().form();
                }
            }
        });
    });
 // mobile check
    var _mobileChk;
    var _winW = $(window).width();
    mobileChk();
    $(window).resize(function(){
        _winW = $(window).width();
        mobileChk();
    });
    function mobileChk(){
        if(_winW > 1024){
            _mobileChk = false;
        } else {
            _mobileChk = true;
        }
    }
    if(_mobileChk){
        touchMobile("button");
    }

    $("#key-ingredient-show-all").on('click', function() {
    	$("#ingredients-tab").click();
    });

 // gnb hover 
    $('#gnb').find('> ul > li').hover(function(){
        //close help util and mini cart popup
        $('.header-util .help').removeClass('active');
        minicart.close();
        if(!_mobileChk){
            $(this).addClass('hover');
            var _line = $(this).find('.sub-category .fl').length;
            var _height = 0;
            for(var i = 0; i < _line; i++){
                var _conH = $(this).find('.sub-category .fl').eq(i).height();
                if(_height < _conH)    _height = _conH;
            }
            $(this).find('.sub-category .fl').height(_height);
        }
    }, function(){
        $(this).removeClass('hover');
    });
    // gnb mobile click
    $('#gnb').find('> ul > li > p > a').click(function(){
        var _noneSub = $(this).parents('li').hasClass('none-sub');
        if(_mobileChk && !_noneSub){
            if($(this).parents('li').hasClass('active')){
                $(this).parents('li').removeClass('active');
            } else {
                $(this).parents('li').addClass('active');
            }
            return false;
        }
    });
    // header util mobile subcategory click
    $('.header-util-mobile').find('.m-display > a').click(function(){
        if($(this).hasClass('active')){
            $(this).removeClass('active').next('.sub-category').removeClass('active');
        } else {
            $(this).addClass('active').next('.sub-category').addClass('active');
        }
        return false;
    });
    $('.header-util-mobile').find('.m-display .sub-category .title a').click(function(){
        var _noneSub = $(this).hasClass('none-sub');
        if(_mobileChk && !_noneSub){
            if($(this).hasClass('active')){
                $(this).removeClass('active');
                $(this).parents('.title').next('ul').removeClass('active');
            } else {
                $(this).addClass('active');
                $(this).parents('.title').next('ul').addClass('active');
            }
            return false;
        }
    });
    // gnb mobile subcategory click
    $('#gnb').find('.sub-category .title a').click(function(){
        var _noneSub = $(this).parent('.title').hasClass('none-sub');
        if(_mobileChk && !_noneSub){
            $(this).parents('.title').toggleClass('active');
            return false;
        }
    });
    // gnb mobile subcategory Active
    $('#m-header').find('.open-sidemenu').click(function(){
        if($(this).hasClass('active')){
            sidemenuOff();
        } else {
            sidemenuOn();
        }
        return false;
    });
    $('.snb-close, .bg-cover').click(function(){
        sidemenuOff();
    });
    function sidemenuOn(){
        $('.open-sidemenu').addClass('active');
        $('#d-header').addClass('active');
        $('.bg-cover').addClass('active');
        $('.snb-close').addClass('active');
        $('#wrapper').addClass('hidden');
        if($('#wrapper').find('.bg-cover').length > 0){
            $('#wrapper').find('.bg-cover').addClass('active')
        } else {
            $('#wrapper').append('<span class="bg-cover active"></span>');
        };
    }
    function sidemenuOff(){
        $('.open-sidemenu').removeClass('active');
        $('#d-header').removeClass('active');
        $('.bg-cover').removeClass('active');
        $('.snb-close').removeClass('active');
        $('#wrapper').removeClass('hidden');
        $('body').find('.bg-cover').removeClass('active')
    }
}
/** Touch Event on Mobile **/
function touchMobile (el){
    $('body').on("touchstart", el, function() {
      $(this).addClass("touch");
    }).on("touchend", el, function() {
      $(this).removeClass("touch");
    });
}
/** Carousel Resize Mobile **/
function mainBestSellerMobile() {
    $('.main-best-seller').addClass('mobile');
    if ($('.main-best-seller').hasClass('web')) {
        console.log('mobile');
        $('.main-best-seller .bestSeller-slider ul').find('li.blank').remove();
        $('.main-best-seller .bestSeller-slider ul').slick('setPosition');
        $('.main-best-seller').removeClass('web');
    }
}
/** Carousel Resize Web **/
function mainBestSellerWeb() {
    $('.main-best-seller').addClass('web');
    if ($('.main-best-seller').hasClass('mobile')) {
        console.log('web');
        $('.main-best-seller').removeClass('mobile');
        setTimeout(function() {
            $('.main-best-seller .bestSeller-slider ul').slick('setPosition');
            $('.main-best-seller .bestSeller-slider ul').slick('slickAdd');
        }, 100);
    }
}

/**
 * @private
 * @function
 * @description Adds class ('js') to html for css targeting and loads js specific styles.
 */
function initializeDom() {
    // add class to html for css targeting
    $('html').addClass('js');
    if (SitePreferences.LISTING_INFINITE_SCROLL) {
        $('html').addClass('infinite-scroll');
    }
    // load js specific styles
    util.limitCharacters();
}

var pages = {
    account: require('./pages/account'),
    cart: require('./pages/cart'),
    checkout: require('./pages/checkout'),
    compare: require('./pages/compare'),
    product: require('./pages/product'),
    registry: require('./pages/registry'),
    search: require('./pages/search'),
    storefront: require('./pages/storefront'),
    wishlist: require('./pages/wishlist'),
    storelocator: require('./pages/storelocator'),
    customerservice: require('./pages/customerservice'),
    styleguide: require('./pages/styleguide'),
    mycuishon: require('./pages/mycuishon')
};

var app = {
    init: function() {
        if (document.cookie.length === 0) {
            $('<div/>').addClass('browser-compatibility-alert').append($('<p/>').addClass('browser-error').html(Resources.COOKIES_DISABLED)).appendTo('#browser-check');
        }
        initializeDom();
        initializeEvents();

        // init specific global components
        countries.init();
        tooltip.init();
        minicart.init();
        validator.init();
        rating.init();
        searchplaceholder.init();
        login.init();
        login.initLoginDialog();
        productTile.init();
        // execute page specific initializations
        $.extend(page, window.pageContext);
        var ns = page.ns;
        if (ns && pages[ns] && pages[ns].init) {
            pages[ns].init();
        }

        // Check TLS status if indicated by site preference
        if (SitePreferences.CHECK_TLS === true) {
            tls.getUserAgent();
        }
    }
};

// general extension functions
(function() {
    String.format = function() {
        var s = arguments[0];
        var i, len = arguments.length - 1;
        for (i = 0; i < len; i++) {
            var reg = new RegExp('\\{' + i + '\\}', 'gm');
            s = s.replace(reg, arguments[i + 1]);
        }
        return s;
    };
})();

// initialize app
$(document).ready(function() {
    app.init();
});