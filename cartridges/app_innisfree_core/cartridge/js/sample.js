'use strict';

var dialog = require('./dialog'),
    util = require('./util'),
    $cache = {};

function InitializeDom() {
    $cache.body = $('.bite-size-bar-body');
    $cache.header = $('.cart-top-section');
    $cache.quickview = $('.sampleview');
}

var addSampleToCart = function (e) {
    e.preventDefault();
    var $this = $(this);
    if ($this.attr('disabled')) {
        return false;
    }
    $.ajax({
        type: 'POST',
        url: util.ajaxUrl(Urls.addSample),
        data: {
            'pid': $this.data('product-id') || '',
            'sample': true
        },
        success: function (response) {
            if (!response.hasOwnProperty('invalid')) {
                //hide add button, show remove button
                $this.removeClass('addsamplebutton');
                $this.addClass('removesamplebutton').off('click').on('click', removeSampleFromCart);
                $this.html('<span class="check on"></span>');
                //show the check icon
                $this.parent().siblings('span.check').addClass('on');
                checkSampleButton(response.limit);
                var redirectUrl = window.location.href;    
                if (redirectUrl.indexOf('?') > -1 && redirectUrl.indexOf('addSample=true') == -1){
                	redirectUrl += '&addSample=true'
                } else if (redirectUrl.indexOf('addSample=true') == -1) {
                	redirectUrl += '?addSample=true'
                }
                window.location.href = redirectUrl;
            }
        }
    });
}

function removeSampleFromCart(e) {
    e.preventDefault();
    var $this = $(this);
    $.ajax({
        type: 'POST',
        url: util.ajaxUrl(Urls.removeSample),
        data: {
            'pid': $this.data('product-id') || '',
            'sample': true
        },
        success: function (response) {
            //hide remove button, show add button
            $this.addClass('addsamplebutton').off('click').on('click', addSampleToCart);
            $this.html(Resources.SAMPLE_ADD);
            $this.removeClass('removesamplebutton');
            //remove check icon
            $this.parent().siblings('span.check').removeClass('on');
            checkSampleButton(response.limit);
            location.reload();
        }
    });
}

function checkSampleButton(limit) {
    //if samples exceed the limitation, disable add-sample buttons
    if ($('.bite-size-bar-body .removesamplebutton').length == limit) {
        $('.bite-size-bar-body .addsamplebutton').each(function() {
            $(this).removeClass('dark-line');
            $(this).addClass('light-gray');
            $(this).attr('disabled', true);
            $(this).closest('.img').addClass('sample-disabled');
        });
        if ($('.samplemessage').hasClass('show')) {
            $('.samplemessage').removeClass('show');
            $('.samplemessage').addClass('hide');
        }
    }
    else {
        //else, enable add-sample buttons
        $('.bite-size-bar-body .addsamplebutton').each(function() {
            $(this).removeClass('light-gray');
            $(this).addClass('dark-line');
            $(this).attr('disabled', false);
            $(this).closest('.img').removeClass('sample-disabled');
        });
        if ($('.samplemessage').hasClass('hide')) {
            $('.samplemessage').removeClass('hide');
            $('.samplemessage').addClass('show');
        }
    }
}

function disablesampleProducts() {
    $('.bite-size-bar-body .addsamplebutton').each(function() {
        $(this).removeClass('dark-line');
        $(this).addClass('light-gray');
        $(this).attr('disabled', true);
    });
} 

function InitializeEvents() {
    $('.bite-size-bar-body .addsamplebutton').on('click', addSampleToCart);
    $('.bite-size-bar-body .removesamplebutton').on('click', removeSampleFromCart);
    checkSampleButton($cache.body.data('limit'));
    //toggle bite size bar content
    $cache.header.on('click', function () {
        var $this = $(this);
        var targetSelector = $this.data('target');
        if (!targetSelector) {
            return;
        }
        var $targetContainer = $(targetSelector);
        $cache.header.each(function(index, el) {
            var $el = $(el);
            var currentSelector = $el.data('target');
            var $currentSelector = $(currentSelector);
            if (currentSelector != targetSelector && $currentSelector.is(':visible')) {
                $currentSelector.toggleClass('hide');
                updateTabState($el);
            } else if (currentSelector == targetSelector) {
                $targetContainer.toggleClass('hide');
                updateTabState($this);
            }
        });
    });
    
    if (window.location.href.indexOf('addSample=true') !== -1) {
    	$cache.header.first().trigger('click');
    }
    
    function updateTabState($el) {
        if (!$el) {
            return;
        }
        if ($el.hasClass('open')) {
            $el.removeClass('open');
            $el.find('.sign .fa').removeClass('fa-minus').addClass('fa-plus');
        } else {
            $el.addClass('open');
            $el.find('.sign .fa').removeClass('fa-plus').addClass('fa-minus');
        }
    }
    $cache.quickview.on('click', function () {
        dialog.open({
            url: $(this).data('url'),
            options: {
                width: 720,
                dialogClass: 'sample',
            }
        });
    });

    if ($('#userAuthenticated').val() == 'false') {
        disablesampleProducts();
    }
}

/**
 * @function
 * @description Binds the click event to a given target for the add-to-cart handling
 */
module.exports = function () {
    InitializeDom();
    InitializeEvents();
};
