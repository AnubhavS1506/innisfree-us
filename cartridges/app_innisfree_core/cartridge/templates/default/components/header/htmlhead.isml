<iscontent type="text/html" charset="UTF-8" compact="true"/>
<iscomment>
The <!—BEGIN/END… comments are control statements for the build cartridge which can be found in xChange https://xchange.demandware.com/docs/DOC-5728 or checked out from SVN at https://svn2.hosted-projects.com/cs_europe/DWTechRepository/cartridges/build_cs
If you are not using the build cartridge the comments can be safely removed.
</iscomment>

<meta charset=UTF-8>

<iscomment>See https://github.com/h5bp/html5-boilerplate/blob/5.2.0/dist/doc/html.md#x-ua-compatible</iscomment>
<meta http-equiv="x-ua-compatible" content="ie=edge">

<iscomment>See https://github.com/h5bp/html5-boilerplate/blob/5.2.0/dist/doc/html.md#mobile-viewport</iscomment>
<iscomment>the page title calculated by the app </iscomment>

<isif condition="${dw.system.System.getInstanceType() != dw.system.System.PRODUCTION_SYSTEM}">
	<title>${pdict.CurrentPageMetaData.title} | ${Resource.msg('global.site.name', 'locale', null)} | ${Resource.msg('revisioninfo.revisionnumber', 'revisioninfo', null)}</title>
<iselse/>
  <title><isprint value="${pdict.CurrentPageMetaData.title}" encoding="off" /></title>
</isif>

<meta name="viewport" content="width=device-width, initial-scale=1">

<iscomment>FAVICON ICON: (website icon, a page icon or an urlicon) 16x16 pixel image icon for website</iscomment>
<link href="${URLUtils.staticURL('/images/innisfree_favicon.png')}" rel="shortcut icon" />

<iscomment>include all meta tags</iscomment>
<iscomment>
	This Content-Type setting is optional as long as the webserver transfers
	the Content-Type in the http header correctly. But because some browsers or
	proxies might not deal with this setting in the http header correctly, a
	second setting can help to keep everything just fine.
</iscomment>

<iscomment>Automatic generation for meta tags.</iscomment>
<meta name="description" content=" <isif condition="${!empty(pdict.CurrentPageMetaData.description)}">${pdict.CurrentPageMetaData.description}</isif> ${Resource.msg('global.storename','locale',null)}"/>
<meta name="keywords" content=" <isif condition="${!empty(pdict.CurrentPageMetaData.keywords)}">${pdict.CurrentPageMetaData.keywords}</isif> ${Resource.msg('global.storename','locale',null)}"/>

<iscomment>
	Add your own meta information here, e.g. Dublin-Core information
</iscomment>

<iscomment>STYLE SHEETS ARE PLACED HERE SO THAT jQuery, Power Review and other RichUI styles do not overwrite the definitions below.</iscomment>

<iscomment>DEFAULT STYLESHEETS INCLUDED ON ALL PAGES</iscomment>

<isinclude template="components/header/htmlhead_UI"/>

<!--  UI -->
<!--[if gt IE 9]><!-->
<link rel="stylesheet" href="${URLUtils.staticURL('/css/style.css')}" />
<!--
<![endif]
<!-->

<!--[if lt IE 6]> force above endif <![endif]-->

<!--[if lte IE 9]>
<link rel="stylesheet" href="${URLUtils.staticURL('/css/style.ie.css')}" />
<![endif]-->

<!--[if lte IE 8]>
<script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js" type="text/javascript"></script>
<script src="https://cdn.rawgit.com/chuckcarpenter/REM-unit-polyfill/master/js/rem.min.js" type="text/javascript"></script>
<![endif]-->
<iscomment>Insert meta tag for the "Google-Verification" feature to verify that you are the owner of this site.</iscomment>
    <script>

    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){

    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),

    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)

    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', '${dw.system.Site.current.preferences.custom.googleAnalyticCode}');

    ga('send', 'pageview');

  </script>

<script>
	var gtmID = '${dw.system.Site.current.preferences.custom.gtmId}';
	var gtmCall = function(w,d,s,l,i){
		w[l]=w[l]||[];
		w[l].push({'gtm.start': new Date().getTime(),
					event:'gtm.js'
				});
		var f=d.getElementsByTagName(s)[0],
			j=d.createElement(s),
			dl=l!='dataLayer'?'&l='+l:'';
			j.async=true;
			j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;
			f.parentNode.insertBefore(j,f);
	};
	gtmCall(window,document,'script','dataLayer','${dw.system.Site.current.preferences.custom.gtmId}'); 
</script>


<script type="text/javascript">
  WebFontConfig = {
    google: { families: [ 'Lato:100,300,700,100italic,300italic:latin', 'Crete+Round:400,400italic:latin' ] }
  };
  (function() {
    var wf = document.createElement('script');
    wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
      '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
    wf.type = 'text/javascript';
    wf.async = 'true';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wf, s);
  })();
</script>

<isif condition="${'GoogleVerificationTag' in dw.system.Site.current.preferences.custom && dw.system.Site.current.preferences.custom.GoogleVerificationTag!=''}">
    <meta name="google-site-verification" content="<isprint value="${dw.system.Site.current.preferences.custom.GoogleVerificationTag}"/>" />
</isif>

<iscomment>Gather device-aware scripts</iscomment>
<isinclude url="${URLUtils.url('Home-SetLayout')}"/>
<isinclude template="include/yotpoheader"/>


