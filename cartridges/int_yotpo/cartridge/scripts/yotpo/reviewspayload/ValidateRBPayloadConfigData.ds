/**
* 	ValidateRBPayloadConfigData.ds
*
* 	It validates if the reviews and bottomline should be imported for current locale in Yotop Configuration object. 
* 	It returns error if the mandatory data is missing. It will also indicate skipping orders for current 
* 	locale if the flag in configuration indicate so. 
*	
* 	@input YotpoConfiguration : dw.object.CustomObject The Yotpo configuration object holding all necessary configuration data.
*
*	@output skipCurrentLocale : Boolean The flag to indicate if the import payload should be skipped for current locale. 
*
*/
importPackage( dw.system );
importPackage( dw.object );

importScript("yotpo/utils/YotpoUtils.ds");

function execute( pdict : PipelineDictionary ) : Number
{
	var isDebugEnabled : Boolean = dw.system.Site.getCurrent().preferences.custom.yotpoDebugLogEnabled;
	
	var yotpoConfiguration : CustomObject = pdict.YotpoConfiguration;
	
	var validationResult : Boolean = validateMandatoryConfigData(yotpoConfiguration);
	
	if(!validationResult){
		Logger.getLogger("YotpoIntegration")
			.error( "ValidateRBPayloadConfigData: The current locale missing mandatory data therefore aborting the process." );
			
			return PIPELET_ERROR;
	}
	
	var skip : Boolean = false;
	
	if(!( yotpoConfiguration.custom.enableReviews && yotpoConfiguration.custom.enableBottomLine ) ){
		
		if(isDebugEnabled){
			Logger.getLogger("YotpoIntegration")
				.debug( "ValidateRBPayloadConfigData: Skipping Reviews and BottomLine for current locale" + 
					"Locale ID - " + yotpoConfiguration.custom.localeID +
						"Reviews Flag - " + yotpoConfiguration.custom.enableReviews +
							"Bottomline Flag - " + yotpoConfiguration.custom.enableBottomLine);
		}
		
		skip = true;
	}
	
	pdict.skipCurrentLocale = skip;
	
   	return PIPELET_NEXT;
}
