'use strict';

/**
 * @module scripts/yotpo/common/CommonModel
 *
 * This is a common model for Yotpo cartridge.
 */

/**
 * It reads the Yotpo configurations from Custom Objects.
 *
 * @returns  {Object} YotpoConfigurationList : The list of CustomObject holding Yotpo configurations.
 */
function loadAllYotpoConfigurations() {
    var CustomObjectMgr = require('dw/object/CustomObjectMgr');
    var YotpoLogger = require('~/cartridge/scripts/yotpo/utils/YotpoLogger');
    var Constants = require('~/cartridge/scripts/yotpo/utils/Constants');

    var logLocation = 'CommonModel~loadAllYotpoConfigurations';
    var yotpoConfigurations = CustomObjectMgr.getAllCustomObjects(Constants.YOTPO_CONFIGURATION_OBJECT);

    if (yotpoConfigurations == null || !yotpoConfigurations.hasNext()) {
        YotpoLogger.logMessage('The Yotpo configuration does not exist, therefore cannot proceed further.', 'error', logLocation);
        throw Constants.YOTPO_CONFIGURATION_LOAD_ERROR;
    }

    YotpoLogger.logMessage('Yotpo Configurations count - ' + yotpoConfigurations.count, 'debug', logLocation);

    var yotpoConfigurationList = yotpoConfigurations.asList();
    yotpoConfigurations.close();// closing list...

    return yotpoConfigurationList;
}

/**
 * It loads the Yotpo configuration by locale ID from Custom Objects.
 *
 * @param {string} localeID - current locale id
 * @returns {Object} YotpoConfiguration The CustomObject holding Yotpo configuration.
 */
function loadYotpoConfigurationsByLocale(localeID) {
    var CustomObjectMgr = require('dw/object/CustomObjectMgr');
    var YotpoLogger = require('~/cartridge/scripts/yotpo/utils/YotpoLogger');
    var Constants = require('~/cartridge/scripts/yotpo/utils/Constants');

    var logLocation = 'CommonModel~loadYotpoConfigurationsByLocale';
    var yotpoConfiguration = CustomObjectMgr.getCustomObject(Constants.YOTPO_CONFIGURATION_OBJECT, 'default');

    if (yotpoConfiguration == null) {
        YotpoLogger.logMessage('The Yotpo configuration does not exist for Locale, cannot proceed further. Locale ID is: ' + localeID, 'error', logLocation);
        throw Constants.YOTPO_CONFIGURATION_LOAD_ERROR;
    }

    return yotpoConfiguration;
}

/**
 * It reads the Yotpo job configurations from Custom Objects and read the last execution date time of job.
 *
 * @returns {Object} : The last execution and current date time.
 */
function loadYotpoJobConfigurations() {
    var Calendar = require('dw/util/Calendar');
    var CustomObjectMgr = require('dw/object/CustomObjectMgr');
    var YotpoLogger = require('~/cartridge/scripts/yotpo/utils/YotpoLogger');
    var Constants = require('~/cartridge/scripts/yotpo/utils/Constants');

    var logLocation = 'CommonModel~loadYotpoJobConfigurations';
    var yotpoJobConfiguration = CustomObjectMgr.getCustomObject(Constants.YOTPO_JOBS_CONFIGURATION_OBJECT, Constants.YOTPO_JOB_CONFIG_ID);

    if (yotpoJobConfiguration == null) {
        YotpoLogger.logMessage('The Yotpo job configuration does not exist, cannot proceed.', 'error', logLocation);
        return false;
    }

    var orderFeedJobLastExecutionTime = yotpoJobConfiguration.custom.orderFeedJobLastExecutionDateTime;
    var	helperCalendar = new Calendar();
    var currentDateTime = helperCalendar.getTime();

    return {
        orderFeedJobLastExecutionTime: orderFeedJobLastExecutionTime,
        currentDateTime: currentDateTime
    };
}

/* Module Exports */
exports.loadAllYotpoConfigurations = loadAllYotpoConfigurations;
exports.loadYotpoJobConfigurations = loadYotpoJobConfigurations;
exports.loadYotpoConfigurationsByLocale = loadYotpoConfigurationsByLocale;
