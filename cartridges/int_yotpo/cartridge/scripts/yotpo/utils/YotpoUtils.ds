/**
 * This script provides utility functions shared across other Yotpo scripts.
 * Reused script components for Yotpo should be contained here, while this
 * script is imported into the requiring script.
 */
 
importPackage( dw.system );
importPackage( dw.object );
importPackage( dw.catalog );

/**
 * Validates the mandatory data in yotpoConfiguration, it returns false if that is missing. 
 *
 */
function validateMandatoryConfigData( yotpoConfiguration : CustomObject ) : Boolean
{	
	var appKey : String =  yotpoConfiguration.custom.appKey;
	var clientSecretKey : String = yotpoConfiguration.custom.clientSecretKey;
	
	if(empty(appKey) || empty(clientSecretKey)){
		return false;	
	}

   	return true;
}

/**
 * Validates the mandatory data related to order feed job configuraiton, it returns false if that is missing. 
 *
 */
function validateOrderFeedJobConfiguration( orderFeedJobLastExecutionDateTime : Number ) : Boolean
{
	
	if(empty(orderFeedJobLastExecutionDateTime)){
		return false;	
	}

   	return true;
}

/**
 * Retrieves the appKey based on the current locale 
 *
 */
function getAppKeyForCurrentLocale( currentLocaleID : String ) : String
{
	var isDebugEnabled : Boolean = dw.system.Site.getCurrent().preferences.custom.yotpoDebugLogEnabled;
	
	if(empty(currentLocaleID)){
		Logger.getLogger("YotpoIntegration")
			.error( "YotpoUtils: getAppKeyForCurrentLocale - The current LocaleID is missing, therefore cannot proceed." );
		
		return "";
	} else {
		if(isDebugEnabled){
			Logger.getLogger("YotpoIntegration")
				.debug( "YotpoUtils: getAppKeyForCurrentLocale -  The current LocaleID is : " + currentLocaleID );
		}
	}
	
	var yotpoConfiguration : CustomObject = CustomObjectMgr.getCustomObject("yotpoConfiguration", currentLocaleID);
	
	if(yotpoConfiguration == null){
		Logger.getLogger("YotpoIntegration")
			.error( "YotpoUtils: getAppKeyForCurrentLocale -  The yotpo configuration does not exist for " + currentLocaleID + ", cannot proceed." );
		
		return "";
	}
	
	var appKey : String =  yotpoConfiguration.custom.appKey;
	
	if(empty(appKey)){
		Logger.getLogger("YotpoIntegration")
			.error( "YotpoUtils: getAppKeyForCurrentLocale -  The app key couldnt found for current locale." );
	}

   	return appKey;
}


/**
 * Retrieves the current locale from request, if not found then revert to 'default' locale
 *
 */
function getCurrentLocale( request : Request ) : String
{
	var isDebugEnabled : Boolean = dw.system.Site.getCurrent().preferences.custom.yotpoDebugLogEnabled;
	
	var currentLocaleID : String  = request.getLocale();
		
	if(empty(currentLocaleID)){
		currentLocaleID = request.getHttpLocale();	
	}
	
	if(empty(currentLocaleID)){
		currentLocaleID = "default"; //Default to default locale
	}
	
	if(isDebugEnabled){
		Logger.getLogger("YotpoIntegration")
			.debug( "YotpoUtils: getCurrentLocale - The current LocaleID is : " + currentLocaleID );
	}
	
   	return currentLocaleID;
}

/**
 * Retrieves if the reviews are enabled for current locale 
 *
 */
function isReviewsEnabledForCurrentLocale( currentLocaleID : String ) : Boolean
{
	var isDebugEnabled : Boolean = dw.system.Site.getCurrent().preferences.custom.yotpoDebugLogEnabled;
	
	if(empty(currentLocaleID)){
		Logger.getLogger("YotpoIntegration")
			.error( "YotpoUtils: isReviewsEnabledForCurrentLocale - The current LocaleID is missing, therefore cannot proceed." );
		
		return false;
	} else {
		if(isDebugEnabled){
			Logger.getLogger("YotpoIntegration")
				.debug( "YotpoUtils: isReviewsEnabledForCurrentLocale - The current LocaleID is : " + currentLocaleID );
		}
	}
	
	var yotpoConfiguration : CustomObject = CustomObjectMgr.getCustomObject("yotpoConfiguration", currentLocaleID);
	
	if(yotpoConfiguration == null){
		Logger.getLogger("YotpoIntegration")
			.error( "YotpoUtils: isReviewsEnabledForCurrentLocale - The yotpo configuration does not exist for " + currentLocaleID + ", cannot proceed." );
		
		return false;
	}
	
	var reviewsEnabled : Boolean =  yotpoConfiguration.custom.enableReviews;


   	return reviewsEnabled;
}

/**
 * Retrieves if the bottomline are enabled for current locale 
 *
 */
function isBottomLineEnabledForCurrentLocale( currentLocaleID : String ) : Boolean
{
	var isDebugEnabled : Boolean = dw.system.Site.getCurrent().preferences.custom.yotpoDebugLogEnabled;
	
	if(empty(currentLocaleID)){
		Logger.getLogger("YotpoIntegration")
			.error( "YotpoUtils: isBottomLineEnabledForCurrentLocale - The current LocaleID is missing, therefore cannot proceed." );
		
		return false;
	} 
	else {
		if(isDebugEnabled){
			Logger.getLogger("YotpoIntegration")
				.debug( "YotpoUtils: isBottomLineEnabledForCurrentLocale - The current LocaleID is : " + currentLocaleID );
		}
	}
	
	var yotpoConfiguration : CustomObject = CustomObjectMgr.getCustomObject("yotpoConfiguration", currentLocaleID);
	
	if(yotpoConfiguration == null){
		Logger.getLogger("YotpoIntegration")
			.error( "YotpoUtils: isBottomLineEnabledForCurrentLocale - The yotpo configuration does not exist for " + currentLocaleID + ", cannot proceed." );
		
		return false;
	}
	
	var bottomLineEnabled : Boolean =  yotpoConfiguration.custom.enableBottomLine;
	
   	return bottomLineEnabled;
}

function escape(text : String, regex : String, replacement : String) : String
{
	var regExp = new RegExp(regex, "gi");
	var escapedText : String = text.replace(regExp, replacement);
	return escapedText;
}

/**
*	This functions return the large image of a product
*/
function getImageLink(inProduct : Product) : String
{
	var imageURL : String = "";
	
	var image : dw.content.MediaFile = inProduct.getImage('large',0);
	if(!empty(image))	{
		imageURL = image.getAbsURL();
	}
	
	return imageURL;
}

/**
*	This functions return the primary category path of a product
*/
function getCategoryPath(product : Product) : String {
	
	var categoryPath : String = "";
	var topProduct : Product = product;
	
	if( topProduct.isVariant() ) {
		topProduct = product.getVariationModel().master;
	}
	
	var theCategory : Category = topProduct.getPrimaryCategory();
	
	if( empty(theCategory) ) {
		var categories : dw.util.Collection = topProduct.categories;
		
		if( !empty( categories ) ) {
			theCategory = categories[0];
		}
	}
	
	var cat : Category = theCategory;
	var path : dw.util.ArrayList = new dw.util.ArrayList();
	if(!empty(cat)){
		while( cat.parent != null)
		{
			if(cat.online){
			path.addAt( 0, cat );
			}
			cat = cat.parent;
		}
	}
	
	var index : Number;
	for(index=0; index<path.length ; index++){
		if(index==0) {
			categoryPath = categoryPath + path[index].getDisplayName();
		} else {
			categoryPath = categoryPath + " > " + path[index].getDisplayName();
		}
	}
	
	return categoryPath;
}
