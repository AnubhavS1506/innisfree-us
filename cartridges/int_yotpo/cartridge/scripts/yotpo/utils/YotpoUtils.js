'use strict';

/**
 * @module scripts/yotpo/utils/YotpoUtils
 *
 * This script provides utility functions shared across other Yotpo scripts.
 * Reused script components for Yotpo should be contained here, while this
 * script is imported into the requiring script.
 */
var StringUtils = require('dw/util/StringUtils');

/**
 * Validates the mandatory data in yotpoConfiguration, it returns false if that is missing.
 *
 * @param {Object} yotpoConfiguration - yotpo configuration object
 *
 * @returns {boolean} boolean
 */
function validateMandatoryConfigData(yotpoConfiguration) {
    var appKey = yotpoConfiguration.custom.appKey;
    var clientSecretKey = yotpoConfiguration.custom.clientSecretKey;

    if (empty(appKey) || empty(clientSecretKey)) {
        return false;
    }

    return true;
}

/**
 * Validates the mandatory data related to order feed job configuraiton, it returns false if that is missing.
 *
 * @param {Object} orderFeedJobLastExecutionDateTime - last job execution time
 *
 * @returns {boolean} boolean
 */
function validateOrderFeedJobConfiguration(orderFeedJobLastExecutionDateTime) {
    if (empty(orderFeedJobLastExecutionDateTime)) {
        return false;
    }
    return true;
}

/**
 * Retrieves the appKey based on the current locale
 *
 * @param {string} currentLocaleID - current locale id
 *
 * @returns {string} yotpo appKey
 */
function getAppKeyForCurrentLocale(currentLocaleID) {
    var CustomObjectMgr = require('dw/object/CustomObjectMgr');
    var Constants = require('../utils/Constants');
    var YotpoLogger = require('../utils/YotpoLogger');

    var logLocation = 'YotpoUtils~getAppKeyForCurrentLocale';

    if (empty(currentLocaleID)) {
        YotpoLogger.logMessage('The current LocaleID is missing, therefore cannot proceed.', 'error', logLocation);
        return '';
    }

    YotpoLogger.logMessage('The current LocaleID is : ' + currentLocaleID, 'debug', logLocation);

    var yotpoConfiguration = CustomObjectMgr.getCustomObject(Constants.YOTPO_CONFIGURATION_OBJECT, currentLocaleID);

    if (yotpoConfiguration == null) {
        YotpoLogger.logMessage('The yotpo configuration does not exist for ' + currentLocaleID + ', cannot proceed.', 'error', logLocation);
        return '';
    }

    var appKey = yotpoConfiguration.custom.appKey;

    if (empty(appKey)) {
        YotpoLogger.logMessage('The app key couldnt found for current locale.', 'error', logLocation);
    }

    return appKey;
}

/**
 * Retrieves the current locale from request, if not found then revert to 'default' locale.
 *
 * @param {Object} request - request object
 *
 * @returns {string} current locale id
 */
function getCurrentLocale(request) {
    var currentLocaleID = request.getLocale();

    if (empty(currentLocaleID)) {
        currentLocaleID = request.getHttpLocale();
    }

    if (empty(currentLocaleID)) {
        currentLocaleID = 'default'; // Default to default locale
    }

    return currentLocaleID;
}

/**
 * Retrieves the current locale from request for MFRA sites, if not found then revert to 'default' locale.
 *
 * @param {string} currentLocaleID - current locale id
 *
 * @returns {string} localeID
 */
function getCurrentLocaleMFRA(currentLocaleID) {
    var localeID = currentLocaleID;

    if (empty(localeID) || localeID === 'undefined') {
        localeID = 'default'; // Default to default locale
    }

    return localeID;
}

/**
 * Retrieves if the reviews are enabled for current locale.
 *
 * @param {string} currentLocaleID - current locale id
 *
 * @returns {boolean} reviewsEnabled
 */
function isReviewsEnabledForCurrentLocale(currentLocaleID) {
    var CustomObjectMgr = require('dw/object/CustomObjectMgr');
    var Constants = require('../utils/Constants');
    var YotpoLogger = require('../utils/YotpoLogger');

    var logLocation = 'YotpoUtils~isReviewsEnabledForCurrentLocale';

    if (empty(currentLocaleID)) {
        YotpoLogger.logMessage('The current LocaleID is missing, therefore cannot proceed.', 'error', logLocation);
        return false;
    }

    YotpoLogger.logMessage('The current LocaleID is : ' + currentLocaleID, 'debug', logLocation);

    var yotpoConfiguration = CustomObjectMgr.getCustomObject(Constants.YOTPO_CONFIGURATION_OBJECT, currentLocaleID);

    if (yotpoConfiguration == null) {
        YotpoLogger.logMessage('The yotpo configuration does not exist for ' + currentLocaleID + ', cannot proceed.', 'error', logLocation);
        return false;
    }

    var reviewsEnabled = yotpoConfiguration.custom.enableReviews;
    return reviewsEnabled;
}

/**
 * Retrieves if the bottomline are enabled for current locale.
 *
 * @param {string} currentLocaleID - current locale id
 *
 * @returns {boolean} bottomLineEnabled
 */
function isBottomLineEnabledForCurrentLocale(currentLocaleID) {
    var CustomObjectMgr = require('dw/object/CustomObjectMgr');
    var Constants = require('../utils/Constants');
    var YotpoLogger = require('../utils/YotpoLogger');

    var logLocation = 'YotpoUtils~isBottomLineEnabledForCurrentLocale';

    if (empty(currentLocaleID)) {
        YotpoLogger.logMessage('The current LocaleID is missing, therefore cannot proceed.', 'error', logLocation);
        return false;
    }

    YotpoLogger.logMessage('The current LocaleID is : ' + currentLocaleID, 'debug', logLocation);

    var yotpoConfiguration = CustomObjectMgr.getCustomObject(Constants.YOTPO_CONFIGURATION_OBJECT, 'en');

    if (yotpoConfiguration == null) {
        YotpoLogger.logMessage('The yotpo configuration does not exist for ' + currentLocaleID, 'error', logLocation);
        return false;
    }

    var bottomLineEnabled = yotpoConfiguration.custom.enableBottomLine;
    return bottomLineEnabled;
}


/**
 * This function escapes specific characters from the text based on the regular expression.
 *
 * @param {string} text - string to be escaped
 * @param {string} regex - regular expression
 * @param {string} replacement - replacement character
 *
 * @returns {string} escapedText
 */
function escape(text, regex, replacement) {
    var regExp = new RegExp(regex, 'gi');
    var escapedText = text.replace(regExp, replacement);
    return StringUtils.trim(escapedText);
}

/**
*	This functions return the large image of a product.
*
* @param {Object} inProduct - product object to get image from.
*
* @returns {string} imageURL
*/
function getImageLink(inProduct) {
    var imageURL = '';
    var image = inProduct.getImage('large', 0);

    if (!empty(image)) {
        imageURL = image.getAbsURL();
    }

    return imageURL;
}

/**
*	This functions return the primary category path of a product.
*
* @param {Object} product - product object to get category path from.
*
* @returns {string} categoryPath
*/
function getCategoryPath(product) {
    var ArrayList = require('dw/util/ArrayList');

    var categoryPath = '';
    var topProduct = product;

    if (topProduct.isVariant()) {
        topProduct = product.getVariationModel().master;
    }

    var theCategory = topProduct.getPrimaryCategory();

    if (empty(theCategory)) {
        var categories = topProduct.categories;
        if (!empty(categories)) {
            theCategory = categories[0];
        }
    }

    var cat = theCategory;
    var path = new ArrayList();

    while (cat.parent != null) {
        if (cat.online) {
            path.addAt(0, cat);
        }
        cat = cat.parent;
    }

    var index;
    for (index = 0; index < path.length; index++) {
        if (index === 0) {
            categoryPath += path[index].getDisplayName();
        } else {
            categoryPath += ' > ' + path[index].getDisplayName();
        }
    }
    return categoryPath;
}

/**
 * This is a common function to check whether the Yotpo cartridge is disabled or not.
 * @returns {boolean} boolean
 */
function isCartridgeEnabled() {
    var Site = require('dw/system/Site');
    var YotpoLogger = require('../utils/YotpoLogger');

    var logLocation = 'YotpoUtils~isCartridgeEnabled';
    var yotpoCartridgeEnabled = Site.getCurrent().preferences.custom.yotpoCartridgeEnabled;

    if (!yotpoCartridgeEnabled) {
        YotpoLogger.logMessage('The Yotpo cartridge is disabled, please check custom preference (yotpoCartridgeEnabled).', 'info', logLocation);
    }

    return yotpoCartridgeEnabled;
}

/* Module Exports */
exports.validateMandatoryConfigData = validateMandatoryConfigData;
exports.validateOrderFeedJobConfiguration = validateOrderFeedJobConfiguration;
exports.getAppKeyForCurrentLocale = getAppKeyForCurrentLocale;
exports.getCurrentLocale = getCurrentLocale;
exports.getCurrentLocaleMFRA = getCurrentLocaleMFRA;
exports.isReviewsEnabledForCurrentLocale = isReviewsEnabledForCurrentLocale;
exports.isBottomLineEnabledForCurrentLocale = isBottomLineEnabledForCurrentLocale;
exports.isCartridgeEnabled = isCartridgeEnabled;
exports.getCategoryPath = getCategoryPath;
exports.getImageLink = getImageLink;
exports.escape = escape;
