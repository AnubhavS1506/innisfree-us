'use strict';

var dialog = require('./dialog');
var util = require('./util');
var $cache = {};

function InitializeDom() {
    $cache.body = $('.bite-size-bar-body');
    $cache.header = $('div.bite-size-bar-header');
    $cache.quickview = $('.sampleview');
}

var addSampleToCart = function (e) {
    e.preventDefault();
    var $this = $(this);
    if ($this.attr('disabled')) {
        return false;
    }
    $.ajax({
        type: 'POST',
        url: util.ajaxUrl(Urls.addSample),
        data: {
            'pid': $this.data('product-id') || '',
            'sample': true
        },
        success: function (response) {
            //hide add button, show remove button
            $this.addClass('removesamplebutton').off('click').on('click', removeSampleFromCart);
            $this.html('<i class="fa fa-check"></i>');
            $this.removeClass('addsamplebutton');
            //show the check icon
            $this.parent().siblings('span.check').addClass('on');
            checkSampleButton(response.limit);
        }
    });
};

function removeSampleFromCart(e) {
    e.preventDefault();
    var $this = $(this);
    $.ajax({
        type: 'POST',
        url: util.ajaxUrl(Urls.removeSample),
        data: {
            'pid': $this.data('product-id') || '',
            'sample': true
        },
        success: function (response) {
            //hide remove button, show add button
            $this.addClass('addsamplebutton').off('click').on('click', addSampleToCart);
            $this.html(Resources.SAMPLE_ADD);
            $this.removeClass('removesamplebutton');
            //remove check icon
            $this.parent().siblings('span.check').removeClass('on');
            checkSampleButton(response.limit);
        }
    });
}

function checkSampleButton(limit) {
    //if samples exceed the limitation, disable add-sample buttons
    if ($('.removesamplebutton').length === limit) {
        $('.addsamplebutton').each(function() {
            $(this).removeClass('dark-line');
            $(this).addClass('light-gray');
            $(this).attr('disabled', true);
        });
    }
    else {
        //else, enable add-sample buttons
        $('.addsamplebutton').each(function() {
            $(this).removeClass('light-gray');
            $(this).addClass('dark-line');
            $(this).attr('disabled', false);
        });
    }
}

function InitializeEvents() {
    $('.addsamplebutton').on('click', addSampleToCart);
    $('.removesamplebutton').on('click', removeSampleFromCart);
    checkSampleButton($cache.body.data('limit'));
    //toggle bite size bar content
    $cache.header.on('click', function (e) {
         e.preventDefault();
         $(this).siblings('.choose-con').children('.bite-size-bar-content').slideToggle(1000);
    });
    $cache.quickview.on('click', function () {
        dialog.open({
            url: $(this).data('url'),
            options: {
                width: 720,
                dialogClass: 'sample'
            }
        });
    });
}

/**
 * @function
 * @description Binds the click event to a given target for the add-to-cart handling
 */
module.exports = function () {
    InitializeDom();
    InitializeEvents();
};
