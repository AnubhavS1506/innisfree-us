'use strict';

/**
 * Controller that renders sample section
 *
 * @module controllers/Sample
 */

/* API */
var Site = require('dw/system/Site');
var Transaction = require('dw/system/Transaction');
/* Script Modules */
var app = require('app_innisfree_controllers/cartridge/scripts/app');
var guard = require('app_innisfree_controllers/cartridge/scripts/guard');
var SampleHelper = require('~/cartridge/scripts/SampleHelper');
/*Global Variables*/
var params = request.httpParameterMap;
/**
 * Renders detail popup
 */
function show() {
    var Product = app.getModel('Product');
    var product = Product.get(params.pid.stringValue);

    if (product.isVisible()) {
        app.getView('Product', {
            product: product
        }).render('quickview/producttopcontent');
    }
}
/**
 * Renders all the sample products available
 */
function showSamples() {
    if ('enableSampleSection' in Site.getCurrent().getPreferences().getCustom() &&
            Site.getCurrent().getCustomPreferenceValue('enableSampleSection')) {
        if (SampleHelper.checkValidCartWithSamples()) {
            // Constructs the search
            var Products = SampleHelper.getSampleProducts('bitesize-barsample');
            var basket = app.getModel('Cart').goc();
            var Limit = SampleHelper.getLimitSample();
            app.getView({
                Products: Products,
                Basket: basket,
                Limit: Limit
            }).render('cart/samples');
        }
        else {
            app.getView().render('cart/sampleserror');
        }
    }
}
/**
 * add a sample to the cart
 */
function addProduct() {
    var cart = app.getModel('Cart').goc();
    var response = require('app_innisfree_controllers/cartridge/scripts/util/Response');
    var cSample = getNumberSampleFromCart(cart);
    var Limit = SampleHelper.getLimitSample();

    var renderInfo = null;
    if ((cSample + 1) <= Limit) {
        renderInfo = cart.addProductToCart();
    } else {
        response.renderJSON({invalid: 'invalid'});
        return;
    }

    if (renderInfo.format === 'ajax' && params.sample.value) {
        response.renderJSON({limit: Limit});
    }
}

function getNumberSampleFromCart(basket) {
    var plis = basket.getAllProductLineItems();
    var cSample = 0;
    for (var i = 0; i < plis.length; i++) {
        if(plis[i].custom.sample) {
            cSample += 1;
        }
    }
    return cSample;
}

/**
 * remove a sample from the cart
 */
function removeProduct() {
    var cart = app.getModel('Cart').goc();
    var productID = params.pid.value;
    if (productID) {
        var items = cart.getAllProductLineItems(productID);
        Transaction.wrap(function () {
            for (var i = 0; i < items.length; i++) {
                var pli = items[i];
                cart.removeProductLineItem(pli);
            }
        });
    }
    if (params.sample.value) {
        var Limit = SampleHelper.getLimitSample();
        var r = require('app_innisfree_controllers/cartridge/scripts/util/Response');
        r.renderJSON({limit: Limit});
    }
}

/*
 * Web exposed methods
 */
exports.Show = guard.ensure(['get'], show);
exports.ShowSamples = guard.ensure(['get'], showSamples);
exports.RemoveProduct = guard.ensure(['post'], removeProduct);
exports.AddProduct = guard.ensure(['post'], addProduct);