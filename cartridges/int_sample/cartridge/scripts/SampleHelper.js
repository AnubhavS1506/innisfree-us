'use strict';

/**
 * supports additional functions
 */

/* API */
var Site = require('dw/system/Site');
/* Script Modules */
var app = require('app_innisfree_controllers/cartridge/scripts/app');
var ArrayList = require('dw/util/ArrayList');
/**
 * based on JSON configuration on BM,
 * determines the limit sample
 * @returns {Number}
 */
function getLimitSample() {
    var basket = app.getModel('Cart').get().object;
    var samplesDynamicConfig = Site.getCurrent().getCustomPreferenceValue('samplesDynamicConfig');
    var Limit = 0;
    if (samplesDynamicConfig) {
        samplesDynamicConfig = JSON.parse(samplesDynamicConfig);
        for (var price in samplesDynamicConfig) {
            if (basket.adjustedMerchandizeTotalNetPrice.value >= Number(price)) {
                Limit = samplesDynamicConfig[price];
            }
        }
    }
    return Limit;
}
/**
 * check the cart
 * invalid if there's no product in the cart
 * @returns {Boolean}
 */
function checkValidCartWithSamples() { 
    if ('enableSampleSection' in Site.getCurrent().getPreferences().getCustom() && Site.getCurrent().getCustomPreferenceValue('enableSampleSection')) {
        var basket = app.getModel('Cart').goc();
        var plis = basket.getAllProductLineItems();
        var cSample = 0;
        for (var i = 0; i < plis.length; i++) {
            if(plis[i].custom.sample) {
                cSample += 1;
            }
        }
        var Limit = getLimitSample();
        //invalid if the cart only contains sample products
        //invalid if sample products > limitation
        return cSample === plis.length || cSample > Limit ? false : true;
    } else {
        return true;
    }
}

/**
 * get all samples belong to specific category
 * @returns {dw.util.Collection} samples
 */
function getSampleProducts(categoryID) {
    var category = require('dw/catalog/CatalogMgr').getCategory(categoryID);
    var sampleProducts = new ArrayList();
    if (category) {
        if ('showOutOfStockSamples' in Site.getCurrent().getPreferences().getCustom() && Site.getCurrent().getCustomPreferenceValue('showOutOfStockSamples')) {
            return category.onlineProducts;
        } else {
            var onlineProds = category.onlineProducts;
            for (var i = 0; i < onlineProds.length; i++) {
                var prod = onlineProds[i];
                if (prod.availabilityModel.inStock) {
                    sampleProducts.add(prod);
                }
            }
            return sampleProducts;
        }
    }
}

/**
 * checks whether customer can checkout or not
 * @returns {Boolean}
 */
function sampleCheckoutValid(basket) {
    var sampleLimit = 0;
    var sampleCount = 0;
    sampleLimit = getLimitSample();
    var disableCheckout = false;
    for (var pli in basket.getProductLineItems()) {
        if (!empty(pli.custom) && !empty(pli.custom.sample)) {
            sampleCount++;
        }
    }

    if (sampleCount > sampleLimit) {
        disableCheckout = true;
    }
    return disableCheckout;
}

function removeSampleProducts() {
    if ('enableSampleSection' in Site.getCurrent().getPreferences().getCustom() && Site.getCurrent().getCustomPreferenceValue('enableSampleSection')) {
        var basket = app.getModel('Cart').get();
        var plis = basket.getAllProductLineItems();
        for (var i = 0; i < plis.length; i++) {
            if (plis[i].custom.sample) {
                basket.removeProductLineItem(plis[i]);
            }
        }
    }
}

exports.getLimitSample = getLimitSample;
exports.checkValidCartWithSamples = checkValidCartWithSamples;
exports.getSampleProducts = getSampleProducts;
exports.sampleCheckoutValid = sampleCheckoutValid;
exports.removeSampleProducts = removeSampleProducts;