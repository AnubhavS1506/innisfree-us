'use strict';

/**
 * Controller that renders the account overview, manages customer registration and password reset,
 * and edits customer profile information.
 *
 * @module controllers/Account
 */

/* API includes */
var Resource = require('dw/web/Resource');
var URLUtils = require('dw/web/URLUtils');
var Form = require('~/cartridge/scripts/models/FormModel');
var OrderMgr = require('dw/order/OrderMgr');
var PagingModel = require('dw/web/PagingModel');

/* Script Modules */
var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');
var Transaction = require('dw/system/Transaction');
var DateUtils = require('~/cartridge/scripts/util/Date');
var CustomerMgr = require('dw/customer/CustomerMgr');
var Calendar = require('dw/util/Calendar');
var StringUtils = require('dw/util/StringUtils');
var Pipeline = require('dw/system/Pipeline');

/**
 * Gets a ContentModel object that wraps the myaccount-home content asset,
 * updates the page metadata, and renders the account/accountoverview template.
 */
function show() {
    var accountHomeAsset, pageMeta, Content, serviceResult, message;
    var profileEdited = request.httpParameterMap.profileEdited.value;
    var CurrentCustomer = customer.getProfile();

    serviceResult = app.getController('POSAccount').RequestCustomerInfo();
    if (!empty(serviceResult) && serviceResult.status == 'OK') {
        var responseCode = serviceResult.object.RESULT.CODE;
        if (responseCode != '000') {
            message = serviceResult.object.RESULT.MESSAGE;
        }
    }
    Content = app.getModel('Content');
    accountHomeAsset = Content.get('myaccount-home');

    pageMeta = require('~/cartridge/scripts/meta');
    pageMeta.update(accountHomeAsset);

    var ProductList = app.getModel('ProductList');
    var productList = ProductList.get();
    var OrderHistoryModel = app.getModel('OrderHistory');
    var OrderHistory = OrderHistoryModel.getHistory(CurrentCustomer.customerNo);

    if (OrderHistory.length > 0) {
        OrderHistory = OrderHistory.slice(0, 5);
        for (var i = 0; i < OrderHistory.length; i++) {
            OrderHistory[i].orderDate = DateUtils.getOrderDateFromOMS(OrderHistory[i].orderDate);
        }
    }

    app.getView({
        ProductList: productList.object,
        OrderHistory: OrderHistory,
        profileEdited: profileEdited,
        posErrorMessage: message
    }).render('account/accountoverview');
}

/**
 * Clears the profile form and copies customer profile information from the customer global variable
 * to the form. Gets a ContentModel object that wraps the myaccount-personaldata content asset, and updates the page
 * meta data. Renders the account/user/registration template using an anonymous view.
 */
function editProfile() {
    var pageMeta;
    var accountPersonalDataAsset;
    var Content = app.getModel('Content');

    if (!request.httpParameterMap.invalid.submitted) {
        app.getForm('profile').clear();

        app.getForm('profile.customer').copyFrom(customer.profile);
        app.getForm('profile.customer.customerBirthday.year').object.value = customer.profile.custom.birthdayYear;
        app.getForm('profile.customer.customerBirthday.month').object.value = customer.profile.custom.birthdayMonth;
        app.getForm('profile.customer.customerBirthday.day').object.value = customer.profile.custom.birthdayDay;
        app.getForm('profile.customer.isOptInNewsleter').object.value = customer.profile.custom.isOptInNewsleter;
        app.getForm('profile.login').copyFrom(customer.profile.credentials);
        app.getForm('profile.addressbook.addresses').copyFrom(customer.profile.addressBook.addresses);
    }
    accountPersonalDataAsset = Content.get('myaccount-personaldata');

    pageMeta = require('~/cartridge/scripts/meta');
    pageMeta.update(accountPersonalDataAsset);
    // @FIXME bctext2 should generate out of pagemeta - also action?!
    app.getView({
        selected: 'accountEditProfile',
        bctext3: Resource.msg('account.user.registration.accountinformation', 'account', null),
        Action: 'edit',
        ContinueURL: URLUtils.https('Account-EditForm')
    }).render('account/user/editprofile');
}

function confirmPasswordDialog() {
    app.getView().render('account/password/currentpassworddialog');
}

function confirmBirthdayDialog() {
    app.getView().render('account/password/confirmbirthdaydialog');
}

function checkCurrentPassword() {
    var customerCredentials = customer.profile.getCredentials();
    var r = require('~/cartridge/scripts/util/Response');
    var message = '';
    var success = true;
    var password = empty(app.getForm('profile.login.currentpassword').value()) ? '' : app.getForm('profile.login.currentpassword').value();
    var customerProfiles = customer.getExternalProfiles();
    var authenticationProviderID;
    if (!empty(customerProfiles)) {
        authenticationProviderID = customerProfiles[0].getAuthenticationProviderID();
    }
    if (empty(authenticationProviderID)) {
        var authenticatedCustomer = Transaction.wrap(function() {
            return CustomerMgr.loginCustomer(customerCredentials.getLogin(), password, false);
        });
        if (!authenticatedCustomer) {
            success = false;
            message = Resource.msg('account.validation.invalidCurrentPassword', 'account', null)
        }
    }
    r.renderJSON({
        success: success,
        message: message
    });
    return r;
}

/**
 * Handles the form submission on profile update of edit profile. Handles cancel and confirm actions.
 *  - cancel - clears the profile form and redirects to the Account-Show controller function.
 *  - confirm - gets a CustomerModel object that wraps the current customer. Validates several form fields.
 * If any of the profile validation conditions fail, the user is redirected to the Account-EditProfile controller function. If the profile is valid, the user is redirected to the Account-Show controller function.
 */
function editForm() {
    app.getForm('profile').handleAction({
        cancel: function() {
            app.getForm('profile').clear();
            response.redirect(URLUtils.https('Account-Show'));
        },
        confirm: function() {
            var isProfileUpdateValid = true;
            var hasEditSucceeded = false;
            var Customer = app.getModel('Customer');

            if (!Customer.checkUserName()) {
                app.getForm('profile.customer.email').invalidate();
                isProfileUpdateValid = false;
            }

            if (app.getForm('profile.customer.email').value() !== app.getForm('profile.customer.emailconfirm').value()) {
                app.getForm('profile.customer.emailconfirm').invalidate();
                isProfileUpdateValid = false;
            }

            if (app.getForm('profile.login.newpassword').value() !== app.getForm('profile.login.newpasswordconfirm').value()) {
                app.getForm('profile.login.newpasswordconfirm').invalidate();
                isProfileUpdateValid = false;
            }

            // Check birthday fields
            var day = app.getForm('profile.customer.customerBirthday.day').value();
            var month = app.getForm('profile.customer.customerBirthday.month').value();
            var year = app.getForm('profile.customer.customerBirthday.year').value();
            if (!empty(day) && !empty(month) && !empty(year)) {
                var inDateString, dwDate, calendar, isValidDate = false;
                inDateString = year + '-' + month + '-' + day;
                dwDate = new Date(year, month - 1, day, '00', '00', '00');
                if ((dwDate instanceof Date)) {
                    calendar = new Calendar(dwDate);
                    isValidDate = StringUtils.formatCalendar(calendar, 'yyyy-MM-dd') == inDateString;
                }
                if (!isValidDate) {
                    isProfileUpdateValid = false;
                }
            }

            if (isProfileUpdateValid) {
                hasEditSucceeded = Customer.editAccount(app.getForm('profile.customer.email').value(), app.getForm('profile.login.newpassword').value(), app.getForm('profile.login.currentpassword').value(), app.getForm('profile'));
                if (!hasEditSucceeded) {
                    app.getForm('profile.login.password').invalidate();
                    isProfileUpdateValid = false;
                }

                var email = app.getForm('profile.customer.email').value();
                var isOptInNewsleter = app.getForm('profile.customer.isOptInNewsleter').value();
                if (!empty(isOptInNewsleter) && isOptInNewsleter) {
                    Pipeline.execute('BrontoOptIn-EditAccount', {
                        email: email
                    });
                }
            }
            if (isProfileUpdateValid && hasEditSucceeded) {
                app.getController('POSAccount').UpdateCustomerInfo();
                response.redirect(URLUtils.https('Account-Show', 'profileEdited', true));
            } else {
                response.redirect(URLUtils.https('Account-EditProfile', 'invalid', 'true'));
            }
        },
        changepassword: function() {
            var isProfileUpdateValid = true;
            var hasEditSucceeded = false;
            var Customer = app.getModel('Customer');

            if (!Customer.checkUserName()) {
                app.getForm('profile.customer.email').invalidate();
                isProfileUpdateValid = false;
            }

            if (app.getForm('profile.login.newpassword').value() !== app.getForm('profile.login.newpasswordconfirm').value()) {
                app.getForm('profile.login.newpasswordconfirm').invalidate();
                isProfileUpdateValid = false;
            }

            if (isProfileUpdateValid) {
                hasEditSucceeded = Customer.editAccount(app.getForm('profile.customer.email').value(), app.getForm('profile.login.newpassword').value(), app.getForm('profile.login.currentpassword').value(), app.getForm('profile'));
                if (!hasEditSucceeded) {
                    app.getForm('profile.login.currentpassword').invalidate();
                }
            }

            if (isProfileUpdateValid && hasEditSucceeded) {
                response.redirect(URLUtils.https('Account-Show'));
            } else {
                response.redirect(URLUtils.https('Account-EditProfile', 'invalid', 'true'));
            }
        },
        error: function() {
            response.redirect(URLUtils.https('Account-EditProfile', 'invalid', 'true'));
        }
    });
}

/**
 * Gets the requestpassword form and renders the requestpasswordreset template. This is similar to the password reset
 * dialog, but has a screen-based interaction instead of a popup interaction.
 */
function passwordReset() {
    app.getForm('requestpassword').clear();
    app.getView({
        ContinueURL: URLUtils.https('Account-PasswordResetForm')
    }).render('account/password/requestpasswordreset');
}

/**
 * Handles form submission from dialog and full page password reset. Handles cancel, send, and error actions.
 *  - cancel - renders the given template.
 *  - send - gets a CustomerModel object that wraps the current customer. Gets an EmailModel object that wraps an Email object.
 * Checks whether the customer requested the their login password be reset.
 * If the customer wants to reset, a password reset token is generated and an email is sent to the customer using the mail/resetpasswordemail template.
 * Then the account/password/requestpasswordreset_confirm template is rendered.
 *  - error - the given template is rendered and passed an error code.
 */
function passwordResetFormHandler(templateName, continueURL) {
    var resetPasswordToken, passwordemail;

    app.getForm('profile').handleAction({
        cancel: function() {
            app.getView({
                ContinueURL: continueURL
            }).render(templateName);
        },
        send: function() {
            var Customer, resettingCustomer, Email;
            Customer = app.getModel('Customer');
            Email = app.getModel('Email');
            var requestForm = Form.get('requestpassword').object.email.htmlValue;
            resettingCustomer = Customer.retrieveCustomerByLogin(requestForm);

            if (!empty(resettingCustomer)) {
                resetPasswordToken = resettingCustomer.generatePasswordResetToken();

                passwordemail = Email.get('mail/resetpasswordemail', resettingCustomer.object.profile.email);
                passwordemail.setSubject(Resource.msg('resource.passwordassistance', 'email', null));
                passwordemail.send({
                    ResetPasswordToken: resetPasswordToken,
                    Customer: resettingCustomer.object.profile.customer
                });
            }

            //for security reasons the same message will be shown for a valid reset password request as for a invalid one
            app.getView({
                ErrorCode: null,
                ShowContinue: true,
                ContinueURL: continueURL
            }).render('account/password/requestpasswordreset_confirm');
        },
        error: function() {
            app.getView({
                ErrorCode: 'formnotvalid',
                ContinueURL: continueURL
            }).render(templateName);
        }
    });
}

/**
 * The form handler for password resets.
 */
function passwordResetForm() {
    passwordResetFormHandler('account/password/requestpasswordreset', URLUtils.https('Account-PasswordResetForm'));
}

/**
 * Clears the requestpassword form and renders the account/password/requestpasswordresetdialog template.
 */
function passwordResetDialog() {
    // @FIXME reimplement using dialogify
    app.getForm('requestpassword').clear();
    app.getView({
        ContinueURL: URLUtils.url('Account-PasswordResetDialogForm')
    }).render('account/password/requestpasswordresetdialog');
}

/**
 * Handles the password reset form.
 */
function passwordResetDialogForm() {
    // @FIXME reimplement using dialogify
    passwordResetFormHandler('account/password/requestpasswordresetdialog', URLUtils.https('Account-PasswordResetDialogForm'));
}

/**
 * Gets a CustomerModel wrapping the current customer. Clears the resetpassword form. Checks if the customer wants to reset their password.
 * If there is no reset token, redirects to the Account-PasswordReset controller function. If there is a reset token,
 * renders the screen for setting a new password.
 */
function setNewPassword() {
    var Customer, resettingCustomer;
    Customer = app.getModel('Customer');

    app.getForm('resetpassword').clear();
    resettingCustomer = Customer.getByPasswordResetToken(request.httpParameterMap.Token.getStringValue());

    if (empty(resettingCustomer)) {
        response.redirect(URLUtils.https('Account-PasswordReset'));
    } else {
        app.getView({
            ContinueURL: URLUtils.https('Account-SetNewPasswordForm')
        }).render('account/password/setnewpassword');
    }
}

/**
 * Gets a profile form and handles the cancel and send actions.
 *  - cancel - renders the setnewpassword template.
 *  - send - gets a CustomerModel object that wraps the current customer and gets an EmailModel object that wraps an Email object.
 * Checks whether the customer can be retrieved using a reset password token.
 * If the customer does not have a valid token, the controller redirects to the Account-PasswordReset controller function.
 * If they do, then an email is sent to the customer using the mail/setpasswordemail template and the setnewpassword_confirm template is rendered.
 * */
function setNewPasswordForm() {

    app.getForm('profile').handleAction({
        cancel: function() {
            app.getView({
                ContinueURL: URLUtils.https('Account-SetNewPasswordForm')
            }).render('account/password/setnewpassword');
            return;
        },
        send: function() {
            var Customer;
            var Email;
            var passwordChangedMail;
            var resettingCustomer;
            var success;

            Customer = app.getModel('Customer');
            Email = app.getModel('Email');
            resettingCustomer = Customer.getByPasswordResetToken(request.httpParameterMap.Token.getStringValue());

            if (!resettingCustomer) {
                response.redirect(URLUtils.https('Account-PasswordReset'));
            }

            if (app.getForm('resetpassword.password').value() !== app.getForm('resetpassword.passwordconfirm').value()) {
                app.getForm('resetpassword.passwordconfirm').invalidate();
                app.getView({
                    ContinueURL: URLUtils.https('Account-SetNewPasswordForm')
                }).render('account/password/setnewpassword');
            } else {

                success = resettingCustomer.resetPasswordByToken(request.httpParameterMap.Token.getStringValue(), app.getForm('resetpassword.password').value());
                if (!success) {
                    app.getView({
                        ErrorCode: 'formnotvalid',
                        ContinueURL: URLUtils.https('Account-SetNewPasswordForm')
                    }).render('account/password/setnewpassword');
                } else {
                    passwordChangedMail = Email.get('mail/passwordchangedemail', resettingCustomer.object.profile.email);
                    passwordChangedMail.setSubject(Resource.msg('resource.passwordassistance', 'email', null));
                    passwordChangedMail.send({
                        Customer: resettingCustomer.object
                    });

                    app.getView().render('account/password/setnewpassword_confirm');
                }
            }
        }
    });
}

/** Clears the profile form, adds the email address from login as the profile email address,
 *  and renders customer registration page.
 */
function startRegister() {

    app.getForm('profile').clear();

    if (app.getForm('login.username').value() !== null) {
        app.getForm('profile.customer.email').object.value = app.getForm('login.username').object.value;
    }

    app.getView({
        ContinueURL: URLUtils.https('Account-RegistrationForm')
    }).render('account/user/registration');
}

/**
 * Gets a CustomerModel object wrapping the current customer.
 * Gets a profile form and handles the confirm action.
 *  confirm - validates the profile by checking  that the email and password fields:
 *  - match the emailconfirm and passwordconfirm fields
 *  - are not duplicates of existing username and password fields for the profile
 * If the fields are not valid, the registration template is rendered.
 * If the fields are valid, a new customer account is created, the profile form is cleared and
 * the customer is redirected to the Account-Show controller function.
 */
function registrationForm() {
    app.getForm('profile').handleAction({
        confirm: function() {
            var firstName, email, orderNo, profileValidation, password, passwordConfirmation, existingCustomer, Customer, target, isRewardProgram, isTermsConditions, isOptInNewsleter;

            Customer = app.getModel('Customer');
            email = app.getForm('profile.customer.email').value();
            orderNo = app.getForm('profile.customer.orderNo').value();
            firstName = app.getForm('profile.customer.firstname').value();
            profileValidation = true;

            password = app.getForm('profile.login.password').value();
            passwordConfirmation = app.getForm('profile.login.passwordconfirm').value();
            if (password !== passwordConfirmation) {
                app.getForm('profile.login.passwordconfirm').invalidate();
                profileValidation = false;
            }

            isRewardProgram = app.getForm('profile.customer.isAcceptRewardProgram').value();
            isTermsConditions = app.getForm('profile.customer.isAcceptTermsAndConditions').value();
            if (!isRewardProgram) {
                app.getForm('profile.customer.isAcceptRewardProgram').invalidate();
                profileValidation = false;
            }
            if (!isTermsConditions) {
                app.getForm('profile.customer.isAcceptTermsAndConditions').invalidate();
                profileValidation = false;
            }

            // Check birthday fields
            var day = app.getForm('profile.customer.customerBirthday.day').value();
            var month = app.getForm('profile.customer.customerBirthday.month').value();
            var year = app.getForm('profile.customer.customerBirthday.year').value();
            if (!empty(day) && !empty(month) && !empty(year)) {
                var inDateString, dwDate, calendar, isValidDate = false;
                inDateString = year + '-' + month + '-' + day;
                dwDate = new Date(year, month - 1, day, '00', '00', '00');
                if ((dwDate instanceof Date)) {
                    calendar = new Calendar(dwDate);
                    isValidDate = StringUtils.formatCalendar(calendar, 'yyyy-MM-dd') == inDateString;
                }
                if (!isValidDate) {
                    app.getForm('profile.customer.customerBirthday.day').invalidate();
                    profileValidation = false;
                }
            }

            // Checks if login is already taken.
            existingCustomer = Customer.retrieveCustomerByLogin(email);
            if (existingCustomer !== null) {
                app.getForm('profile.customer.email').invalidate();
                profileValidation = false;
            }

            if (profileValidation) {
                profileValidation = Customer.createAccount(email, password, app.getForm('profile'));
                Customer.createAccountSetInfo();

                isOptInNewsleter = app.getForm('profile.customer.isOptInNewsleter').value();
                if (!empty(isOptInNewsleter) && isOptInNewsleter) {
                    Pipeline.execute('BrontoOptIn-CreateAccount', {
                        email: email
                    });
                }

                if (orderNo) {
                    var orders = OrderMgr.searchOrders('orderNo={0} AND status!={1}', 'creationDate desc', orderNo,
                        dw.order.Order.ORDER_STATUS_REPLACED);
                    if (orders) {
                        var foundOrder = orders.next();
                        Transaction.wrap(function() {
                            foundOrder.customer = profileValidation;
                        });
                        session.custom.TargetLocation = URLUtils.https('Account-Show', 'Registration', 'true');
                    }
                }
                app.getController('AbandonedCart').UpdateAbandonCartProducts();
                //Send welcome email to new customer
                var Email = app.getModel('Email');
                var welcomeEmail = Email.get('mail/register', email);
                welcomeEmail.setSubject(Resource.msg('account.email.welcome.subject', 'account', null));
                welcomeEmail.send({
                    FirstName: firstName
                });
            }
            if (request.httpParameterMap.scope.stringValue === 'dialog') {
                if (!profileValidation) {
                    app.getView('Login').render();
                } else {
                    app.getController('POSAccount').UpdateCustomerInfo();
                    app.getForm('profile').clear();
                    app.getView().render('/account/login/accountlogindialogsuccess');
                }
                return;
            }
            if (!profileValidation) {
                // TODO redirect
                app.getView({
                    ContinueURL: URLUtils.https('Account-RegistrationForm')
                }).render('account/user/registration');
            } else {
                app.getController('POSAccount').UpdateCustomerInfo();
                app.getForm('profile').clear();
                target = session.custom.TargetLocation;
                if (target) {
                    delete session.custom.TargetLocation;
                    //@TODO make sure only path, no hosts are allowed as redirect target
                    dw.system.Logger.info('Redirecting to "{0}" after successful login', target);
                    response.redirect(target);
                } else {
                    response.redirect(URLUtils.https('Account-Show'));
                }
            }
        },
        error: function() {
            app.getView('Login', {RegisterDialogError: true}).render();
            return;
        }
    });
}

function showPopupBarcode() {
    app.getView().render('account/dialog/popupbarcode');
}

function rewards() {
    app.getView({
        bctext2: 'Rewards',
        Action: 'Rewards',
        selected: 'accountRewards'
    }).render('account/rewards/rewards');
}

/**
 * Renders the accountnavigation template.
 */
function includeNavigation() {
    app.getView().render('account/accountnavigation');
}


function checkBirthday() {
    var r = require('~/cartridge/scripts/util/Response');
    var inDateString, dwDate, calendar, isValidDate = false;
    var day = request.httpParameterMap.day;
    var month = request.httpParameterMap.month;
    var year = request.httpParameterMap.year;

    inDateString = year + '-' + month + '-' + day;
    dwDate = new Date(year, month - 1, day, '00', '00', '00');
    if ((dwDate instanceof Date)) {
        calendar = new Calendar(dwDate);
        isValidDate = StringUtils.formatCalendar(calendar, 'yyyy-MM-dd') == inDateString;
    }
    r.renderJSON({
        status: isValidDate
    });
    return r;
}

function pointHistory() {
    var customerPointHistory = app.getController('POSAccount').RequestPointHistory();
    if (customerPointHistory.statusCode === '000') {
        var pointHistoryList = customerPointHistory.pointHistoryList;
        if (!pointHistoryList.isEmpty()) {
            var pointHistoryPagingModel = new PagingModel(pointHistoryList);
            var pageSize = request.httpParameterMap.sz.intValue || 5;
            var start = request.httpParameterMap.start.intValue || 0;
            pointHistoryPagingModel.setStart(start);
            pointHistoryPagingModel.setPageSize(pageSize);

            app.getView({
                PointHistoryPagingModel: pointHistoryPagingModel
            }).render('account/rewards/pointhistory');
        }
    } else if (customerPointHistory.statusCode === '201') {
        app.getView().render('account/rewards/pointemptyhistory');
    } else {
        app.getView().render('account/rewards/pointfailedhistory');
    }
}

/* Web exposed methods */

/** Renders the account overview.
 * @see {@link module:controllers/Account~show} */
exports.Show = guard.ensure(['get', 'https', 'loggedIn'], show);
/** Updates the profile of an authenticated customer.
 * @see {@link module:controllers/Account~editProfile} */
exports.EditProfile = guard.ensure(['get', 'https', 'loggedIn'], editProfile);
/** Handles the form submission on profile update of edit profile.
 * @see {@link module:controllers/Account~editForm} */
exports.EditForm = guard.ensure(['post', 'https', 'loggedIn', 'csrf'], editForm);
/** Renders the password reset dialog.
 * @see {@link module:controllers/Account~passwordResetDialog} */
exports.PasswordResetDialog = guard.ensure(['get'], passwordResetDialog);
/** Renders the password reset screen.
 * @see {@link module:controllers/Account~passwordReset} */
exports.PasswordReset = guard.ensure(['get', 'https'], passwordReset);
/** Handles the password reset form.
 * @see {@link module:controllers/Account~passwordResetDialogForm} */
exports.PasswordResetDialogForm = guard.ensure(['post', 'csrf'], passwordResetDialogForm);
/** The form handler for password resets.
 * @see {@link module:controllers/Account~passwordResetForm} */
exports.PasswordResetForm = guard.ensure(['post', 'https'], passwordResetForm);
/** Renders the screen for setting a new password.
 * @see {@link module:controllers/Account~setNewPassword} */
exports.SetNewPassword = guard.ensure(['get', 'https'], setNewPassword);
/** Handles the set new password form submit.
 * @see {@link module:controllers/Account~setNewPasswordForm} */
exports.SetNewPasswordForm = guard.ensure(['post', 'https'], setNewPasswordForm);
/** Start the customer registration process and renders customer registration page.
 * @see {@link module:controllers/Account~startRegister} */
exports.StartRegister = guard.ensure(['https'], startRegister);
/** Handles registration form submit.
 * @see {@link module:controllers/Account~registrationForm} */
exports.RegistrationForm = guard.ensure(['post', 'https', 'csrf'], registrationForm);
/** Renders the account navigation.
 * @see {@link module:controllers/Account~includeNavigation} */
exports.ShowPopupBarcode = guard.ensure(['get'], showPopupBarcode);
exports.IncludeNavigation = guard.ensure(['get'], includeNavigation);
exports.ConfirmPasswordDialog = guard.ensure(['get', 'https'], confirmPasswordDialog);
exports.ConfirmBirthdayDialog = guard.ensure(['get', 'https'], confirmBirthdayDialog);
exports.CheckCurrentPassword = guard.ensure(['post', 'https', 'csrf'], checkCurrentPassword);
exports.Rewards = guard.ensure(['get', 'https', 'loggedIn'], rewards);
exports.CheckBirthday = guard.ensure(['post', 'https'], checkBirthday);
/** Renders the point history overview.
 * @see {@link module:controllers/Account~pointHistory} */
exports.PointHistory = guard.ensure(['post', 'https', 'loggedIn'], pointHistory);