'use strict';

/**
 * This controller handles customer service related pages, such as the contact us form.
 *
 * @module controllers/CustomerService
 */

/* API Includes */
var Status = require('dw/system/Status');
var Site = require('dw/system/Site');

/* Script Modules */
var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');


/**
 * Renders the customer service overview page.
 */
function show() {
    app.getView('CustomerService').render('content/customerservice');
}
/**
 * Renders the customer service overview page.
 */
function page() {
    var Content = app.getModel('Content');
    var content = Content.get(request.httpParameterMap.cid.stringValue);

    if (!content) {
        // @FIXME Correct would be to set a 404 status code but that breaks the page as it utilizes
        // remote includes which the WA won't resolve
        response.setStatus(410);
        app.getView().render('error/notfound');
    } else {
        var Search = app.getModel('Search');
        var contentSearchModel = Search.initializeContentSearchModel(request.httpParameterMap);
        contentSearchModel.setContentID(null);
        contentSearchModel.search();

        require('~/cartridge/scripts/meta').update(content);

        app.getView({
            Content: content.object,
            ContentSearchResult: contentSearchModel
        }).render(content.object.template || 'content/customerservicepage');
    }
}

/**
 * Renders the left hand navigation.
 */
function leftNav() {
    app.getView('CustomerService').render('content/customerserviceleftnav');
}

/**
 * Provides a contact us form which sends an email to the configured customer service email address.
 */
function contactUs() {
    app.getForm('contactus').clear();
    app.getView('CustomerService', {
        Content: dw.web.Resource.msg('breadcrumb.contactus', 'customerservice', 'Contact Us')
    }).render('content/contactus');
}

/**
 * The form handler for the contactus form.
 */
function submit() {
    var r = require('~/cartridge/scripts/util/Response')
    var formgroup = session.forms.contactus;
    var Email = app.getModel('Email');
    if(formgroup.validationtext.valid){
    	var contactUsResult = Email.sendMail({
            template: 'mail/contactus',
            recipient: Site.getCurrent().getCustomPreferenceValue('customerServiceEmail'),
            subject: formgroup.myquestion.value,
            context: formgroup.comment.value
        });

    	if (contactUsResult && (contactUsResult.getStatus() === Status.OK)) {
            Email.sendMail({
                template: 'mail/contactusthanks',
                recipient: formgroup.email.value,
                subject: dw.web.Resource.msg('contactus.mailtocustomer.subject','forms',null),
                context: formgroup.comment.value
            });
            r.renderJSON({
                message: dw.web.Resource.msg('contactus.messagesent','forms',null) + ' ' + dw.web.Resource.msg('contactus.messagerequires','forms',null)
            });
        } else {
            r.renderJSON({
                message: dw.web.Resource.msg('contactus.messageerror','forms',null)
            });
        }
    }else {
        r.renderJSON({
            message: dw.web.Resource.msg('contactus.validationtexterror','forms',null)
        });
    }
}

/*
 * Module exports
 */

/*
 * Web exposed methods
 */
/** @see module:controllers/CustomerService~show */
exports.Show = guard.ensure(['get'], show);
/** @see module:controllers/CustomerService~page */
exports.Page = guard.ensure(['get'], page);
/** @see module:controllers/CustomerService~leftNav */
exports.LeftNav = guard.ensure(['get'], leftNav);
/** @see module:controllers/CustomerService~contactUs */
exports.ContactUs = guard.ensure(['get', 'https'], contactUs);
/** @see module:controllers/CustomerService~submit */
exports.Submit = guard.ensure(['post', 'https'], submit);
