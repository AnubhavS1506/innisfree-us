'use strict';

/**
 * This controller implements the last step of the checkout. A successful handling
 * of billing address and payment method selection leads to this controller. It
 * provides the customer with a last overview of the basket prior to confirm the
 * final order creation.
 *
 * @module controllers/COSummary
 */

/* API Includes */
var Resource = require('dw/web/Resource');
var Transaction = require('dw/system/Transaction');
var URLUtils = require('dw/web/URLUtils');

/* Script Modules */
var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');
var PointUtil = require('~/cartridge/scripts/util/PointUtil');
var Cart = app.getModel('Cart');
var Site = require('dw/system/Site');

/**
 * Renders the summary page prior to order creation.
 * @param {Object} context context object used for the view
 */
function start(context) {
    var cart = Cart.get();

    // Checks whether all payment methods are still applicable. Recalculates all existing non-gift certificate payment
    // instrument totals according to redeemed gift certificates or additional discounts granted through coupon
    // redemptions on this page.

    var COBilling = app.getController('COBilling');
    if (!COBilling.ValidatePayment(cart)) {
        COBilling.Start();
        return;
    } else {
        Transaction.wrap(function() {
            cart.calculate();
        });

        Transaction.wrap(function() {
            if (!cart.calculatePaymentTransactionTotal()) {
                COBilling.Start();
            }
        });

        if ((typeof context.PlaceOrderError !== 'undefined') && !empty(context.PlaceOrderError.error) && context.PlaceOrderError.error) {
            response.redirect(URLUtils.https('COShipping-Start', 'SPCheckoutStep', 'Billing', 'message', context.PlaceOrderError.message));
        }

        var pageMeta = require('~/cartridge/scripts/meta');
        var viewContext = require('app_innisfree_core/cartridge/scripts/common/extend').immutable(context, {
            Basket: cart.object,
            redirectURL: context.redirectURL
        });
        pageMeta.update({
            pageTitle: Resource.msg('summary.meta.pagetitle', 'checkout', 'SiteGenesis Checkout')
        });
        app.getView(viewContext).render('checkout/summary/spsummary');
    }
}

/**
 * This function is called when the "Place Order" action is triggered by the
 * customer.
 */
function submit() {
    // Calls the COPlaceOrder controller that does the place order action and any payment authorization.
    // COPlaceOrder returns a JSON object with an order_created key and a boolean value if the order was created successfully.
    // If the order creation failed, it returns a JSON object with an error key and a boolean value.
    var placeOrderResult = app.getController('COPlaceOrder').Start();
    if (placeOrderResult.error) {
        start({
            PlaceOrderError: placeOrderResult.PlaceOrderError
        });
    } else if (placeOrderResult.order_created) {
        session.privacy.omsInvResponse = null;
        app.getController('AbandonedCart').ResetAbandonCartProducts();
        showConfirmation(placeOrderResult.Order);
    }
}

/**
 * Renders the order confirmation page after successful order
 * creation. If a nonregistered customer has checked out, the confirmation page
 * provides a "Create Account" form. This function handles the
 * account creation.
 */
function showConfirmation(order) {
    if (!customer.authenticated) {
        // Initializes the account creation form for guest checkouts by populating the first and last name with the
        // used billing address.
        var customerForm = app.getForm('profile.customer');
        customerForm.setValue('firstname', order.billingAddress.firstName);
        customerForm.setValue('lastname', order.billingAddress.lastName);
        customerForm.setValue('email', order.customerEmail);
        customerForm.setValue('orderNo', order.orderNo);
    }

    // Below transaction updates 'isWelcomeKitUsed' status to used in case user 
    // placing first order from his/her registered account
    if ((customer.authenticated) && ('isWelcomeKitUsed' in customer.profile.custom && customer.profile.custom.isWelcomeKitUsed == 'NOT_USED')) {
        Transaction.begin();
        customer.profile.custom.isWelcomeKitUsed = 'USED';
        Transaction.commit();
    }
    Transaction.wrap(function() {
        // Save earpoint
        order.custom.earnPoint = PointUtil.rewardPoint(order);
        order.setPaymentStatus(order.PAYMENT_STATUS_PAID);
    });

    //Call POS-updatePoint Service
    app.getController('POSAccount').UpdatePoint(order);

    //Call POS-updateCustomerInfo Service    
    app.getController('POSAccount').UpdateCustomerInfo();

    app.getForm('profile.login.passwordconfirm').clear();
    app.getForm('profile.login.password').clear();

    var pageMeta = require('~/cartridge/scripts/meta');
    pageMeta.update({
        pageTitle: Resource.msg('confirmation.meta.pagetitle', 'checkout', 'SiteGenesis Checkout Confirmation')
    });
    app.getView({
        Order: order,
        point: order.custom.earnPoint,
        ContinueURL: URLUtils.https('Account-RegistrationForm') // needed by registration form after anonymous checkouts
    }).render('checkout/confirmation/confirmation');
}

/*
 * Module exports
 */

/*
 * Web exposed methods
 */
/** @see module:controllers/COSummary~Start */
exports.Start = guard.ensure(['https'], start);
/** @see module:controllers/COSummary~Submit */
exports.Submit = guard.ensure(['https', 'post', 'csrf'], submit);

/*
 * Local method
 */
exports.ShowConfirmation = showConfirmation;