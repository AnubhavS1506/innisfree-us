/**
* This controllers provides method required for Mycuishon functionality
* 1. Show Choose Your Shade page
* 2. Show Choose Your Applicator page
* 3. Show Choose Your Case page 
* 4. Show Finish (Add all to Cart) page 
* 5. Show Success page 
*
* @module  controllers/MyCuishon
*/

'use strict';

// HINT: do not put all require statements at the top of the file
// unless you really need them for all functions
var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');

/**
 * Renders the 'My Cuishon Landing' page.
 *
 */
function showMcLanding() {
	session.custom.mcShadeId = session.custom.mcApplicatorId = session.custom.mcCaseId = null;
	return null;
}

/**
 * Renders the 'My Cuishon Landing' page.
 *
 */
function showMcHeader() {
	const params = request.httpParameterMap;
	var currentpage = '';
    if(!empty(params.currentpage.stringValue)){
    	currentpage = params.currentpage.stringValue;
    }
    
    app.getView({currentpage: currentpage}).render('rendering/category/mycuishon/mycuishonheader');
}

/**
 * Renders the 'Choose Your Shade' page.
 *
 */
function showShades() {
    const Product = app.getModel('Product');
    const params = request.httpParameterMap;
    var pid = '10021';
    if('mcShadeId' in session.custom && !empty(session.custom.mcShadeId)){
   		pid = session.custom.mcShadeId;
    }
    if(!empty(params.pid.stringValue)){
    	pid = params.pid.stringValue;
    	session.custom.mcShadeId = pid;
    }else
	    if(params.shadeProductType.stringValue == 'matte'){
	    	pid = '10020';
	    }
    var product = Product.get(pid);
    if (product.isMaster()) {
        product = Product.get(product.getVariationModel().getDefaultVariant());
    }
    app.getView('Product',{product: product}).render('rendering/category/mycuishon/mycuishonshade');
}

/**
 * Renders the 'Choose Your Applicator' page.
 *
 */
function showApplicator() {
    const Product = app.getModel('Product');
    const params = request.httpParameterMap;
    var pid = '10034';
   
   if('mcApplicatorId' in session.custom && !empty(session.custom.mcApplicatorId)){
   		pid = session.custom.mcApplicatorId;
    }
    
   
    if(!empty(params.pid.stringValue)){
    	pid = params.pid.stringValue;
    	session.custom.mcApplicatorId = pid;
    }
    
    if(params.saveShadeIdInSession.booleanValue){
    	session.custom.mcShadeId = params.shadeVariantIdToBeSaved.stringValue;
    }
    
    var product = Product.get(pid);
    if (product.isMaster()) {
        product = Product.get(product.getVariationModel().getDefaultVariant());
    }
    app.getView('Product',{product: product}).render('rendering/category/mycuishon/mycuishonapplicator');
}

/**
 * Renders the 'Choose Your Case' page.
 *
 */
function showCase() {
    const Product = app.getModel('Product');
    const params = request.httpParameterMap;
    var pid = '10033';
   
   if('mcCaseId' in session.custom && !empty(session.custom.mcCaseId)){
   		pid = session.custom.mcCaseId;
    }
    
   
    if(!empty(params.pid.stringValue)){
    	pid = params.pid.stringValue;
    	session.custom.mcCaseId = pid;
    }
    
    if(params.saveApplicatorIdInSession.booleanValue){
    	session.custom.mcApplicatorId = params.applicatorVariantIdToBeSaved.stringValue;
    }
    
    var product = Product.get(pid);
    if (product.isMaster()) {
        product = Product.get(product.getVariationModel().getDefaultVariant());
    }
    app.getView('Product',{product: product}).render('rendering/category/mycuishon/mycuishoncase');
}

/**
 * Renders the 'My Cuishon Finish (Add all to Cart)' page.
 *
 */
function showFinish() {
	const params = request.httpParameterMap;
    if(params.saveCaseIdInSession.booleanValue){
    	session.custom.mcCaseId = params.caseVariantIdToBeSaved.stringValue;
    }
 
	app.getView().render('rendering/category/mycuishon/mycuishonfinish');
}

/**
 * Add all three products to cart and Renders the 'My Cuishon Success' page.
 *
 */
function addAllToCart() {
	var URLUtils = require('dw/web/URLUtils');
	var Cart = app.getModel('Cart').goc();
	const Product = app.getModel('Product');
    var pid = '',product = null;
    
    if('mcShadeId' in session.custom && !empty(session.custom.mcShadeId)){
   		pid = session.custom.mcShadeId;
   		product = Product.get(pid);
   		Cart.addProductItem(product.object , 1 ,null);
   		session.custom.mcShadeId = null;
    }
	
	if('mcApplicatorId' in session.custom && !empty(session.custom.mcApplicatorId)){
   		pid = session.custom.mcApplicatorId;
   		product = Product.get(pid);
   		Cart.addProductItem(product.object , 1 ,null);
   		session.custom.mcApplicatorId = null;
    }
    
    
    if('mcCaseId' in session.custom && !empty(session.custom.mcCaseId)){
   		pid = session.custom.mcCaseId;
   		product = Product.get(pid);
   		Cart.addProductItem(product.object , 1 ,null);
   		session.custom.mcCaseId = null;
    }
	

	app.getView().render('rendering/category/mycuishon/mycuishonsuccess');
    
}

/**
 * Remove a 'My Cuishon' product from the session.
 *
 */
function removeMcLineItem() {
	const params = request.httpParameterMap;
    if(!empty(params.mcProductToBeRemoved.stringValue)){
    	
    	switch(params.mcProductToBeRemoved.stringValue){
    		case 'shade' :  session.custom.mcShadeId = null;
    						break
    		case 'applicator' :  session.custom.mcApplicatorId = null;
    						break
    		case 'case' :  session.custom.mcCaseId = null;
    						break
    	}
    	
    }
 
	app.getView().render('rendering/category/mycuishon/mycuishonfinish');
}




exports.ShowShades = guard.ensure(['get'], showShades);
exports.ShowApplicator = guard.ensure(['get'], showApplicator);
exports.ShowCase = guard.ensure(['get'], showCase);
exports.ShowFinish = guard.ensure(['get'], showFinish);
exports.AddAllToCart = guard.ensure(['get'], addAllToCart);
exports.RemoveMcLineItem = guard.ensure(['get'], removeMcLineItem);
exports.ShowMcLanding = guard.ensure(['get'], showMcLanding);
exports.ShowMcHeader = guard.ensure(['get'], showMcHeader);
