/* eslint no-unused-vars: 0 */
'use strict';

/**
 * Controller that renders the home page.
 *
 * @module controllers/Home
 */

var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');

/**
 * Renders the home page.
 */
function show() {
    var rootFolder = require('dw/content/ContentMgr').getSiteLibrary().root;
    require('~/cartridge/scripts/meta').update(rootFolder);

    app.getView().render('content/home/homepage');
}

/**
 * Renders the Element styleguide page.
 */
function element() {
    var rootFolder = require('dw/content/ContentMgr').getSiteLibrary().root;
    require('~/cartridge/scripts/meta').update(rootFolder);

    app.getView().render('content/styleguide/element');
}

/**
 * Renders the Banner styleguide page.
 */
function banner() {
    var rootFolder = require('dw/content/ContentMgr').getSiteLibrary().root;
    require('~/cartridge/scripts/meta').update(rootFolder);

    app.getView().render('content/styleguide/banner');
}

/**
 * Renders the Product styleguide page.
 */
function product() {
    var rootFolder = require('dw/content/ContentMgr').getSiteLibrary().root;
    require('~/cartridge/scripts/meta').update(rootFolder);

    app.getView().render('content/styleguide/product');
}

/*
 * Export the publicly available controller methods
 */
/** Renders the Element Style Guide page.
 * @see module:controllers/Home~Element */
exports.Element = guard.ensure(['get'], element);

/** Renders the Banner Style Guide page.
 * @see module:controllers/Home~Banner */
exports.Product = guard.ensure(['get'], product);


/** Renders the Product Style Guide page.
 * @see module:controllers/Home~Banner */
exports.Banner = guard.ensure(['get'], banner);