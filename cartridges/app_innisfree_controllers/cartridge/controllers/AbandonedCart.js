'use strict';

/**
 * Controller that handles the Abandoned Cart functionality of Innisfree
 *
 * @module  controllers/AbandonedCart
 */
// HINT: do not put all require statements at the top of the file
// unless you really need them for all functions
/* Script Modules */
var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');
var Transaction = require('dw/system/Transaction');

/**
* Description of the function
*
* @return {String} The string 'myFunction'
*/
// var myFunction = function(){
//     return 'myFunction';
// }
/**
 * Renders the home page.
 */
function show() {

	try{
		var BasketMgr = require('dw/order/BasketMgr');
		if (customer.registered && customer.authenticated) {
			var CartModel = app.getModel('Cart');
			var ProductModel = app.getModel('Product');
			var abandonedBasket = CartModel.goc();
			var abandonedCartPlis = JSON.parse(customer.profile.custom.abandonedCartProductJson).plis;
			var currentPidsInCart = new dw.util.LinkedHashSet();
			var product = null;
            var basketProductLineItems = abandonedBasket.productlineItems;
            
            if(!empty(abandonedBasket)){
            	var basketProductLineItems = abandonedBasket.getProductLineItems().iterator();
    			while (basketProductLineItems.hasNext()) {
        			var productLineItem = basketProductLineItems.next();
        			currentPidsInCart.add(productLineItem.productID);
    			}
            }
            
            
			for (var i = 0; i < abandonedCartPlis.length; i++) {
                var pli = abandonedCartPlis[i];
                var productIdToAdd = pli.pid;
                var productIdToAddQty = pli.qty;
                if(!currentPidsInCart.contains(productIdToAdd.toString())){
            		product = ProductModel.get(productIdToAdd.toString()); 
            		abandonedBasket.addProductItem(product.object , productIdToAddQty ,null);
            	}
            } 
            
			app.getController('Cart').Show(abandonedBasket);
		}else{
			app.getController('Home').Show();
		}
	}catch(e){
		var ex = e;
		dw.system.Logger.error('Exception in AbandonedCart show :'+ e);
        
	}
	
}

function updateAbandonCartProducts(cart){
	cart = cart || app.getModel('Cart').get();
	try{
		if (customer.registered && customer.authenticated) {
			var basketProductLineItems = cart.getProductLineItems().iterator();
			var plis = [], abandonedCartJson = null;
	    	while (basketProductLineItems.hasNext()) {
				var productLineItem = basketProductLineItems.next();
				if (productLineItem && !productLineItem.isBonusProductLineItem()) {
					plis[plis.length] = new Object(); 
					plis[plis.length-1].pid = productLineItem.productID;
					plis[plis.length-1].qty = productLineItem.quantityValue;
				}
			}
			if(plis.length>0){
				abandonedCartJson = new Object();
				abandonedCartJson.plis = plis;
			}	
			Transaction.wrap(function() {
	    		customer.profile.custom.abandonedCartProductJson = JSON.stringify(abandonedCartJson);
	    		customer.profile.custom.isAbandonedCartMailSent = false; 
	        });
	        
	        
	    	
	    }
	}catch(e){
		dw.system.Logger.error('Exception in AbandonedCart updateAbandonCartProducts :'+ e);
	}
	
    
	
}

function resetAbandonCartProducts(){
	try{
		if (customer.registered && customer.authenticated) {
			Transaction.wrap(function() {
	    		customer.profile.custom.abandonedCartProductJson = null;
	    		customer.profile.custom.isAbandonedCartMailSent = false; 
	        });
	    	
	    }
	}catch(e){
		dw.system.Logger.error('Exception in AbandonedCart resetAbandonCartProducts :'+ e);
	}
	
    
	
}



/* Exports of the controller */
/** Renders the home page.
 * @see module:controllers/Home~show */
exports.Show = guard.ensure(['get', 'https', 'loggedIn'], show);

exports.UpdateAbandonCartProducts = updateAbandonCartProducts;
exports.ResetAbandonCartProducts = resetAbandonCartProducts;
