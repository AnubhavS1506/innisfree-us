'use strict';
/**
 * Controller that calls POS customer registration and customer update,
 * 
 * @module controllers/POSAccount
 * 
 */
/* API Modules */
var dwsvc = require('dw/svc');
var Site = require('dw/system/Site');
var Transaction = require('dw/system/Transaction');
var ArrayList = require('dw/util/ArrayList');

/* Script Modules */
var guard = require('~/cartridge/scripts/guard');
var POSUtils = require('int_pos/cartridge/scripts/library/libPOS');

function requestPointHistoryRq() {
    var service, webref, requestPointHistory, requestParam, serviceResult, customerProfile;

    //get service registry object, webreference and service request.
    service = dwsvc.ServiceRegistry.get('posRequestPointHistory');
    webref = webreferences.ForDWImplService;
    requestPointHistory = new webref.RequestPointHistory();
    requestParam = new webref.RequestPointHistory_params();

    if (customer.authenticated && customer.registered) {
        customerProfile = customer.profile;

        // Preparing request object
        requestParam.SALORGCD = POSUtils.generateChannelCode();
        requestParam.CSTMSEQ = customerProfile.customerNo;

        requestPointHistory.PARAMS = requestParam;

        //Call web service, In case of failed try for 3 times.
        //pass your service and request object to serviceCall(service,request)
        serviceResult = serviceCall(service, requestPointHistory);

        //Update Customer Profile in DW 
        return requestPointHistoryRs(serviceResult);
    }
}

function requestPointHistoryRs(serviceResult) {
    var result, code, custPointHistory = null;
    if (!empty(serviceResult) && serviceResult.status == 'OK') {
        result = serviceResult.object.RESULT;
        code = result.CODE;
        custPointHistory = {
            'statusCode': result.CODE,
            'message': result.MESSAGE
        };
        if (code == '000') {
            var pointHistoryList = new ArrayList();
            for (var i = 0; i < result.LIST.length; i++) {
                var pointHistoryDetails = {
                    'pointUseDate': POSUtils.prepareDateFormat(result.LIST[i].CHNGDATE),
                    'point': result.LIST[i].CHNGMLG,
                    'pointCode': POSUtils.prepareStatusFormat(result.LIST[i].CHNGCD),
                    'pointReasonCode': result.LIST[i].CHNGRNCD,
                    'pointReasonName': result.LIST[i].CHNGRNNM,
                    'storeCode': result.LIST[i].PRTNRID,
                    'storeName': result.LIST[i].PRTNRNM
                };
                pointHistoryList.add(pointHistoryDetails);
            }
            custPointHistory.pointHistoryList = pointHistoryList;
        }
    }
    return custPointHistory;
}

function updatePointRq(Order) {
    if (!empty(Order)) {
        var service, webref, updatePoint, requestParam, serviceResult, customerProfile, redeemedPoints = 0,
            earnedPoints = 0,
            points = 0,
            redeemedAmount = 0;
        //get service registry object, webreference and service request.
        service = dwsvc.ServiceRegistry.get('posUpdatePoint');
        webref = webreferences.ForDWImplService;
        updatePoint = new webref.UpdatePoint();
        requestParam = new webref.UpdatePoint_params();

        earnedPoints = Order.custom.earnPoint;
        if (earnedPoints > 0) {
            points = earnedPoints;
            requestParam.TRCD = 'PR41';
        } else {
            //SitePreference to fetch Currency to Point ratio
            var pointRate = dw.system.Site.getCurrent().getCustomPreferenceValue('pointToCurrency');
            redeemedAmount = Order.custom.discountAmount;
            redeemedPoints = redeemedAmount * pointRate;
            points = redeemedPoints;
            requestParam.TRCD = 'PR40';
        }
        //Call UpdatePoint Service.
        if (points > 0) {
            customerProfile = customer.profile;

            // Preparing request object
            requestParam.SALORGCD = POSUtils.generateChannelCode();
            requestParam.CSTMSEQ = customerProfile.customerNo;
            requestParam.USEDATE = POSUtils.generateDateString(new Date(), 'yyyyMMdd');
            requestParam.SEQ = POSUtils.generateUniqueCode();
            requestParam.USEPOINT = points;
            requestParam.REGDATE = POSUtils.prepareDateString(customerProfile.custom.updateDateTime);
            requestParam.SENDDATE = POSUtils.prepareDateString(new Date());

            updatePoint.PARAMS = requestParam;

            //Call web service, In case of failed try for 3 times.
            //pass your service and request object to serviceCall(service,request)
            serviceResult = serviceCall(service, updatePoint);

            //Update Customer Profile in DW 
            updatePointRs(serviceResult);
        }
    }
}

function updatePointRs(serviceResult) {
    var result, code;
    if (!empty(serviceResult) && serviceResult.status == 'OK') {
        var customerProfile = customer.profile;
        result = serviceResult.object.RESULT;
        code = result.CODE;
        if (code != '000') {
            Transaction.begin();
            customerProfile.custom.isCustomerInfoUpdated = false;
            customerProfile.custom.updateDateTime = new Date();
            Transaction.commit();
        } else {
            Transaction.begin();
            customerProfile.custom.isCustomerInfoUpdated = true;
            customerProfile.custom.updateDateTime = new Date();
            Transaction.commit();
        }
    }
}

function updateCustomerInfoRq() {
    var service, webref, updateCustomerInfo, requestParam, serviceResult, customerProfile;

    //get service registry object, webreference and service request.
    service = dwsvc.ServiceRegistry.get('posUpdateCustomerInfo');
    webref = webreferences.ForDWImplService;
    updateCustomerInfo = new webref.UpdateCustomerInfo();
    requestParam = new webref.UpdateCustomerInfo_params();

    //check for logged in and registered customer
    if (customer.authenticated && customer.registered) {
        customerProfile = customer.profile;

        // Preparing request object
        requestParam.SALORGCD = POSUtils.generateChannelCode();
        requestParam.CSTMSEQ = customerProfile.customerNo;
        requestParam.USERID = customerProfile.email;
        requestParam.CSTMFIRSTNM = customerProfile.firstName;
        requestParam.CSTMLASTNM = customerProfile.lastName;
        var birthday = customerProfile.birthday;
        if (!empty(birthday)) {
            var birthYYYY = birthday.getFullYear();
            var birthMM = POSUtils.generateDateString(birthday, 'MM');
            var birthDD = POSUtils.generateDateString(birthday, 'dd');
            var birthMMDD = birthMM + birthDD;
            requestParam.BIRTHYY = birthYYYY;
            requestParam.BIRTHMD = birthMMDD;
        } else {
            requestParam.BIRTHYY = '';
            requestParam.BIRTHMD = '';
        }
        requestParam.VIPKITUSEYN = POSUtils.prepareKitData(customerProfile.custom.isVipKitUsed.value);
        requestParam.BIRTHKITUSELYN = POSUtils.prepareKitData(customerProfile.custom.isBirthdayKitUsed.value);
        requestParam.WELCOMKITUSEYN = POSUtils.prepareKitData(customerProfile.custom.isWelcomeKitUsed.value);
        requestParam.DATAFG = customerProfile.custom.isCrtUpd;
        requestParam.REGDATE = POSUtils.prepareDateString(customerProfile.custom.updateDateTime);
        requestParam.SENDDATE = POSUtils.prepareDateString(new Date());

        updateCustomerInfo.PARAMS = requestParam;

        //Call web service, In case of failed try for 3 times.
        //pass your service and request object to serviceCall(service,request)
        serviceResult = serviceCall(service, updateCustomerInfo);

        //Update Customer Profile in DW 
        updateCustomerInfoRs(serviceResult);
    }
}

function updateCustomerInfoRs(serviceResult) {
    var result, code;
    if (!empty(serviceResult) && serviceResult.status == 'OK') {
        var customerProfile = customer.profile;
        result = serviceResult.object.RESULT;
        code = result.CODE;
        if (code != '000') {
            Transaction.begin();
            customerProfile.custom.isCustomerInfoUpdated = false;
            customerProfile.custom.updateDateTime = new Date();
            Transaction.commit();
        } else {
            var isCrtUpd = customerProfile.custom.isCrtUpd.value;
            Transaction.begin();
            if (!empty(isCrtUpd) && isCrtUpd.equals('I')) {
                customerProfile.custom.isCrtUpd = 'U';
            }
            customerProfile.custom.isCustomerInfoUpdated = true;
            customerProfile.custom.updateDateTime = new Date();
            Transaction.commit();
        }
    }
}

function requestCustomerInfoRq() {
    var service, webref, requestCustomerInfo, requestParam, serviceResult, customerProfile;

    //get service registry object, webreference and service request.
    service = dwsvc.ServiceRegistry.get('posRequestCustomerInfo');
    webref = webreferences.ForDWImplService;
    requestCustomerInfo = new webref.RequestCustomerInfo();
    requestParam = new webref.RequestCustomerInfo_params();
    customerProfile = customer.profile;

    //Preparing request object
    requestParam.SALORGCD = POSUtils.generateChannelCode();
    requestParam.CSTMSEQ = customerProfile.customerNo;

    requestCustomerInfo.PARAMS = requestParam;

    //Call web service, In case of failed try for 3 times.
    //pass your service and request object to serviceCall(service,request)
    serviceResult = serviceCall(service, requestCustomerInfo);

    //Update Customer Profile in DW 
    requestCustomerInfoRs(serviceResult);

    return serviceResult;
}

function requestCustomerInfoRs(serviceResult) {
    var result, code;
    if (!empty(serviceResult) && serviceResult.status == 'OK') {
        var customerProfile = customer.profile;
        result = serviceResult.object.RESULT;
        code = result.CODE;
        if (code != '000') {
            Transaction.begin();
            customerProfile.custom.isCustomerInfoUpdated = false;
            customerProfile.custom.updateDateTime = new Date();
            Transaction.commit();
        } else {
            Transaction.begin();
            customerProfile.custom.membershipLevel = result.CSTMGRADECD;

            if (!empty(result.EXPIREDATE)) {
                customerProfile.custom.membershipExpiryDate = POSUtils.prepareDateFromString(result.EXPIREDATE);
            }
            if (!empty(result.ABLPOINT)) {
                customerProfile.custom.availablePoints = new Number(result.ABLPOINT);
            }
            if (!empty(result.REQSPEND)) {
                customerProfile.custom.spendReqForNextLevel = new Number(result.REQSPEND);
            }
            customerProfile.custom.isCustomerInfoUpdated = true;
            customerProfile.custom.updateDateTime = new Date();
            Transaction.commit();
        }
    }
    return code;
}

function serviceCall(service, requestObj) {
    var response, isRetry, retryAttempts, i = 0;
    retryAttempts = Site.getCurrent().getCustomPreferenceValue('posRetryAttempts');

    for (i; i < retryAttempts; i++) {
        response = service.call(requestObj);
        if (response.status == 'OK') {
            isRetry = doRetry(response.object.RESULT.CODE);
            if (!isRetry) {
                break;
            }
        }
    }
    return response;
}

function doRetry(returnCode) {
    var respCodeList, retry = false;
    respCodeList = Site.getCurrent().getCustomPreferenceValue('posResponseCodes');

    for (var i = 0; i < respCodeList.length; i++) {
        if (respCodeList[i] == returnCode) {
            retry = true;
        }
    }
    return retry;
}

function requestPurchaseHistoryRq() {
    var service, webref, requestPurchaseHistory, requestParam, orderList,
        POSUtils, serviceResult, customerProfile, purchaseStartDate, purchaseEndDate;
    POSUtils = require('int_pos/cartridge/scripts/library/libPOS');

    //get service registry object, webreference and service request.
    service = dwsvc.ServiceRegistry.get('posOfflineCustomerPurchaseHistoryInfo');
    webref = webreferences.ForDWImplService;
    requestPurchaseHistory = new webref.OfflineCustomerPurchaseHistory();
    requestParam = new webref.OfflineCustomerPurchaseHistory_params();
    customerProfile = customer.profile;

    purchaseStartDate = new Date(1970, 0, 1);
    purchaseStartDate = POSUtils.generateDateString(purchaseStartDate, 'yyyyMMdd');

    purchaseEndDate = new Date();
    purchaseEndDate = POSUtils.generateDateString(purchaseEndDate, 'yyyyMMdd');
    
    //Preparing request object
    requestParam.SALORGCD = POSUtils.generateChannelCode();
    requestParam.CSTMSEQ = customerProfile.customerNo;
    requestParam.SALSTDATE = purchaseStartDate;
    requestParam.SALEDDATE = purchaseEndDate;
    requestPurchaseHistory.PARAMS = requestParam;

    //Call web service, In case of failed try for 3 times.
    //pass your service and request object to serviceCall(service,request)
    serviceResult = serviceCall(service, requestPurchaseHistory);

    // Get Purchase History in DW 
    orderList = POSUtils.getPurchaseHistory(serviceResult);

    return orderList;
}

/** Renders the pos overview.
 * @see {@link module:controllers/POSAccount~updateCustomerInfoRq} */
exports.UpdateCustomerInfo = updateCustomerInfoRq;
/** Renders the pos overview.
 * @see {@link module:controllers/POSAccount~requestCustomerInfoRq} */
exports.RequestCustomerInfo = guard.ensure(['get', 'https', 'loggedIn'], requestCustomerInfoRq);
/** Renders the pos overview.
 * @see {@link module:controllers/POSAccount~updatePointRq} */
exports.UpdatePoint = guard.ensure(['post', 'https'], updatePointRq);
/** Renders the pos overview.
 * @see {@link module:controllers/POSAccount~requestPurchaseHistoryRq} */
exports.RequestPurchaseHistoryInfo = guard.ensure(['get', 'https', 'loggedIn'], requestPurchaseHistoryRq);
 /** Renders the pos overview.
  * @see {@link module:controllers/POSAccount~requestPointHistoryRq} */
exports.RequestPointHistory = guard.ensure(['post', 'https', 'loggedIn'], requestPointHistoryRq); 