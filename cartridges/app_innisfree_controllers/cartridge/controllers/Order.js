'use strict';

/**
 * Controller that manages the order history of a registered user.
 *
 * @module controllers/Order
 */

/* API Includes */
var ContentMgr = require('dw/content/ContentMgr');
var OrderMgr = require('dw/order/OrderMgr');
var PagingModel = require('dw/web/PagingModel');
var Site = require('dw/system/Site');

/* Script Modules */
var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');
var DateUtils = require('~/cartridge/scripts/util/Date');
var Email = require('~/cartridge/scripts/models/EmailModel');
var Response = require('~/cartridge/scripts/util/Response');
var URLUtils = require('dw/web/URLUtils');

/**
 * Renders a page with the order history of the current logged in customer.
 *
 * Creates a PagingModel for the orders with information from the httpParameterMap.
 * Invalidates and clears the orders.orderlist form. Updates the page metadata. Sets the
 * ContinueURL property to Order-Orders and renders the order history page (account/orderhistory/orders template).
 */

function historyCustom() {
    var CurrentCustomer = customer.getProfile();
    var orderModel = app.getModel('OrderHistory');
    var orders = orderModel.getHistory(CurrentCustomer.customerNo);
    for (var i = 0; i < orders.length; i++) {
        orders[i].orderDate = DateUtils.getOrderDateFromOMS(orders[i].orderDate);
    }
    var parameterMap = request.httpParameterMap;
    var pageSize = parameterMap.sz.intValue || 10;
    var start = parameterMap.start.intValue || 0;
    var orderPagingModel = new PagingModel(orders);
    orderPagingModel.setPageSize(pageSize);
    orderPagingModel.setStart(start);

    var orderListForm = app.getForm('orders.orderlist');
    orderListForm.invalidate();
    orderListForm.clear();
    orderListForm.copyFrom(orderPagingModel.pageElements);

    var pageMeta = require('~/cartridge/scripts/meta');
    pageMeta.update(ContentMgr.getContent('myaccount-orderhistory'));

    app.getView({
        selected: 'orderHistory',
        OrderPagingModel: orderPagingModel,
        ContinueURL: dw.web.URLUtils.https('Order-Orders')
    }).render('account/orderhistory/orders_history');
}

function purchaseHistory() {
    var parameterMap = request.httpParameterMap;
    var pageSize = parameterMap.sz.intValue || 10;
    var start = parameterMap.start.intValue || 0;

    var orders = app.getController('POSAccount').RequestPurchaseHistoryInfo();
    var orderPagingModel = new PagingModel(orders);
    orderPagingModel.setPageSize(pageSize);
    orderPagingModel.setStart(start);

    var orderListForm = app.getForm('orders.orderlist');
    orderListForm.invalidate();
    orderListForm.clear();
    orderListForm.copyFrom(orderPagingModel.pageElements);

    app.getView({
        selected: 'orderHistory',
        OrderPagingModel: orderPagingModel
    }).render('account/orderhistory/orderspurchase_history');
}

/**
 * Gets an OrderView and renders the order detail page (account/orderhistory/orderdetails template). If there is an error,
 * redirects to the {@link module:controllers/Order~history|history} function.
 */

function ordersCustom() {
    var orderId = request.httpParameterMap.order_no.stringValue;
    var CurrentCustomer = customer.getProfile();
    var Order = app.getModel('Order');
    var orderModel = Order.get(orderId);
    var orderDetail = orderModel.object;

    if (CurrentCustomer.customerNo === orderDetail.customerNo) {
        var orderDetailModel = app.getModel('OrderDetail');
        var order = orderDetailModel.getDetail(orderId);

        app.getView({
            selected: 'orderHistory',
            Order: order
        }).render('account/orderhistory/orderdetails_custom');
    } else {
        response.redirect(URLUtils.https('Account-Show'));
    }

}

function returnRequest() {
    app.getView().render('account/returnrequest/step');
}

/**
 * Renders a page with details of a single order. This function
 * renders the order details by the UUID of the order, therefore it can also be used
 * for unregistered customers to track the status of their orders. It
 * renders the order details page (account/orderhistory/orderdetails template), even
 * if the order cannot be found.
 */
function track() {
    var parameterMap = request.httpParameterMap;

    if (empty(parameterMap.orderID.stringValue)) {
        app.getView().render('account/orderhistory/orderdetails');
        return response;
    }

    var uuid = parameterMap.orderID.stringValue;
    var orders = OrderMgr.searchOrders('UUID={0} AND status!={1}', 'creationDate desc', uuid, dw.order.Order.ORDER_STATUS_REPLACED);

    if (empty(orders)) {
        app.getView().render('account/orderhistory/orderdetails');
    }

    var Order = orders.next();
    app.getView({
        Order: Order
    }).render('account/orderhistory/orderdetails');
}

/**
 * Send an email to recipient of order return
 */
function sendReturnOrderEmail() {
    var orderNo = request.httpParameterMap.orderNo.stringValue;
    var order = OrderMgr.getOrder(orderNo);

    if (!empty(order)) {
        var customerEmail = order.getCustomerEmail();
        var customerName = order.getCustomerName();
        var customerNo = order.getCustomerNo();
        var emailContent = ContentMgr.getContent('order-return-email');
        var customerServiceEmail = Site.getCurrent().getCustomPreferenceValue('customerServiceEmail');
        var subject = dw.web.Resource.msgf('orderreturn.subject', 'email', null, orderNo);
        var contentBody = '',
            contentTitle = '';

        if (emailContent != null) {
        	contentTitle = emailContent.pageTitle;
        	contentTitle = contentTitle.replace('{orderno}', orderNo);
            if (!empty(emailContent.custom.body)) {
                contentBody = emailContent.custom.body.markup;
                contentBody = contentBody.replace('{customerno}', customerNo)
                    .replace('{customername}', customerName)
                    .replace('{orderno}', orderNo);
            }
        }

        var status = Email.sendMail({
            recipient: customerEmail,
            template: 'mail/orderreturn',
            subject: contentTitle,
            context: {
                contentBody: contentBody
            }
        });
        
        Email.sendMail({
            recipient: customerServiceEmail,
            template: 'mail/orderreturnforcs',
            subject: subject,
            context: {
                contentBody: contentBody,
                customerNo: customerNo,
                customerName: customerName,
                orderNo: orderNo
            }
        });

        Response.renderJSON({
            status: status.code
        });
    } else {
        Response.renderJSON({
            status: false
        });
    }
}


/*
 * Module exports
 */

/*
 * Web exposed methods
 */
/** Renders a page with the order history of the current logged in customer.
 * @see module:controllers/Order~history */
//exports.History = guard.ensure(['get', 'https', 'loggedIn'], history);
exports.History = guard.ensure(['get', 'https', 'loggedIn'], historyCustom);

/** Renders the order detail page.
 * @see module:controllers/Order~orders */
//exports.Orders = guard.ensure(['post', 'https', 'loggedIn'], orders);
exports.Orders = guard.ensure(['get', 'https', 'loggedIn'], ordersCustom);

exports.ReturnRequest = guard.ensure(['get', 'https', 'loggedIn'], returnRequest);

/** Renders a page with details of a single order.
 * @see module:controllers/Order~track */
exports.Track = guard.ensure(['get', 'https'], track);

/** Send order return email.
 * @see module:controllers/Order~sendReturnOrderEmail */
exports.SendReturnOrderEmail = guard.ensure(['get', 'https'], sendReturnOrderEmail);

exports.PurchaseHistory = guard.ensure(['get', 'https', 'loggedIn'], purchaseHistory);