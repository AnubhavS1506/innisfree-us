'use strict';

/* API Includes */
var StringUtils = require('dw/util/StringUtils');
var Calendar = require('dw/util/Calendar');
var Resource = require('dw/web/Resource');

/*
 * This function is get point rate of current customer
 */
function getPointRate(isProduct) {
    var result = 0;
    //Get reward in JSON format for customer membership level
    var rewards = JSON.parse(dw.system.Site.getCurrent().getCustomPreferenceValue('rewardPoint'));
    
    if (customer.authenticated && customer.registered) {
        var level = customer.profile.custom.membershipLevel;

        for (var i = 0; i < rewards.length; i++) {
            if (level === rewards[i].id) {
                result = rewards[i].rate;
                break;
            }
        }
    } else if (isProduct) {
    	return !empty(rewards[0]) ? rewards[0].rate : 3;
    }
    return result;
}

/*
 * This function is used to calculate earned point after purchase
 */
function rewardPoint(LineItemCtnr) {
    var result = 0;
    if (customer.authenticated && customer.registered && !empty(LineItemCtnr)) {
        var pointRate = getPointRate();
        if (pointRate > 0) {
            var gcAmount = 0;
            var gcPaymentInstruments = LineItemCtnr.getGiftCertificatePaymentInstruments();
            //if GC exist
            if (gcPaymentInstruments.length > 0) {
                for (var i = 0; i < gcPaymentInstruments.length; i++) {
                    gcAmount = gcPaymentInstruments[i].paymentTransaction.amount.value;
                }
            }
            var pointToCurrency = dw.system.Site.getCurrent().getCustomPreferenceValue('pointToCurrency');
            var subTotal = LineItemCtnr.getAdjustedMerchandizeTotalNetPrice().value - gcAmount;
            subTotal = subTotal < 0 ? 0 : subTotal;
            result = (subTotal * pointRate * pointToCurrency) / 100;
        }
    }

    return Math.ceil(result);
}

/*
 * This function is used to get the giftcertificate code
 */
function giftCertCode() {
    var calendar = new Calendar();
    var dateString = StringUtils.formatCalendar(calendar, 'yyyyMMddHHmmss');
    var gcPrefix = Resource.msg('point.giftcert.prefix', 'preference', 'redeempoint');
    var giftCertID = gcPrefix + '_' + customer.getProfile().getCustomerNo() + '_' + dateString;
    return giftCertID;
}

/**
 * This function is used to return membership Level
 */
function getCustomerLevel() {
    var name;
    var level = customer.profile.custom.membershipLevel;

    //Get reward in JSON format for customer membership level
    var rewards = JSON.parse(dw.system.Site.getCurrent().getCustomPreferenceValue('rewardPoint'));
    for (var i = 0; i < rewards.length; i++) {
        if (level === rewards[i].id) {
            name = rewards[i].name;
            break;
        }
    }
    return name;
}

/*
 * This function is used to calculate productPoints
 */
function getProductPoints(productPrice) {
    //SitePreference to fetch Currency to Point ratio
    var pointToCurrency = dw.system.Site.getCurrent().getCustomPreferenceValue('pointToCurrency');
    var pointRate = getPointRate(true);
    var productPoints = (productPrice * pointRate * pointToCurrency) / 100;
    
    return Math.round(productPoints);
}

/*
 * This function is used to calculate orderPoints
 */
function calculateOrderPoints(cart) {
    var orderMerchandizeNetTotal, orderPercentage, orderAmountToRedeem, pointRate, orderPoints;
    orderMerchandizeNetTotal = cart.object.getMerchandizeTotalNetPrice().value;

    //SitePreference to fetch Order Percentage
    orderPercentage = dw.system.Site.getCurrent().getCustomPreferenceValue('maxRewardPoints');

    //Calculate order amount which can be paid using points
    orderAmountToRedeem = (orderMerchandizeNetTotal * orderPercentage) / 100;

    //SitePreference to fetch Currency to Point ratio
    pointRate = dw.system.Site.getCurrent().getCustomPreferenceValue('pointToCurrency');

    orderPoints = orderAmountToRedeem * pointRate;

    //Rounding amount to compare max amount
    orderPoints = Math.round(orderPoints);

    return orderPoints;
}

/*
 * This function is used to check minimum and maximum allowed points per order
 */
function checkRedeemPoint(args) {
    var cart, point, minPoint, maxPoint, orderPercentage, response;
    point = args.rewardPoints;
    cart = args.cart;

    //site preference minimum redeem point and max point can redeem order% of
    minPoint = dw.system.Site.getCurrent().getCustomPreferenceValue('minRewardPoints');

    //SitePreference to fetch Order Percentage
    orderPercentage = dw.system.Site.getCurrent().getCustomPreferenceValue('maxRewardPoints');

    //maximum point to be redeem per order
    maxPoint = calculateOrderPoints(cart);

    //redeem point should not be less than minimum allowed points
    if (point < minPoint) {
        response = {
            status: 'ERROR',
            message: 'less',
            code: minPoint
        };
        return response;
    }

    //redeem point should not be more than maximum allowed points per order
    if (point > maxPoint) {
        response = {
            status: 'ERROR',
            message: 'more',
            code: orderPercentage,
            maxPoint:maxPoint
        };
        return response;
    }
    response = {
        status: 'OK'
    };
    return response;
}

function getPointProgressData(point) {
	var level = getCustomerLevel(); 
	var reqAmount = customer.profile.custom.spendReqForNextLevel;
	var pointRate = getPointRate();
	var availablePoints = customer.profile.custom.availablePoints;
	var nextLevelPoints = Math.round(reqAmount * pointRate);
	var totalPoints = nextLevelPoints + availablePoints;
	var currentPointsPerc = Math.round((availablePoints / totalPoints) * 100);
	var orderPointsPerc = Math.round((point / totalPoints) * 100);
	
	if ((currentPointsPerc + orderPointsPerc) > 100) {
		orderPointsPerc = 100 - currentPointsPerc;
	}
	return {
		level: level,
		totalPoints: totalPoints,
		orderPointsPerc: orderPointsPerc,
		currentPointsPerc: currentPointsPerc,
		availablePoints: availablePoints
	}
}

module.exports = {
    getPointRate: getPointRate,
    giftCertCode: giftCertCode,
    checkRedeemPoint: checkRedeemPoint,
    getCustomerLevel: getCustomerLevel,
    rewardPoint: rewardPoint,
    getProductPoints: getProductPoints,
    calculateOrderPoints: calculateOrderPoints,
    getPointProgressData: getPointProgressData
};