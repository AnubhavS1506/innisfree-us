'use strict';
var Calendar = require('dw/util/Calendar');
var StringUtils = require('dw/util/StringUtils');

exports.getOrderDateFromOMS = function (orderDate) {
    var calendar = new Calendar();
    calendar.parseByFormat(orderDate, 'yyyy-MM-dd\'T\'HH:mm:ss');
    return StringUtils.formatCalendar(calendar, 'dd/MM/yyyy');
};