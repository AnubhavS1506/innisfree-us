var ServiceRegistry = require('dw/svc/ServiceRegistry');

ServiceRegistry.configure('http.inventorycheck', {
    createRequest: function(svc:HTTPService, params) {
        var reqParam = "";
        for(var i=0; i<params.length; i++){
            reqParam = reqParam+"{'availQty': null, 'productId': '"+params[i]+"'},";
        }
        reqParam = reqParam.substr(0, reqParam.length-1);
        var completeReqParam="{'items': ["+reqParam+"]}";
        svc.URL = svc.URL+escape(completeReqParam);
        return escape(completeReqParam);
    },
    parseResponse: function(svc:HTTPService, response) {
        return response;
    }
});