var ServiceRegistry = require('dw/svc/ServiceRegistry');

ServiceRegistry.configure('http.orderdetail', {
    createRequest: function(svc, params) {
        if (svc.getRequestMethod() === 'POST') {
            svc.addHeader('Content-Type', 'application/json; charset=utf-8');
            return JSON.stringify(params);
        }
        return null;
    },
    parseResponse: function(svc, response) {
        return response;
    }
});