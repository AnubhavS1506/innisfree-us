var AbstractModel = require('./AbstractModel');
var ServiceRegistry = require('dw/svc/ServiceRegistry');
var Service = require('dw/svc/Service');
var Calendar = require('dw/util/Calendar');
var StringUtils = require('dw/util/StringUtils');
var ShippingMgr = require('dw/order/ShippingMgr');
var app = require('~/cartridge/scripts/app');
var Order = app.getModel('Order');

var OrderDetailModel = AbstractModel.extend({

});

OrderDetailModel.getDetail = function(orderID) {
    require('~/cartridge/scripts/services/initOrderDetailService');
    var service; 
    var result;
    var orderdetail = null;
    var isNotExistedInOMS = true;
    service = ServiceRegistry.get("http.orderdetail");
    service.URL+= "?SONO=" + orderID;
    service.setRequestMethod("GET");
    result = service.call();

    // connect service OK
    if (!empty(result) && !empty(result.object)) {
        var xmlText = new XML(result.object.text);
        var ns = xmlText.namespace();
        var order = xmlText.children(); 

        // order existed in OMS
        if (order.length() > 0) {
            orderdetail = getOrderDetailFromOMS(order, ns);
            isNotExistedInOMS = false;
        }
    }

    // connect service fail or return result fail
    // order existed in DW
    if (isNotExistedInOMS) {
        orderdetail = getOrderDetailFromSFCC(orderID);
    }

    // when shipping status is shipped then get shipped date
    if (orderdetail.shippingStatus.equalsIgnoreCase('SHIPPED')) {
        orderdetail.shippedDate = getShippedDate(orderID);
    }

    updateStatusGiftProducts(orderdetail);
    return orderdetail;
};

function getOrderDetailFromSFCC(orderID) {
    var orderModel = Order.get(orderID);
    var dwOrderDetail = orderModel.object;
    var orderdetail = null;

    if (!empty(dwOrderDetail)) {
        orderdetail = {
            'orderNo': dwOrderDetail.orderNo,
            'status': dwOrderDetail.status.toString(),
            'shippingStatus': Order.getOrderStatus(dwOrderDetail),
            'confirmationstatus': dwOrderDetail.confirmationStatus.toString(),
            'paymentstatus': dwOrderDetail.paymentStatus.toString(),
            'shipments':[],
            'paymentInstruments': [],
            'customAttributes': [],
            'orderPrices': {},
            'shippedDate': ''
        };

        var orderPrices = orderModel.getOrderPrices();
        orderdetail.orderPrices = orderPrices;

        var billingAddress = {
            'firstName': dwOrderDetail.billingAddress.firstName,
            'lastName': dwOrderDetail.billingAddress.lastName,
            'companyname': dwOrderDetail.billingAddress.companyName,
            'address1': dwOrderDetail.billingAddress.address1,
            'address2': dwOrderDetail.billingAddress.address2,
            'city': dwOrderDetail.billingAddress.city,
            'postalCode': dwOrderDetail.billingAddress.postalCode,
            'stateCode': dwOrderDetail.billingAddress.stateCode,
            'countryCode': dwOrderDetail.billingAddress.countryCode,
            'phone': dwOrderDetail.billingAddress.phone
        };
        orderdetail.billingAddress = billingAddress;

        var paymentInstruments = dwOrderDetail.paymentInstruments;
        for (var prop in paymentInstruments) {
            var paymentInstrument = paymentInstruments[prop];
            var payment = {
                'giftCetificateID' : paymentInstrument.maskedGiftCertificateCode,
                'paymentMethod': paymentInstrument.paymentMethod,
                'creditCardType' : paymentInstrument.creditCardType,
                'maskedCreditCardNumber' : paymentInstrument.maskedCreditCardNumber,
                'creditCardHolder' : paymentInstrument.creditCardHolder,
                'creditCardExpirationMonth' : paymentInstrument.creditCardExpirationMonth,
                'creditCardExpirationYear' : paymentInstrument.creditCardExpirationYear
            };
            orderdetail.paymentInstruments.push(payment);
        }

        var dwShipments = dwOrderDetail.shipments;
        for (var shipments in dwShipments) {
            var shipment = {
                'shipmentid': dwShipments[shipments].shipmentNo,
                'status': dwShipments[shipments].shippingStatus,
                'shippingStatus': dwShipments[shipments].shippingStatus,
                'shippingMethod': dwShipments[shipments].shippingMethod.displayName,
                'shippingMethodID': dwShipments[shipments].shippingMethodID,
                'trackingNumber': dwShipments[shipments].trackingNumber,
                'shippingAddress':[],
                'productLineItems': new dw.util.ArrayList(),
                'gift': dwShipments[shipments].gift,
                'shipmentgrossprice': dwShipments[shipments].shippingTotalGrossPrice
            };

            var shippingaddress = dwShipments[shipments].shippingAddress;
            var shipaddress = {
                'firstName': shippingaddress.firstName,
                'lastName': shippingaddress.lastName,
                'address1':shippingaddress.address1,
                'address2':shippingaddress.address2,
                'city': shippingaddress.city,
                'stateCode': shippingaddress.stateCode,
                'postalCode': shippingaddress.postalCode,
                'countryCode': shippingaddress.countryCode.displayValue,
                'phone': shippingaddress.phone
            };
            shipment.shippingAddress = shipaddress;

            var productItems = dwOrderDetail.productLineItems;
            for (var productItem in productItems) {
                var productline = {
                    'netprice': productItems[productItem].netPrice,
                    'tax': productItems[productItem].tax,
                    'adjustedPrice': productItems[productItem].netPrice,
                    'baseprice': productItems[productItem].basePrice,
                    'lineitemtext': productItems[productItem].lineItemText,
                    'taxbasis': productItems[productItem].taxBasis,
                    'position': productItems[productItem].position,
                    'productID': productItems[productItem].productID,
                    'productName': productItems[productItem].productName,
                    'quantity': productItems[productItem].quantity,
                    'tax-rate': productItems[productItem].taxRate,
                    'shipmentid': productItems[productItem].shipment.shipmentNo,
                    'gift': productItems[productItem].isBonusProductLineItem()
                };
                shipment.productLineItems.push(productline);
            }
            orderdetail.shipments.push(shipment);
        }

        var customAttributes = dwOrderDetail.custom;
        if (!empty(customAttributes)) {
            for (var customAttr in customAttributes) {
                var custom = {
                    attribute: customAttr,
                    value: customAttributes[customAttr]
                }
                orderdetail.customAttributes.push(custom);
            }
        }
    }
    return orderdetail;
};

function getOrderDetailFromOMS(order, ns) {
    var subTotal = 0, orderTotal = 0, pointDiscount = 0, orderDiscount = 0;
    var shippingTotalPrice = 0, shippingDiscount = 0, tax = 0, orderTotal = 0, productDiscount = 0;
    var status = order.ns::["status"];
    var totals = order.ns::["totals"].ns::["merchandize-total"];
    var priceAdjustment = totals.ns::["price-adjustments"].ns::["price-adjustment"];

    var orderdetail = {
        'orderNo' : order.ns::["original-order-no"].toString(),
        'status': status.ns::["order-status"].toString(),
        'shippingStatus':status.ns::["shipping-status"].toString(),
        'confirmationstatus':status.ns::["confirmation-status"].toString(),
        'paymentstatus':status.ns::["payment-status"].toString(),
        'shipments':[],
        'paymentInstruments': [],
        'customAttributes': [],
        'orderPrices': {},
        'shippedDate': ''
    };

    var billing = order.ns::["customer"].ns::["billing-address"];
    var billingAddress = {
        'firstName': billing.ns::["first-name"].toString(),
        'lastName': billing.ns::["last-name"].toString(),
        'company-name': billing.ns::["company-name"].toString(),
        'address1': billing.ns::["address1"].toString(),
        'address2': billing.ns::["address2"].toString(),
        'city': billing.ns::["city"].toString(),
        'postalCode': billing.ns::["postal-code"],
        'stateCode': billing.ns::["state-code"].toString(),
        'countryCode': billing.ns::["country-code"].toString(),
        'phone': billing.ns::["phone"]
    };

    orderdetail.billingAddress = billingAddress;
    var payments = order.ns::["payments"].elements();
    for (var prop in payments) {
        var paymentEl = payments[prop];
        var payment = {
            'giftCetificateID' : paymentEl.ns::["gift-certificate"].ns::["giftcertificate-id"].toString(),
            'creditCardType' : paymentEl.ns::["credit-card"].ns::["card-type"].toString(),
            'maskedCreditCardNumber' : paymentEl.ns::["credit-card"].ns::["card-number"].toString(),
            'creditCardHolder' : paymentEl.ns::["credit-card"].ns::["card-holder"].toString(),
            'creditCardExpirationMonth' : paymentEl.ns::["credit-card"].ns::["expiration-month"].toString(),
            'creditCardExpirationYear' : paymentEl.ns::["credit-card"].ns::["expiration-year"].toString()
        };
        orderdetail.paymentInstruments.push(payment);

        if (!empty(payment.giftCetificateID)) {
            var amount = paymentEl.ns::["amount"].toString();
            amount = parseFloat(amount);
            amount = isNaN(amount) ? 0 : amount;
            pointDiscount += amount; 
        }
    }

    var xmlShipments = order.ns::["shipments"].elements();
    for (var prop in xmlShipments) {
        var shipments = xmlShipments[prop];
        var shippingaddress = shipments.ns::["shipping-address"];
        var shipment = {
            'shipmentid': shipments.attribute("shipment-id"),
            'status': shipments.ns::["status"].ns::["shipping-status"].toString(),
            'shippingStatus' : shipments.ns::["status"].ns::["shipping-status"].toString(),
            'shippingMethod': getShippingMethodName(shipments.ns::["shipping-method"].toString()),
            'shippingMethodID': shipments.ns::["shipping-method"].toString(),
            'trackingNumber' : shipments.ns::["tracking-number"].toString(),
            'shippingAddress':[],
            'productLineItems': new dw.util.ArrayList(),
            'gift': shipments.ns::["gift"],
            'shipmentgrossprice': shipments.ns::["totals"].ns::["shipping-total"].ns::["gross-price"].toString()
        };

        var shipaddress = {
            'firstName': shippingaddress.ns::["first-name"].toString(),
            'lastName': shippingaddress.ns::["last-name"].toString(),
            'address1':shippingaddress.ns::["address1"].toString(),
            'address2':shippingaddress.ns::["address2"].toString(),
            'city': shippingaddress.ns::["city"].toString(),
            'stateCode': shippingaddress.ns::["state-code"].toString(),
            'postalCode': shippingaddress.ns::["postal-code"].toString(),
            'countryCode':shippingaddress.ns::["country-code"].toString(),
            'phone': shippingaddress.ns::["phone"]
        };

        shipment.shippingAddress = shipaddress;
        var productItems = order.ns::["product-lineitems"].elements();
        for (var prop in productItems) {
            var productItem = productItems[prop];
            var pliCustomAttr = productItem.ns::["custom-attributes"].elements();
            var adjustedPrice = new dw.value.Money(Math.abs(productItem.ns::["net-price"]), session.getCurrency().getCurrencyCode());
            var productline = {
                'netprice': productItem.ns::["net-price"],
                'tax' : productItem.ns::["tax"],
                'adjustedPrice' : adjustedPrice,
                'baseprice' : productItem.ns::["base-price"],
                'lineitemtext' : productItem.ns::["lineitem-text"].toString(),
                'taxbasis' : productItem.ns::["tax-basis"],
                'position' : productItem.ns::["position"],
                'productID' : productItem.ns::["product-id"].toString(),
                'productName' : productItem.ns::["product-name"].toString(),
                'quantity' : Math.abs(productItem.ns::["quantity"]),
                'tax-rate' : productItem.ns::["tax-rate"],
                'shipmentid' : productItem.ns::["shipment-id"].toString(),
                'gift' : JSON.parse(productItem.ns::["gift"])
            };
            shipment.productLineItems.push(productline);

            var productPriceAdjustments = productItem.ns::["price-adjustments"].elements();
            for (var prop in productPriceAdjustments) {
                var productPriceAdjustment = productPriceAdjustments[prop];
                var productLineItemDiscount = productPriceAdjustment.ns::['net-price'].toString();

                productLineItemDiscount = parseFloat(productLineItemDiscount);
                productLineItemDiscount = isNaN(productLineItemDiscount) ? 0 : Math.abs(productLineItemDiscount);
                productDiscount += productLineItemDiscount;
            }
        }
        orderdetail.shipments.push(shipment);
    }

    var priceAdjustments = totals.ns::["price-adjustments"].elements();
    for (var prop in priceAdjustments) {
        var priceAdjustment = priceAdjustments[prop];
        var orderLineItemDiscount = priceAdjustment.ns::["net-price"].toString();

        // Calculate order discount include order discount and coupon discount
        orderLineItemDiscount = parseFloat(orderLineItemDiscount);
        orderLineItemDiscount = isNaN(orderLineItemDiscount) ? 0 : Math.abs(orderLineItemDiscount);
        orderDiscount += orderLineItemDiscount; 
    }

    var Money = require('dw/value/Money');

    // Calculate subtotal
    subTotal = totals.ns::["net-price"].toString();
    subTotal = parseFloat(subTotal);
    subTotal = isNaN(subTotal) ? 0 : Math.abs(subTotal);
    subTotal = new Money(subTotal + productDiscount, session.getCurrency().getCurrencyCode());

    // Calculate product discount
    productDiscount = new Money(productDiscount, session.getCurrency().getCurrencyCode());

    // Calculate order discount
    orderDiscount = new Money(orderDiscount, session.getCurrency().getCurrencyCode());

    // get point discount
    pointDiscount = new Money(pointDiscount, session.getCurrency().getCurrencyCode());

    // Shipping total price
    shippingTotalPrice = order.ns::["totals"].ns::["shipping-total"].ns::["net-price"];
    shippingTotalPrice = parseFloat(shippingTotalPrice);
    shippingTotalPrice = isNaN(shippingTotalPrice) ? 0 : Math.abs(shippingTotalPrice);
    shippingTotalPrice = new Money(shippingTotalPrice, session.getCurrency().getCurrencyCode());

    // Calculate shipping discount
    var shippingExclDiscounts = shippingTotalPrice;
    var shippingInclDiscounts = order.ns::["totals"].ns::["adjusted-shipping-total"].ns::["net-price"];
    shippingInclDiscounts = parseFloat(shippingInclDiscounts);
    shippingInclDiscounts = isNaN(shippingInclDiscounts) ? 0 : Math.abs(shippingInclDiscounts);
    shippingDiscount = shippingExclDiscounts - shippingInclDiscounts;
    shippingDiscount = new Money(shippingDiscount, session.getCurrency().getCurrencyCode());

    var tax = order.ns::["totals"].ns::["order-total"].ns::["tax"];
    tax = parseFloat(tax);
    tax = isNaN(tax) ? 0 : Math.abs(tax);
    tax = new Money(tax, session.getCurrency().getCurrencyCode());

    // order total
    orderTotal = order.ns::["totals"].ns::["order-total"].ns::["gross-price"];
    orderTotal = parseFloat(orderTotal);
    orderTotal = isNaN(orderTotal) ? 0 : Math.abs( orderTotal);
    orderTotal = orderTotal - pointDiscount;
    orderTotal = new Money(orderTotal, session.getCurrency().getCurrencyCode());

    orderdetail.orderPrices = {
        subTotal: subTotal,
        orderDiscount: orderDiscount.add(productDiscount),
        pointDiscount: pointDiscount,
        shippingTotalPrice: shippingTotalPrice,
        shippingDiscount: shippingDiscount,
        tax: tax,
        orderTotal: orderTotal
    }

    var customAttributes = order.ns::["custom-attributes"].elements();
    if (!empty(customAttributes)) {
        for (var customAttr in customAttributes) {
            var custom = {
                attribute: customAttributes[customAttr].attributes(),
                value: customAttributes[customAttr].toString()
            }
            orderdetail.customAttributes.push(custom);
        }
    }

    return orderdetail;
};

function updateStatusGiftProducts(orderdetail) {
    var orderModel = Order.get(orderdetail.orderNo);
    var dwOrderDetail = orderModel.object;

    var shipments = dwOrderDetail.shipments;
    for (var i = 0; i < shipments.length; i++) {
        var productItems = shipments[i].productLineItems;
        for (var j = 0; j < productItems.length; j++) {
            var productItem = productItems[j];
            if (productItem.custom.sample) {
                orderdetail.shipments[i].productLineItems[j].gift = true;
            }
        }
    }
}

function getShippingMethodName(shippingMethodID) {
    var currentShippingMethods = ShippingMgr.getAllShippingMethods();
    for (var prop in currentShippingMethods) {
        var currentShippingMethod = currentShippingMethods[prop];
        if(currentShippingMethod.ID === shippingMethodID) {
            return currentShippingMethod.displayName;
        }
    }
}

function getShippedDate(orderID) {
    var orderModel = app.getModel('Order').get(orderID);
    var dwOrderDetail = orderModel.object;

    if (!empty(dwOrderDetail.custom.shippedDate)) {
        var shippedDate = dwOrderDetail.custom.shippedDate;
        var calendar = new Calendar(shippedDate);
        return StringUtils.formatCalendar(calendar, 'MM/dd/yyyy');
    }
}

module.exports = OrderDetailModel;