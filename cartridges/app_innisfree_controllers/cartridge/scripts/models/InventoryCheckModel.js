var AbstractModel = require('./AbstractModel');
var ServiceRegistry = require('dw/svc/ServiceRegistry');
var ArrayList = require('dw/util/ArrayList');
var Logger = dw.system.Logger.getLogger('OmsInventoryCheck');
var InventoryCheckModel = AbstractModel.extend({

});

InventoryCheckModel.checkInventoryRequest = function(productList){
    require('~/cartridge/scripts/services/initInventoryCheckService');
    var prodIds = new ArrayList();
    var service = ServiceRegistry.get('http.inventorycheck');
    var inventoryResponse = null;
    
    try{
    	service.setRequestMethod('GET');
        for (var i = 0; i < productList.length; i++) {
            prodIds.add(productList[i]);
        }
        var result = service.call(prodIds);
        if(empty(result) || result.status != "OK")
    	{
    		Logger.error("InventoryCheckModel checkInventoryRequest returned error status : "+result.status+' unavailableReason : '+result.unavailableReason+' error message : '+result.msg+' for product Ids : '+prodIds.toArray().toString());
    	} else 
    		if(!empty(result.object) && !empty(result.object.text)){
    			inventoryResponse = JSON.parse(result.object.text);
        	}        
    }catch(e){
    	Logger.error("InventoryCheckModel.checkInventoryRequest script ran into error :"+e);
    }
    
    return inventoryResponse;
    
};

module.exports = InventoryCheckModel;