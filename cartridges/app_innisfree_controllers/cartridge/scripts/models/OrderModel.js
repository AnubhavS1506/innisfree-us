'use strict';

/**
* Module for ordering functionality.
* @module models/OrderModel
*/

/* API Includes */
var AbstractModel = require('./AbstractModel');
var Order = require('dw/order/Order');
var OrderMgr = require('dw/order/OrderMgr');
var Resource = require('dw/web/Resource');
var Status = require('dw/system/Status');
var Transaction = require('dw/system/Transaction');

/**
 * Place an order using OrderMgr. If order is placed successfully,
 * its status will be set as confirmed, and export status set to ready.
 * @param {dw.order.Order} order
 */
function placeOrder(order) {
    var placeOrderStatus = OrderMgr.placeOrder(order);
    if (placeOrderStatus === Status.ERROR) {
        OrderMgr.failOrder(order);
        throw new Error('Failed to place order.');
    }
    order.setConfirmationStatus(Order.CONFIRMATION_STATUS_CONFIRMED);
    order.setExportStatus(Order.EXPORT_STATUS_READY);
}
/**
 * Order helper class providing enhanced order functionality.
 * @class module:models/OrderModel~OrderModel
 * @extends module:models/AbstractModel
 *
 * @param {dw.order.Order} obj The order object to enhance/wrap.
 */
var OrderModel = AbstractModel.extend({
    getOrderPrices: function () {
        var Money = require('dw/value/Money');
        var order = this.object;
        // Calculate subtotal
        var subTotal = order.getMerchandizeTotalNetPrice();
        var i = 0;
        var j = 0;

        // Calculate product discounts
        var productDiscount = 0;
        var productItems = order.productLineItems;
        for (i = 0; i < productItems.length; i++) {
            var priceAdjustments = productItems[i].priceAdjustments;
            for (j = 0; j < priceAdjustments.length; j++) {
                var productLineItemDiscount = priceAdjustments[j];
                productDiscount += productLineItemDiscount.netPrice;
            }
        }
        productDiscount = Math.abs(productDiscount);
        productDiscount = new Money(productDiscount, order.currencyCode);

        // Calculate order discounts
        var merchTotalExclOrderDiscounts = order.getAdjustedMerchandizeTotalPrice(false);
        var merchTotalInclOrderDiscounts = order.getAdjustedMerchandizeTotalPrice(true);
        var orderDiscount = merchTotalExclOrderDiscounts.subtract(merchTotalInclOrderDiscounts);

        // get giftcertificate amount
        var pointDiscount = 0;
        var gcPrefix = dw.web.Resource.msg('point.giftcert.prefix', 'preference', null);
        var gcPaymentInstruments = order.getGiftCertificatePaymentInstruments();
        //if GC exist
        if (gcPaymentInstruments.length > 0) {
            for (i = 0; i < gcPaymentInstruments.length; i++) {
                if (gcPaymentInstruments[i].giftCertificateCode.indexOf(gcPrefix) > -1) {
                    pointDiscount = gcPaymentInstruments[i].paymentTransaction.amount.value;
                    break;
                }
            }
        }
        pointDiscount = new Money(pointDiscount, order.currencyCode);

        // Shipping total price
        var shippingTotalPrice = order.shippingTotalPrice;

        // Calculate shipping discount
        var shippingExclDiscounts = order.shippingTotalPrice;
        var shippingInclDiscounts = order.getAdjustedShippingTotalPrice();
        var shippingDiscount = shippingExclDiscounts.subtract(shippingInclDiscounts);

        // tax
        var tax = order.totalTax;

        // order total
        var orderTotal = order.totalGrossPrice;
        orderTotal = orderTotal.subtract(pointDiscount);

        return {
            subTotal: subTotal,
            orderDiscount: orderDiscount.add(productDiscount),
            pointDiscount: pointDiscount,
            shippingTotalPrice: shippingTotalPrice,
            shippingDiscount: shippingDiscount,
            tax: tax,
            orderTotal: orderTotal
        };
    }
});

/**
 * Gets a new instance for a given order or order number.
 *
 * @alias module:models/OrderModel~OrderModel/get
 * @param parameter {dw.order.Order | String} The order object to enhance/wrap or the order ID of the order object.
 * @returns {module:models/OrderModel~OrderModel}
 */
OrderModel.get = function (parameter) {
    var obj = null;
    if (typeof parameter === 'string') {
        obj = OrderMgr.getOrder(parameter);
    } else if (typeof parameter === 'object') {
        obj = parameter;
    }
    return new OrderModel(obj);
};

/**
 * Submits an order
 * @param order {dw.order.Order} The order object to be submitted.
 * @transactional
 * @return {Object} object If order cannot be placed, object.error is set to true. Ortherwise, object.order_created is true, and object.Order is set to the order.
 */
OrderModel.submit = function (order) {
    var Email = require('./EmailModel');
    var GiftCertificate = require('./GiftCertificateModel');
    try {
        Transaction.begin();
        placeOrder(order);

        // Creates gift certificates for all gift certificate line items in the order
        // and sends an email to the gift certificate receiver

        order.getGiftCertificateLineItems().toArray().map(function (lineItem) {
            return GiftCertificate.createGiftCertificateFromLineItem(lineItem, order.getOrderNo());
        }).forEach(GiftCertificate.sendGiftCertificateEmail);
        
        Transaction.commit();
    } catch (e) {
        Transaction.rollback();
        return {
            error: true,
            PlaceOrderError: new Status(Status.ERROR, 'confirm.error.technical')
        };
    }

//	  **** Order is Placed ****
    var orderConfirmationMessageObject = require('int_bronto/cartridge/scripts/messages/OrderConfirmationMessage');
    var objects = null;
    var additionalRecipients = null;
    var edmStatus = orderConfirmationMessageObject.edmOrderConfirmation(order, objects, additionalRecipients);
    if (edmStatus.status == 'error') {
        Email.sendMail({
            template: 'mail/orderconfirmation',
            recipient: order.getCustomerEmail(),
            subject: Resource.msg('order.orderconfirmation-email.001', 'order', null),
            context: {
                Order: order
            }
        });
    }
    return {
        Order: order,
        order_created: true
    };
}

OrderModel.getOrderStatus = function(dwOrder) {
    var status = '';
    if (((dwOrder.status == Order.ORDER_STATUS_NEW || dwOrder.status == Order.ORDER_STATUS_OPEN) && dwOrder.shippingStatus == Order.SHIPPING_STATUS_SHIPPED) || dwOrder.status == Order.ORDER_STATUS_COMPLETED) {
        status = Resource.msg('account.orderstatusinclude.ordershipped', 'account', null);
    } else if ((dwOrder.status == Order.ORDER_STATUS_NEW || dwOrder.status == Order.ORDER_STATUS_OPEN) && dwOrder.shippingStatus != Order.SHIPPING_STATUS_SHIPPED) {
        status = Resource.msg('account.orderdetails.orderstatus.confirmed', 'account', null);
    } else if (dwOrder.status == Order.ORDER_STATUS_CANCELLED) {
        status = Resource.msg('account.orderstatusinclude.ordercanceled', 'account', null);
    }
    return status;
};

/** The order class */
module.exports = OrderModel;
