var AbstractModel = require('./AbstractModel');
var ServiceRegistry = require('dw/svc/ServiceRegistry');
var Service = require('dw/svc/Service');
var Result = require('dw/svc/Result');
var WSUtil = require('dw/ws/WSUtil');
var Money = require('dw/value/Money');
var app = require('~/cartridge/scripts/app');
var Order = app.getModel('Order');

var OrderHistoryModel = AbstractModel.extend({
});

OrderHistoryModel.getHistory = function(customerID){
    require('~/cartridge/scripts/services/initOrderHistoryService');
    var orderList = new dw.util.ArrayList();
    var service;
    var result;
    service = ServiceRegistry.get('http.orderhistory');
    service.URL += '?CustomerNo='+ customerID;
    service.setRequestMethod("GET");
    result = service.call();
    if (!empty(result) && !empty(result.object)){
       var xmlText = new XML(result.object.text);
       var ns = xmlText.namespace();
       var orders = xmlText.children(); 
        
       for (var i = 0; i < orders.length(); i++) {
            var order = orders[i];
            var orderTotal = new Money(
                order.ns::['order-total'],
                session.getCurrency().getCurrencyCode()
            );
            var tmpData = {
                'orderNumber' : order.attribute('order-no').toString(),
                'orderDate' : order.ns::['order-date'].toString(),
                'status': order.ns::['status'].toString(),
                'orderTotal' : orderTotal,
                'trackingNumber' : order.ns::['tracking-number'].toString()
            };
            orderList.push(tmpData);
       }

       var profile = customer.getProfile();
       var dwOrderList = dw.order.OrderMgr.queryOrders("status != {0} AND status != {1} AND customerNo = {2}",
            "creationDate desc", 
            dw.order.Order.ORDER_STATUS_FAILED,
            dw.order.Order.ORDER_STATUS_CREATED,
            profile.getCustomerNo()
       );

       while (dwOrderList.hasNext()) {
            var dwOrderd = dwOrderList.next();
            var isNotExistedInOMS = true;
            var orderTotal = new Money(dwOrderd.totalGrossPrice.value, session.getCurrency().getCurrencyCode());
            var dwOrder = {
                'orderNumber': dwOrderd.orderNo.toString(), //Add toString() so as to sort orders in PropertyComparator
                'orderDate': new Date(dwOrderd.creationDate).toISOString(),
                'status' : Order.getOrderStatus(dwOrderd),
                'orderTotal' : orderTotal,
                'trackingNumber': dwOrderd.shipments[0].trackingNumber
            };
            for (var orderd in orders){
                if (dwOrder.orderNumber == orders[orderd].attribute("order-no").toString()){
                    // To check whether current orderNo existed in DW, we should omit the existed one.
                    isNotExistedInOMS = false; 
                    break;
                }
            }
            if (isNotExistedInOMS){
                orderList.push(dwOrder);
            }
       }
       if (!empty(orderList)) {
    	   orderList.sort(new dw.util.PropertyComparator('orderNumber', false));
       }
    }
    return orderList;
};

module.exports = OrderHistoryModel;
