'use strict';

/* API Includes */
var Cart = require('~/cartridge/scripts/models/CartModel');
var PaymentMgr = require('dw/order/PaymentMgr');
var Pipeline = require('dw/system/Pipeline');
var Transaction = require('dw/system/Transaction');
/**
 * This is where additional PayPal integration would go. The current implementation simply creates a PaymentInstrument and
 * returns 'success'.
 */
function Handle(args) {
    var paymentMethod = PaymentMgr.getPaymentMethod(args.PaymentMethodID);
    var cart = Cart.get(args.Basket);
    var pdict = Pipeline.execute('PAYPAL_EXPRESS-Handle', {
        PaymentMethod: paymentMethod,
        Basket: args.Basket
    });
    if (pdict.EndNodeName === 'success') {
        //create payment instrument
        Transaction.wrap(function () {
            var paymentInstrument = cart.createPaymentInstrument(args.PaymentMethodID, cart.getNonGiftCertificateAmount());
            var paymentTransaction = paymentInstrument.getPaymentTransaction();
            paymentTransaction.custom.payPalPayerId = pdict.PayerId;
            paymentTransaction.custom.paypalEcSetRequestID = pdict.SetRequestID;
            paymentTransaction.custom.paypalEcSetRequestToken = pdict.SetRequestToken;
            paymentTransaction.custom.paypalToken = pdict.PayPalToken;
        });
        return {success: true};
    } else {
        return {error: true};
    }
}

/**
 * Authorizes a payment using a credit card. The payment is authorized by using the PAYPAL_EXPRESS processor only
 * and setting the order no as the transaction ID. Customizations may use other processors and custom logic to
 * authorize credit card payment.
 */
function Authorize(args) {
    var pdict = Pipeline.execute('PAYPAL_EXPRESS-Authorize', {
        OrderNo: args.OrderNo,
        Order: args.Order,
        PaymentInstrument: args.PaymentInstrument
    });
    return pdict.CaptureReasonCode;
}

/*
 * Module exports
 */

/*
 * Local methods
 */
exports.Handle = Handle;
exports.Authorize = Authorize;
